#include <cstdio>
#include <stdlib.h>
#include <ctime>

//필요 매커니즘
//갔다온 위치는 재입장 불가

//주의 
//0과 9같은 끝 수자의 경우 오류 발생


int main(){
	srand((unsigned)time(NULL));
	bool map[15][15];
	int x[16];
	int y[16];
	srand(time(NULL));

	//현재 위치에서 상하좌우에 재입장하는 것인지 검사하는 변수

	bool direction[4];

	for (int i = 0; i < 15; i++){
		for (int k = 0; k < 15; k++){
			map[i][k] = false;
		}
	}
	for (int i = 0; i < 16; i++){
		x[i] = 0;
		y[i] = 0;
	}
	x[0] = rand() % 15;
	y[0] = rand() % 15;
	for (int i = 0; i < 15; i++){

		for (int k = 0; k < 4; k++){
			direction[k] = false;
		}
		//random 공통 랜덤 변수
		int random = 0;

		//direction이 갯수를 나타내는 변수
		int Count = 0;
		//현재 위치에서 상하좌우에 재입장하는 것인지 검사하는 함수

		if (y[i] == 14){
			direction[0] = true;
		}
		else if (y[i] == 0){
			direction[1] = true;
		}
		if (x[i] == 0){
			direction[2] = true;
		}
		else if (x[i] == 14){
			direction[3] = true;
		}

		if (map[x[i]][y[i] + 1] == 1){
			direction[0] = true;
		}
		if (map[x[i]][y[i] - 1] == 1){
			direction[1] = true;
		}
		if (map[x[i] - 1][y[i]] == 1){
			direction[2] = true;
		}
		if (map[x[i] + 1][y[i]] == 1){
			direction[3] = true;
		}

		//direction을 변수의 끝에서 증가 못하게 사용했다.

		//Count를 여기서 센다.
		//direction의 곗수 정도만 넣으면 된다.

		for (int k = 0; k < 4; k++){
			if (direction[k]){
				Count += 1;
			}
		}
		//Count의 갯수로 몇번이

		//카운터의 공통점을 찾아 간략하게 만들자
		switch (Count){
		case 4:{
			x[0] = rand() % 15;
			y[0] = rand() % 15;
			for (int k = 0; k < 4; k++){
				direction[k] = false;
			}
			for (int i = 0; i < 15; i++){
				for (int k = 0; k < 15; k++){
					map[i][k] = false;
				}
			}
			i = 0;
			continue;
		}
		case 3:{
			int instead;
			for (int k = 0; k < 4; k++){
				if (!direction[k]){
					switch (k)
					{
					case 0:
						instead = k;
						break;
					case 1:
						instead = k;
						break;
					case 2:
						instead = k;
						break;
					case 3:
						instead = k;
						break;
					}
				}
				random = instead;
			}
			break;
		}
		case 2:{
			int multiply = 0;
			int plus = 0;
			int instead = rand() % 2;
			// 처음 나온 숫자와 나중에 나온 숫자 받고 계산해
			for (int k = 0; k < 4; k++){
				if (!direction[k]){
					multiply = k + 1;

					if (instead == 0){
						random = k;
					}
					break;
				}
			}
			for (int k = multiply; k < 4; k++){
				if (!direction[k] && instead == 1){
					random = k;
					break;
				}
			}
			break;
		}
		case 1:{
			int plus = 0;
			// 처음 나온 숫자와 나중에 나온 숫자 받고 계산해
			for (int k = 0; k < 4; k++){
				if (direction[k]){
					plus = k;
					break;
				}
			}
			int instead = plus;
			while (plus == instead){
				instead = rand() % 4;
			}
			random = instead;
			break;
		}
		case 0:{
			random = rand() & 4;
			break;
		}
		}
		switch (random)
		{
		case 0:
			y[i] += 1;
			break;

		case 1:
			y[i] -= 1;
			break;

		case 2:
			x[i] -= 1;
			break;

		case 3:
			x[i] += 1;
			break;
		}

		map[x[i]][y[i]] = true;

		x[i + 1] = x[i];
		y[i + 1] = y[i];
	}
	for (int i = 0; i < 14; i++){
		for (int k = 0; k < 14; k++){
			if (map[k][i]){
				printf("G");
			}
			else{
				printf(" ");
			}
		}
		printf("\n");
	}
	//false일경우 띄어쓰기를 true일 경우 O을 넣는다.
	return true;

}
