#ifndef __GAME_STRUCTURE_H__
#define __GAME_STRUCTURE_H__
#include "ALlLife.h"
#include "Map.h"

//option에 갔다가 나오면 playScene에 있던 정보는 어떡해야 보존이 가능한가?
//주인공의 위치가필요


// 0 종료
// 1 시작 화면 // 1 새로 시작 2 이어하기 3 종료
// 2 이어하기 화면 // 저장 파일에 따라 갯수 증가
// 3 게임 실행 화면 
// 4 설정 화면   // 1 저장 2 시작화면 3 닫기 4 종료 
// 5 저장 화면 // 비었을 경우 바로 저장 //존재할경우 경고 후 덮어쓰기
// 6 죽은 화면 // 1 시작화면 2 종료
enum GameScene_State {End = '0', StartScene, ConnectScene, PlayScene, OptionScene, SaveScene, DiedScene };

enum Charector_Environment{ Died = '0',Peace, Battle, Trap };

enum Charector_Behavior{ Choice = '0', Map, Rest, Walking, Attack,Option = 27};

class Game_Structure{
private:
	GameScene_State Game_State;
	GameScene_State Befor_Game_State;
	Charector_Environment Char_Environment;
	Charector_Behavior Char_Behavior;
	Life Hero;
	const char Choice_StartScene[3] = { '3', '2', '0' };
	//나중에 추가 const char Choice_Connect_Scene = ; 이어서하기
	const char Choice_OptionScene[4] = { '0','1','2','3' };
	// 나중에 추가const char Choice_SaveScene[]; 저장화면
	const char Choice_DiedScene[2] = {'0','6'};
	const char Play_Choice_Peace[4] = { '1', '2', '3', 27 };
	const char Play_Choice_Battle[5] = { '1', '2', '3','4', 27 };
public:

	//if문으로 이 함수가 false일경우 나가게 만들었다.
	bool All_Scene_Behavior();

	//게임플레이에 씬에 대한 것들을 모두 담당하는 함수
	void Choice_Play_Scene();

	//캐릭터의 선택과 물음을 담당하는 함수
	void Choice_Char_Peace();
	void Choice_Char_Battle();
	void Choice_Char_Trap();
	void Choice_Char_Map();

	Game_Structure(){
		Game_State = GameScene_State::StartScene;
		Char_Environment = Charector_Environment::Peace;
		Char_Behavior = Charector_Behavior::Choice;
	}
};
// 주인공을 공격할 적들이 필요하다.
//함수만을 사용할수가 없다.
//함수의 리턴값도 가능한가?
void Charactor_Cure(Life &  );
void Charactor_Attack(Life & , Life &);

//모든 화면과 선택을 만드는 함수
void Print_and_Choice(int ,const char* , char*,int ,int = 0);

#endif 