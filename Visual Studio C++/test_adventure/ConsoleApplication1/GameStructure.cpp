#include "GameStructure.h"
#include "ALlLife.h"
#include <cstdio>
#include <conio.h>

// play에 들어왔을떄 어떡해 돌려야하는지

//option을 playScene에다가 추가할까?
bool Game_Structure::All_Scene_Behavior(){
	switch (Game_Structure::Game_State)
	{
	case GameScene_State::End:
		return false;
	case GameScene_State::StartScene:
		Print_and_Choice(sizeof(Choice_StartScene), Choice_StartScene, " 1 : Game Play \n 2 : Connect Play \n 3 : End \n", Game_State,Befor_Game_State);
		break;
	case GameScene_State::ConnectScene:
		Print_and_Choice(1, "1", "Comming soon", Game_State);
		break;
	case GameScene_State::PlayScene:
		Game_Structure::Choice_Play_Scene();
		break;
	case GameScene_State::OptionScene:
		Print_and_Choice(sizeof(Choice_OptionScene), Choice_OptionScene, " 1 : Save \n 2 : StartScene \n 3 : Option Close \n 4 : End", Game_State, Befor_Game_State);
		break;
	case GameScene_State::SaveScene:
		Print_and_Choice(1, "1", "Comming soon", Game_State);
		break;
	case GameScene_State::DiedScene:
		Print_and_Choice(sizeof(Choice_DiedScene), Choice_DiedScene, " 1 : Start Scene \n 2 : End", Game_State);
		break;
	default:
		printf("Get_GameScene_State_Wrong Choice   location : Game_Scene_State()");
		break;
	}
	return true;
}
// 1 시작 화면 // 1 새로 시작 2 이어하기 3 종료
// 2 이어하기 화면 //  아직 만들지 않았어 저장 파일에 따라 갯수 증가 
// 3 게임 플레이 화면 // 다른 함수를 사용해 전투와 같은 시스템 구현
void Game_Structure::Choice_Play_Scene(){
	srand(time(NULL));
	switch (Game_Structure::Befor_Game_State){
	case GameScene_State::StartScene:
		//좌표와 현재 게임의 상태 (전투중, 비전투중) 그것들을 모두 값을 지정해주어야한다.
		break;
	case GameScene_State::ConnectScene:
		//게임데이터들을 모두 받아와 입혀야한다.
 		break;
	case GameScene_State::OptionScene:
		//데이터를 초기화 하지 않고 그냥 진행해도된다.
		break;
	}
	while (true)
	{
		switch (Char_Environment){
		case Charector_Environment::Died:
			Game_State = GameScene_State::DiedScene;
			return;
		case Charector_Environment::Peace:
			Choice_Char_Peace();
			break;
		case Charector_Environment::Battle:
			Choice_Char_Battle();
			break;
		case Charector_Environment::Trap:
			Choice_Char_Trap();
			break;
		}
	}
}
// 4 설정 // 1저장 2시작 3닫기 4종료   아직 만들지 않았어 esc
// 5 저장 화면 // 비었을 경우 바로 저장 //존재할경우 경고 후 덮어쓰기  아직 만들지 않았어 esc
// 6 죽은 화면 // 1 시작화면 2 종료

//키보드의 값에서 추가적으로 능력들이 발휘가 되야함
void Game_Structure::Choice_Char_Peace(){
	while (true)
	{
		Print_and_Choice(sizeof(Play_Choice_Peace), Play_Choice_Peace, "1 : MapOpen \n 2 : Rest \n 3 : Walking", Char_Behavior);
		switch (Char_Behavior){
		case Charector_Behavior::Map:
			break;
		case Charector_Behavior::Rest:
			break;
		case Charector_Behavior::Walking:
			return;
		case Charector_Behavior::Option:
			return;
		}
	}
}
void Game_Structure::Choice_Char_Battle(){
	while (true)
	{
		Print_and_Choice(3, Play_Choice_Peace, "1 : MapOpen \n 2 : Rest \n 3 : Walking", Char_Behavior);
		switch (Char_Behavior){
		case Charector_Behavior::Map:
			break;
		case Charector_Behavior::Rest:
			break;
		case Charector_Behavior::Walking:
			return;
		case Charector_Behavior::Option:
			return;
		}
	}
}
void Game_Structure::Choice_Char_Trap(){}
void Game_Structure::Choice_Char_Map(){
}

//Choi에 문자열의 크기를 설정하여 그 값만큼 a가 돌고 그곳에서 트루가 나올경우  instate를 넣어라
void Print_and_Choice(int a,const char* Choi, char* print,int inState,int before = 0){
	printf(print);
	before = inState;
	while (true){
		int co = getch();
		if(a >= co-49){
			inState = Choi[co - 49];
		}
	}
}

void Charactor_Cure(Life &Acting){
	srand(time(NULL));
	Acting.setHP( Acting.getMaxHitPoint() / rand()% 5 + 2);
}
void Charactor_Attack(Life &appointed, Life &Acting){
	srand(time(NULL));
	appointed.setHP(- Acting.getDamage() * rand() % 4);
}