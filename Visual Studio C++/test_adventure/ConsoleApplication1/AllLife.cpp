#include "ALlLife.h"
#include <cstdio>

//캐릭터를 설정하는 것이다

int Life::getLever(){ return Life::Lever; }
int Life::getgauge(){ return Life::gauge; }
int Life::getMaxHitPoint(){ return Life::MaxHitPoint; }
int Life::getNowHitPoint() { return Life::NowHitPoint; }
int Life::getDamage() { return Life::Damage; }

void Life::setDamage(int DM){ Life::Damage += DM; }
void Life::setHP(int HP){
	if (Life::NowHitPoint + HP >= Life::MaxHitPoint){ Life::NowHitPoint = Life::MaxHitPoint; }
	else { Life::NowHitPoint += HP; }
}
void Life::setLever(int ba){
	while (Life::Lever + 1 * 5 <= ba){
		ba -= Lever * 5;
		Life::Lever += 1;
	}
}
