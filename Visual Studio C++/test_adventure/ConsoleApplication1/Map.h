#ifndef __MAP_H__
#define __MAP_H__
#include <cstdlib>
#include <ctime>
//몬스터를 만들어야한다
//함정도 몬스터로 취급하고


class World{
private:
	//길의 갯수를 정하고 더 나아가는 길을 정하고 그곳에서 길의 량을 정하고 반복하다가 맵의 크기(맵의 량, 길의 량)을 넘으면 중단
	//그대신 왔던길은 반향이 안잡히게
	// 6이동시 4는 무시 같은 식으로
	// 좌표 0 , 0
	bool Location[15][15];
	//X와 Y는 현재 위치를 알려주는 것이다.
	int x;
	int y;
public:
	// []배열의 값은 정하지 않아도 된다.
	//[]배열은 주소값을 가지기 때문에 &사용하지 않아도 값이 변경된다
	void Randommap();
	void SetLocation(int, int);
	void SetX(int);
	void SetY(int);
	bool getLocation(int, int);
	int GetX();
	int GetY();
	void SeeMap();
	//랜덤에 필요한 것들 맵의 크기(차례안에 깨야한다), 길의 량, 맵의 반향, 현재 땅의 상태(전투 상태인지, 함정인지 )
};

#endif