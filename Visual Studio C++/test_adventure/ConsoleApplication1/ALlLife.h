#ifndef __ALLLIFE_H__
#define __ALLLIFE_H__
#include <ctime>
#include <cstdlib>
#include "Map.h"

class Life{
private:
	int Lever;
	int gauge;
	int Damage;
	int NowHitPoint;
	int MaxHitPoint;
public:
	World HeroMap;
	Life(){
		srand(time(NULL));
		Life::Lever = 0;
		Life::gauge = 0;
		Life::Damage = rand() % 3 + 1;
		Life::MaxHitPoint = rand() % 4 + 3;
		Life::NowHitPoint = Life::MaxHitPoint;
	}
	int getLever();
	int getDamage();
	int getNowHitPoint();
	int getgauge();
	int getMaxHitPoint();

	void setLever(int);
	void setDamage(int);
	void setHP(int);
};
#endif