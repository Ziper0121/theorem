#include "Map.h"

#include <ctime>
#include <cstdio>
#include <cstdlib>

/*하고있던 일 Map을 그냥 randmmap만가지고 있게 바꿔놓고 있었다.
이유 : player class에서 Map 변수를 그냥 public에 두면 안될것같아서
Player내부에 그냥 Map을 가지고 있기 위해서
*/
void MapInformation::Randomcreate(Difficulty difficulty){
	srand(time(NULL));

	/*난이도에 따라 맵을 배치하고 맵의 사이즈를 설정하는 함수
	*/
	MapinRoomInformation.NowMapDifficulty = difficulty;
	MapinRoomInformation.createMapDifficulty(difficulty);

	int battleRoomCount = 0;

	int arr_maxsize = MapinRoomInformation.MapSize;
	/*-1인 이유는 arr-atleastize의 비교대상은 y는 -1이 된 값이기 때문이다.
	*/
	int arr_atleastize = -1;
	int LodeMaxSize = MapinRoomInformation.LodeSize;
	//현재 위치에서 상하좌우에 재입장하는 것인지 검사하는 변수

	bool direction[4];

	for (int i = 0; i < arr_maxsize; i++){
		for (int k = 0; k < arr_maxsize; k++){
			DungeonStructure[i][k] = RoominMap::BlockageRoom;
			block[i][k] = false;
		}
	}

	/*짧지만 몬스터가 함정이 왕창 나오는 맵
	중간
	길지만 몬스터와 함정 적은 맵
	*/

	for (int i = 0; i < arr_maxsize + 1; i++){
		TrueLode[i].Y = 0;
		TrueLode[i].X = 0;
	}
	TrueLode[0].Y = rand()%arr_maxsize;
	TrueLode[0].X = rand()%arr_maxsize;
	for (int i = 0; i < LodeMaxSize; i++){
		if (i == 0){
			block[TrueLode[i].Y][TrueLode[i].X] = true;
			TrueLode[i + 1].Y = TrueLode[i].Y;
			TrueLode[i + 1].X = TrueLode[i].X;
			continue;
		}

		for (int k = 0; k < 4; k++){
			direction[k] = false;
		}
		//random 공통 랜덤 변수
		int random = 0;

		//direction이 갯수를 나타내는 변수
		int Count = 0;
		//현재 위치에서 상하좌우에 재입장하는 것인지 검사하는 함수

		if (TrueLode[i].Y == arr_maxsize - 1){
			direction[0] = true;
		}
		else if (TrueLode[i].Y == 0){
			direction[1] = true;
		}
		if (TrueLode[i].X == 0){
			direction[2] = true;
		}
		else if (TrueLode[i].X == arr_maxsize - 1){
			direction[3] = true;
		}

		if (block[TrueLode[i].Y + 1][TrueLode[i].X] && arr_maxsize > TrueLode[i].Y + 1){
			direction[0] = true;
		}
		if (block[TrueLode[i].Y - 1][TrueLode[i].X] && arr_atleastize <TrueLode[i].Y - 1){
			direction[1] = true;
		}
		if (block[TrueLode[i].Y][TrueLode[i].X - 1] && arr_atleastize < TrueLode[i].X - 1){
			direction[2] = true;
		}
		if (block[TrueLode[i].Y][TrueLode[i].X + 1] && arr_maxsize > TrueLode[i].X + 1){
			direction[3] = true;
		}

		//direction을 변수의 끝에서 증가 못하게 사용했다.

		//Count를 여기서 센다.
		//direction의 곗수 정도만 넣으면 된다.

		for (int k = 0; k < 4; k++){
			if (direction[k]){
				Count++;
			}
		}
		//Count의 갯수로 몇번이
		//카운터의 공통점을 찾아 간략하게 만들자
		switch (Count){
		case 4:{
			for (int k = 0; k < 4; k++){
				direction[k] = false;
			}
			for (int k = 0; k < arr_maxsize; k++){
				block[TrueLode[k].Y][TrueLode[k].X] = false;
				TrueLode[k].Y = 0;
				TrueLode[k].X = 0;
			}
			TrueLode[i].Y = rand() % arr_maxsize;
			TrueLode[i].X = rand() % arr_maxsize;
			/*continue를 하면 i++이 처리가 되서 올라간다. i의 값이 1이 된다.
			*/
			i = -1;
			continue;
		}
		case 3:{
			for (int k = 0; k < 4; k++){
				if (!direction[k]){
					random = k;
				}
			}
			break;
		}
		case 2:{
			/*건들지마
			*/
			int num = 0;
			int instead = rand() % 2;
			int getdirection[2];
			for (int k = 0; k < 4; k++){
				if (!direction[k]){
					getdirection[num] = k;
					num++;
				}
			}
			random = getdirection[instead];
			break;
		}
		case 1:{
			int plus = 0;
			// direction이 true인 값이 나오면 그값을 받아서
			for (int k = 0; k < 4; k++){
				if (direction[k]){
					plus = k;
					break;
				}
			}
			int instead = plus;
			/*while 문으로 같은 값이 안나올때까지 돌리면 된다.
			*/
			while (plus == instead){
				instead = rand() % 4;
			}
			random = instead;
			break;
		}
		case 0:{
			random = rand() % 4;
			break;
		}
		}
		/*건들지마
		*/
		switch (random)
		{
		case 0:{
			TrueLode[i].Y += 1;
			break;
		}
		case 1:{
			TrueLode[i].Y -= 1;
			break;
		}
		case 2:
		{
			TrueLode[i].X -= 1;
			break;
		}
		case 3:
		{
			TrueLode[i].X += 1;
			break;
		}
		}
		block[TrueLode[i].Y][TrueLode[i].X] = true;
		TrueLode[i + 1].Y = TrueLode[i].Y;
		TrueLode[i + 1].X = TrueLode[i].X;
	}
	int num = 0;
	for (int i = 0; i < LodeMaxSize; i++){
		DungeonStructure[TrueLode[i].Y][TrueLode[i].X] = MapinRoomInformation.Roomorder[i];
		if (MapinRoomInformation.Roomorder[i] == RoominMap::BattleRoom){
			battlestate.push_back(true);
			battleRoomLocation.push_back(TrueLode[i]);
			num++;
		}
	}
	battleMonster = std::vector<std::vector<MonsterData>>(battlestate.size());
	Mostercreate(difficulty);
	PlayerLocation = TrueLode[0];
}

void MapInformation::Mostercreate(Difficulty difficulty){
	srand(time(NULL));
	for (int i = 0; i < battleMonster.size(); i++){
	int MonsterPopulation = 0;
	int MonsterLever = 0;
	switch (difficulty)
	{
	case Easy:
		MonsterLever = 3;
		break;
	case Normal:
		MonsterLever = 15;
		break;
	case Hard:
		MonsterLever = 30;
		break;
	default:
		break;
	}
	switch (rand()%3)
	{
	case 0:
		MonsterPopulation = 1;
		break;
	case 1:
		MonsterPopulation = 2;
		MonsterLever = MonsterLever / 2;
		break;
	case 2:
		MonsterPopulation = 3;
		MonsterLever = MonsterLever / 3;
		break;
	default:
		break;
	}
	battleMonster.at(i) = std::vector<MonsterData>(MonsterPopulation);
		for (int k = 0; k < MonsterPopulation; k++){
			battleMonster.at(i).at(k).MonsterLever = MonsterLever;
			for (int m = 0; m < MonsterLever ; m++){
				battleMonster.at(i).at(k).MonsterMaxHp += rand() % 5;
				battleMonster.at(i).at(k).MonsterPower += rand() % 3;
			}
			battleMonster.at(i).at(k).MonsterMaxHp += 1;
			battleMonster.at(i).at(k).MonsterHp = battleMonster.at(i).at(k).MonsterMaxHp;
		}
	}
}

void MapInformation::setBattleState( bool went){
	for (int i = 0; i < battlestate.size() && 
		battleRoomLocation.at(i).X == PlayerLocation.X && battleRoomLocation.at(i).Y == PlayerLocation.Y; i++){
		battlestate.at(i) = went;
		return;
	}
}
void MapInformation::setBattleMonsterData(Location location,std::vector<MonsterData> monsterdata){
	for (int i = 0; i < battlestate.size() &&
		battleRoomLocation.at(i).X == location.X && battleRoomLocation.at(i).Y == location.Y; i++){
		battleMonster.at(i) = monsterdata;
		if (battleMonster.at(i).size() == 0){
			battlestate.at(i) = false;
			return;
		}
		return;
	}
}
void MapInformation::setPlayerLocation(Location location){
	PlayerLocation.X += location.X;
	PlayerLocation.Y += location.Y;
}
void MapInformation::setRoomInformation(Location location,RoominMap room){
	DungeonStructure[location.Y][location.X] = room;
}

Location MapInformation::getPlayerLocation(){
	return PlayerLocation;
}
std::vector<MonsterData> MapInformation::getBattlemonsterData(){
	for (int i = 0; i < battlestate.size(); i++){
		if (battleRoomLocation.at(i).X == PlayerLocation.X && battleRoomLocation.at(i).Y == PlayerLocation.Y){
			return battleMonster.at(i);
		}
	}
	printf("BattleMonstaer Error");
}
bool MapInformation::getBattleState(){
	for (int i = 0; i < battleRoomLocation.size(); i++){
		if (battleRoomLocation.at(i).X == PlayerLocation.X && battleRoomLocation.at(i).Y == PlayerLocation.Y){
			return battlestate.at(i);
		}
	}
	printf("BattleSate Error");
}
RoominMap MapInformation::getRoomInformation(Location location){
	return DungeonStructure[location.Y][location.X];
}
RoominMap MapInformation::getLocationMoved(Location location){
	if (PlayerLocation.Y, PlayerLocation.X >0 && PlayerLocation.Y, PlayerLocation.X < MapinRoomInformation.MapSize ){
		return DungeonStructure[PlayerLocation.Y + location.Y][PlayerLocation.X + location.X]; 
	}
	return BlockageRoom;
}
Difficulty MapInformation::getDifficulty(){
	return MapinRoomInformation.NowMapDifficulty;
}

void MapInformation::SeeMap(){
	system("cls");
	for (int i = 0; i < MapinRoomInformation.MapSize; i++){
		for (int k = 0; k < MapinRoomInformation.MapSize; k++){
			if (PlayerLocation.Y == i &&PlayerLocation.X == k){
				printf("p");
			}
			else{
				if (block[i][k]){ printf("%d", DungeonStructure[i][k]); }
				else{ printf("X"); }
			}
		}
		printf("\n");
	}
}

bool MapInformation::getsafelocation(Location location){
	if (PlayerLocation.X + location.X >= 0 && PlayerLocation.Y + location.Y >= 0){
		if (PlayerLocation.X + location.X < MapinRoomInformation.MapSize && PlayerLocation.Y + location.Y < MapinRoomInformation.MapSize){
			return true;
		}
	}
	else{
		return false;
	}
}

void RoomInformation::createMapDifficulty(Difficulty difficulty){
	switch (difficulty)
	{
	case Easy:
		RoomInformation::MapSize = 20;
		RoomInformation::LodeSize = 10;
		break;
	case Normal:
		RoomInformation::MapSize = 30;
		RoomInformation::LodeSize = 20;
		break;
	case Hard:
		RoomInformation::MapSize = 40;
		RoomInformation::LodeSize = 30;
		break;
	default:
		break;
	}
	createMapRoom(&MapSize, &LodeSize, &Roomorder);
}
/*맵의 방들을 설정한다.
트랩같은 경우 밟은 후에는 변화가 PeaceRoom로 바꾸자
*/
/*예의주시
std::vector<RoominMap>& create 주의 요망
*/
void createMapRoom(int* MapSize, int* LodeSize, std::vector<RoominMap>* create){
	srand(time(NULL));
	create->resize(*LodeSize + 5);
	switch (rand()%3)
	{
	case 0:
	{
		*MapSize -= 5;
		*LodeSize -= 5;
		for (int i = 0; i < *LodeSize; i++){
			switch (rand() % 3)
			{
			case 0:
				create->at(i) = RoominMap::TrapRoom;
				break;
			default:
				create->at(i) = RoominMap::BattleRoom;
				break;
			}
		}
		switch (rand() % 3)
		{
		case 0:
			create->at(*LodeSize - 1) = RoominMap::treasureBoxRoom;
			break;
		default:
			create->at(*LodeSize - 1) = RoominMap::BattleRoom;
			break;
		}
	}
	break;
	case 1:
	{
		int PeaseRoom = *LodeSize / 3;
		int loudRoom = *LodeSize - PeaseRoom;
		for (int i = 0; i < *LodeSize; i++){
			if (rand() % 2 && PeaseRoom){
				PeaseRoom--;
				create->at(i) = RoominMap::PeaceRoom;
			}
			else{
				switch (rand() % 2)
				{
				case 0:
					create->at(i) = RoominMap::BattleRoom;
					break;
				case 1:
					create->at(i) = RoominMap::TrapRoom;
					break;
				default:
					break;
				}
				loudRoom--;
			}
		}
		switch (rand() % 3)
		{
		case 0:
			create->at(*LodeSize - 1) = RoominMap::treasureBoxRoom;
			break;
		default:
			create->at(*LodeSize - 1) = RoominMap::BattleRoom;
			break;
		}
	}
	break;
	case 2:
	{
		*MapSize += 5;
		*LodeSize += 5;
		int PeaseRoom = *LodeSize / 2;
		int loudRoom = PeaseRoom + 3;
		for (int i = 0; i < *LodeSize; i++){
			if (rand() % 2 && PeaseRoom){
				PeaseRoom--;
				create->at(i) = RoominMap::PeaceRoom;
			}
			else if (loudRoom){
				switch (rand() % 2)
				{
				case 0:
					create->at(i) = RoominMap::BattleRoom;
					break;
				case 1:
					create->at(i) = RoominMap::TrapRoom;
					break;
				default:
					break;
				}
				loudRoom--;
			}
		}
		switch (rand()%3)
		{
		case 0:
			create->at(*LodeSize - 1) = RoominMap::treasureBoxRoom;
			break;
		default:
			create->at(*LodeSize - 1) = RoominMap::BattleRoom;
			break;
		}
	}
	break;
	default:
		break;
	}
	create->at(0) = RoominMap::entranceRoom;
}