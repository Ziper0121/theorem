#ifndef __ENTRYSCENE_H__
#define __ENTRYSCENE_H__
#include "PublicGoods.h"
#include "PlayScene.h"
#include <vector>
#include <string>


/*createDataUsageInterface의 return을 위한 함수
인터페이스에서 선택한 위치와 선택를 받는 변수
이동할 씬의 정보를 받는 변수
데이터를 받는지의 데한 정보를 받는 변수
*/
/*방에 대한 정보 넣는게 좋을 것같다 **저장한다면
*/
class Scene{
private:
public:
	bool StartandEnd();
	Scene(){
		Scene::PlayerSceneEcho.ScenePosition = GameScene_State::StartScene;
		if ((fopen("SaveSize.txt", "rt"))){

			FILE* SaveSize = fopen("SaveSize.txt", "rt");
			int connectcansize = fgetc(SaveSize)-48;
			if (connectcansize <= 0){
				FILE* Savesize = fopen("SaveSize.txt", "wt");
				char zero = '0';
				fputc(zero, Savesize);
				fopen("CharData0.txt", "wt");
				fopen("EquipmentData0.txt", "wt");
				Scene::SaveDataSize = 0;
				fclose(Savesize);
				return;
			}
			else if (connectcansize > 0){
				Scene::SaveDataSize = connectcansize;
			}
			fclose(SaveSize);
		}
		else{
			FILE* Savesize = fopen("SaveSize.txt", "wt");
			char zero = '0';
			fputc(zero, Savesize);
			Scene::SaveDataSize = 0;
			fclose(Savesize);
		}
	}
private:

	/*게임의 씬의 위치를 저장하는 변수
	*/
	PositionButton PlayerSceneEcho;
	GamePlay GamePlay;
	/*저장된 데이터의 계수를 저장하는 변수
	connectScene와 saveScene에 사용하는 변수이다.
	*/
	int SaveDataSize;

	/*플레이화면을 제외한 모든 게임의 화면을 표시하는 함수이다.
	*/
	void StartScene();
	void ConnectScene();
	void GamePlayScene();
	void DiedScene();
};
/*
주인공의 데이터를 저장한다면
게임의 진행상태
맵상태 장비의 상태 현재스탯의 상태
현재 잡고있던 몬스터의 체력과 공격력
*/
/*"createInterface" is create Interface
*/

/*Interface create fuctions
*/

#endif