#ifndef __ITEM_H__
#define __ITEM_H__
#include <vector>
enum Usagetype{ vacant,Armor, weapon };

struct ItemInformation
{
	const int Number;
	const int efficacy;
	const char* ItemName;
	const int Itemprice;
	const Usagetype itemtype;
};
class Allitem{
private:
	std::vector<ItemInformation> AllArmor;

	std::vector<ItemInformation> AllWeapon;
public:
	Allitem(){
		AllArmor.push_back({ 0, 3, "T-shirt", 90, Armor });
		AllArmor.push_back({ 1, 6, "Leather shirt", 450, Armor });
		AllArmor.push_back({ 2, 9, "Chain shirt", 2250, Armor });
		AllWeapon.push_back({ 0, 1, "Stick", 90, weapon });
		AllWeapon.push_back({ 1, 2, "dagger", 450, weapon });
		AllWeapon.push_back({ 2, 3, "Iron pole", 2250, weapon });
	}
	int getItemSize(Usagetype);
	ItemInformation getItemInformation(Usagetype, int);
};

#endif