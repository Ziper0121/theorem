#ifndef __LIFE_H__
#define __LIFE_H__
#include <cstdio>
#include <vector>
#include "Item.h"
//무작위 변경가능성
//전투의 진행방식은
/*
if(적의 HP,나의 HP)
적의 행동함수()
나의 행동함수()
이런식으로 할것이다.
*/
/*'Life' has Information of Life Class
Life Information
*/
struct MonsterData
{
	int MonsterLever;
	int MonsterHp;
	int MonsterMaxHp;
	int MonsterPower;
};
struct EquipmentInformation
{
	int ItemNumver;
	Usagetype ItemType;
};
struct FILFEPlaeyrInformation
{
	int ConnectLever;
	int ConnectMoney;
	int ConnectExperience;
	int ConnectPower;
	int ConnectMax_HP;
	int ConnectEquipmentSpace;
	std::vector<EquipmentInformation> ConnectEquipment;

};

class Life{
protected:
	bool Lifeanddeath;
	int Lever;
	int Experience;
	int Hp;
	int MAXHp;
public:
	/* 'Experience' that reises the Level
	*/
	void setHP(int);
	bool getLifeanddeath();
	int getLever();
	int getExperience();
	int getHP();
	int getMAXHp();
};



/*'Animal' has Information of 'Animal' class
'Animal' is Inherited 'Life' Class
'Animal' has Power
*/
class Animal : public Life{
protected:
	int Power;
public:
	void createAnimal();
	void createMonster(MonsterData);
	void setExperience(int);
	int getPower();
};

/*'Human' has Information of 'Human'
'Human' is Ingerited 'Animal' class
'Human' has equipment
*/
class Human : public Animal{
protected:
	std::vector<EquipmentInformation> HumanEquipment;
	int EquipmentSpace;
public:
	void Humancreate();
	void setequipment(int, Usagetype , int);

	int getequipmentSpace();
	std::vector<EquipmentInformation> getAllEquipment();
	int getequipment(int);
};
/*직업이 생길경우 어떡하게
map
이름
*/
class Player : public Human{
private:
	char* PlayerName;
	int Money;
public:
	void PlayerStartcreate();
	void Playerconnectcreate(FILFEPlaeyrInformation);
	
	void setMoney(int);
	
	char* getPayerName();
	int getMoney();
};

/*'plant' has Information of 'plant' class
'plant' is Inherited 'Life' Class
*/
class Plant :public Life{
public:
	void Plantcreate();
};

#endif