#include "EntryScene.h"
#include "GameRule.h"
#include "PlayScene.h"
#include <cstdio>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <istream>
#include <sstream>
#define ARRSIZE(num) (sizeof(num)/sizeof(int)) 
/*Scene타입이 선언되었을때 변수들을 초기화 해주는 함수이다.
*/
bool Scene::StartandEnd(){
	while (true){
		switch (Scene::PlayerSceneEcho.ScenePosition){
		case GameScene_State::End:
			return false;
		case GameScene_State::StartScene:
			Scene::StartScene();
			break;
		case GameScene_State::ConnectScene:
			Scene::ConnectScene();
			break;
		case GameScene_State::PlayScene:
			Scene::GamePlayScene();
			break;
		case GameScene_State::DiedScene:
			Scene::DiedScene();
			break;
		}
	}
}

void Scene::StartScene(){
	std::vector<std::string> StarteButtonText = { "Start", "Connect", "End" };
	int returnvariable[3] = { 3, 2, 0};
	Scene::PlayerSceneEcho.ScenePosition = (GameScene_State)createInterface("Game", StarteButtonText, returnvariable);
}
void Scene::ConnectScene(){
	char* printfconnect = "Connect";
	std::vector<std::string> ConnectButtonText;
	/*chardata를 SaveDataSize만큼 읽어오자
	내일 이거 꼭봐라
	stringstram으로 << "charData" << 숫자 << ".txt";
	*/
	if ((fopen("SaveSize.txt", "rt")) && Scene::SaveDataSize > 0){
		ConnectButtonText = std::vector<std::string>(Scene::SaveDataSize);
		for (int i = 0; i < Scene::SaveDataSize; i++){
			std::stringstream iposition;
			iposition << "CharData" << i << ".txt";
			std::string fileposition = iposition.str();
			std::ifstream charData = std::ifstream(fileposition.c_str(), std::ios::in);

			char instead[50];

			charData.getline(instead, 50);
			ConnectButtonText.at(i) = " Lver : ";
			ConnectButtonText.at(i).append(instead);

			charData.getline(instead, 50);
			ConnectButtonText.at(i).append(" mony : ");
			ConnectButtonText.at(i).append(instead);
			charData.close();
		}
		ConnectButtonText.push_back("Back");
		int* returnScenePosition = (int*)malloc(ConnectButtonText.size()*sizeof(int));
		for (int i = 0; i < ConnectButtonText.size(); i++){
			returnScenePosition[i] = i;
		}
		char* MessangText = "Do you want to start with Character data ?";
		int getReturnValue = createInterface(printfconnect, ConnectButtonText, returnScenePosition);
		if (getReturnValue == ConnectButtonText.size() - 1 && Scene::SaveDataSize == 1){
			Scene::PlayerSceneEcho.ScenePosition = GameScene_State::StartScene;
		}
		else if (getReturnValue == ConnectButtonText.size() - 1 && Scene::SaveDataSize >1){
			Scene::PlayerSceneEcho.ScenePosition = GameScene_State::StartScene;
		}
		else{
			GamePlay.createGamePlayScene(SaveDataSize);
			GamePlay.GameRule.connectAction(getReturnValue);
			Scene::PlayerSceneEcho.ScenePosition = GamePlay.Statusgame();
		}
	}
	else{
		ConnectButtonText.push_back("Back");
		int returnValue[1] = { 1 };
		Scene::PlayerSceneEcho.ScenePosition = (GameScene_State)createInterface(printfconnect, ConnectButtonText, returnValue);
	}
}
void Scene::GamePlayScene(){
	GamePlay.createGamePlayScene(SaveDataSize);
	Scene::PlayerSceneEcho.ScenePosition = GamePlay.Statusgame();
}
void Scene::DiedScene(){
	char* PrintfDied = "You Died";
	int returnValue[1] = { 1 };
	std::vector<std::string> ButtonText = { "StartScene" };
	Scene::PlayerSceneEcho.ScenePosition = (GameScene_State)createInterface(PrintfDied, ButtonText, returnValue);
}

