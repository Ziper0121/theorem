#include "Item.h"

int Allitem::getItemSize(Usagetype type){
	switch (type)
	{
	case Armor:
		return AllArmor.size();
	case weapon:
		return AllWeapon.size();
	default:
		break;
	}
}

ItemInformation Allitem::getItemInformation(Usagetype type, int num){
	switch (type)
	{
	case Armor:
		return AllArmor.at(num);
	case weapon:
		return AllWeapon.at(num);
	default:
		break;
	}
}