#ifndef __PUBLIC_GOODS_H__
#define __PUBLIC_GOODS_H__
#include <vector>
#include <string>
/*DougeonIn enum
*/
enum Charector_DungeonEnvironment{ Died = 0, Peace, Battle, Trap, DungeonExit, treasureBox };
enum Charector_DungeonActionScreen{ Attack, Rest, Map, Run, InBack, DungeonOut };

/*DougeonOut enum
*/
enum Charector_OutEnvironment{ Hotel, Shop, DungeonEntrance };
enum Charector_OutPlayActionScreen{ BuyScene, SellScene, Rooms, DungeonEnter, OutBack };

enum GameScene_State { End = 0, StartScene, ConnectScene, PlayScene, DiedScene };

struct PositionButton
{
	int ArrowPosition;
	GameScene_State ScenePosition;
	bool ProsandCons;
};


/*interface를 만드는 함수
가장 기본적인 interface를 만드는 함수이다.
*/
int createInterface(char*, std::vector<std::string>, int[]);

/*이곳은 위에 있는 interface를 만드는 함수보다 좀 무거운 함수이다
귀찮게 메모리 할당하고 하는 것이 귀찮아서 만든함수이다.
주로 재차묻는 메세지를 담당하는 함수이다.
*/
PositionButton createDataUsageInterface(char*, std::vector<std::string>, GameScene_State[2], char*);

/*추가적으로 물음이 필요할경우 쓰는 interface를 만드는 함수
return 을 해야하는 returnValue의 값이 print Text보다 작을 경우
빽버튼이 있는 함수
*/
bool createQuestionmessage(char*);

/*나중에 다 만들고 player를 불러 오지 말고 char*를 이용해서 한번에 Lever과 Hp등을 스크린에 띄우자
*/
char switchfuction(int , char []);

#endif