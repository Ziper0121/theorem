#ifndef __PLAYESCENE_H__
#define __PLAYESCENE_H__
#include "PublicGoods.h"
#include "GameRule.h"
/*행동과 씬의 차이점
Attect은 플레이어의 운동을 하는 것이다.
Environment은 현재 플레이어의 환경을 알려주는 것이다.
*/

struct GameMoving
{
	/*던전밖에 씬을 담은 변수들
	*/
	Charector_OutEnvironment PlayerOutEnvironment;
	Charector_OutPlayActionScreen PlayerOutActionScreen;

	/*던전안에 씬을 담은 변수들
	*/
	Charector_DungeonEnvironment PlayerDungeonEnvironment;
	Charector_DungeonActionScreen PlayerDungeonActionScreen;
};

/*다른 씬으로 넘길때 다양한 방법들
bool을 하여 옵션과 게임을 종료하는지를 확인하는 방법
while 을 돌리다가 return struct를 넘겨 그곳에서 씬을 이동하게 하는 방법
죽었을 경우와 option을 켯을 경우 두가지를 나눠야한다.
*/
class GamePlay{
public:
	void createGamePlayScene(int);

	GameScene_State Statusgame();

	PlayerInteraction GameRule;

private:
	bool PlayerInDungeon;
	GameMoving PlayerMoving;

	int savefilesize;

	/*던전 외부에 화면을 담은 씬들
	*/
	void PlayerHotelScene();
	void PlayerShopScene();
	void PlayerDungeonEntranceScene();

	/*던전 외부 액션 화면을 담은 씬들
	*/
	void ActionBuyScene();
	void ActionSellScene();
	void ActionRoomScene();
	void ActionDungeonEnterScene();

	/*던전 내부 환경의 화면을 담은 씬들
	*/
	void PlayerDiedScene();
	void PlayerPeacefulScene();
	void PlayerBattleScene();
	void PlayerTrapScene();
	void PlayerDungeonExitScene();
	void PlayertreasureBoxScene();

	/*던전 내부 액션 화면을 담은 씬들 
	*/
	void ActionAttackScene();
	void ActionRestScene();
	void ActionMapScene();
	void ActionRunScene();
	void ActionDungeonOutScene();
};

int createPlayerInterface(Player, char*, std::vector<std::string>, int[]);
/*플레이어의 행동을 좀더 신중하게 하기 위해 추가적으로 묻는 메세지
*/

/*switch의 값을 리턴하는 함수
*/

#endif