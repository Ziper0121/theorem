#ifndef __GAME_RULE_H__
#define __GAME_RULE_H__
#include "Life.h"
#include "Map.h"
#include "PublicGoods.h"

/*
Died , Peace, Battle, Trap, DungeonExit ;
Attack, Rest, Map, Run, Back, DungeonOut ;

Shop, DungeonEntrance ;
eBuyScene, SellScene, DungeonEnter ;
*//*PlayScene에서 선택된 값을 받아서 그 값에 맞는 데이터를 처리하는 class
*/
class PlayerInteraction{
private:

	bool treasureBoxExistence;

	std::vector<Animal> Monster;
	std::vector<MonsterData> data;
public:
	MapInformation PlayerMap;

	void Battlestart();
	void BattleEnd();

	void BuyequipmentWearAction(ItemInformation);

	void AttackAction(int);
	bool zapAction();
	void RestAction();
	void MapAction();
	void treasureBoxRoomAction();
	bool TrapAction();
	void SellAction(int);
	void SaveAction(int);
	void connectAction(int);

	std::vector<std::string> getstringequipmentstate();
	Charector_DungeonEnvironment RunState(Location);
	
	Player GamePlayer;
	
	std::vector<MonsterData> getdata();
};

#endif