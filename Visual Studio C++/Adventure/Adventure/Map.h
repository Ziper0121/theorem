#ifndef __MAP_H__
#define __MAP_H__
#include <vector>
#include "Life.h"
/*Mpa을 크기와 모양을 변경하고 싶어도 나중에 하자
Map에 추가하고 싶은 것들
난이도 설정
맵의 상태들 :
내가 다녀온 길인지?
방의 정보 :
몬스터가 있는 방
평화로운 방 
덫이 있는 방인지
*/
enum Difficulty{ Easy, Normal, Hard };
enum RoominMap{ BlockageRoom, PeaceRoom, BattleRoom, TrapRoom, treasureBoxRoom, entranceRoom };
struct Location
{
	int Y;
	int X;
};
/*맵의 난이도와 방의 구조의 정보를 가진 struct
*/
struct RoomInformation
{
	// 전체 맵의 사이즈
	int MapSize = 0;
	// 길의 길이
	int LodeSize = 0;

	std::vector<RoominMap> Roomorder;

	Difficulty NowMapDifficulty;

	void createMapDifficulty(Difficulty);
};
/*
*/
class MapInformation{
private:
	/*던전의 방의 대한 정보를 담은 변수
	*/
	RoomInformation MapinRoomInformation;
	RoominMap DungeonStructure[45][45];
	Location PlayerLocation;

	bool block[45][45];
	/*
	location이 
	*/
	std::vector<bool> battlestate;
	std::vector<Location> battleRoomLocation;
	std::vector<std::vector<MonsterData>> battleMonster;

	/*길이 만들어진 순서를 저장하는 변수
	길의 크기라고 할수있다.
	*/
	Location TrueLode[36];
public:
	void Randomcreate(Difficulty);
	void Mostercreate(Difficulty);
	
	void setPlayerLocation(Location);
	void setRoomInformation(Location , RoominMap );
	void setBattleState(bool);
	void setBattleRoomMonster(Location,int);
	void setBattleMonsterData(Location, std::vector<MonsterData> );
	
	Difficulty getDifficulty();
	RoominMap getRoomInformation(Location);
	RoominMap getLocationMoved(Location);
	Location getPlayerLocation();
	bool getBattleState();
	std::vector<MonsterData> getBattlemonsterData();
	void SeeMap();
	bool getsafelocation(Location location);
};
void createMapRoom(int*, int*, std::vector<RoominMap>*);
#endif