#include "PlayScene.h"
#include "Map.h"
#include <conio.h>
#include <sstream>
#include <functional>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <ostream>
#include <istream>
#define OUT false
#define IN true

/*
여것을 저장하자
*/

/*임시용 나중에 GameRule과 합칠것이다. 그때
Player에 connect를 불러올것이다. 
*/
void GamePlay::createGamePlayScene(int filesize){
	GamePlay::savefilesize = filesize;
	GamePlay::GameRule.GamePlayer.PlayerStartcreate();
	GamePlay::PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::DungeonEntrance;
	GamePlay::PlayerMoving.PlayerDungeonEnvironment = Charector_DungeonEnvironment::DungeonExit;
	GamePlay::PlayerInDungeon = OUT;
}

GameScene_State GamePlay::Statusgame(){
	while (true){
		switch (PlayerInDungeon)
		{
		case IN:
			switch (PlayerMoving.PlayerDungeonEnvironment)
			{
			case Charector_DungeonEnvironment::Died:
				PlayerDiedScene();
				return StartScene;
			case Charector_DungeonEnvironment::Peace:
				PlayerPeacefulScene();
				break;
			case Charector_DungeonEnvironment::Battle:
				PlayerBattleScene();
				break;
			case Charector_DungeonEnvironment::Trap:
				PlayerTrapScene();
				break;
			case Charector_DungeonEnvironment::DungeonExit:
				PlayerDungeonExitScene();
				break;
			case Charector_DungeonEnvironment::treasureBox:
				PlayertreasureBoxScene();
				break;
			default:
				break;
			}
			switch (PlayerMoving.PlayerDungeonActionScreen)
			{
			case Charector_DungeonActionScreen::Attack:
				ActionAttackScene();
				break;
			case Charector_DungeonActionScreen::Rest:
				ActionRestScene();
				break;
			case Charector_DungeonActionScreen::Map:
				ActionMapScene();
				break;
			case Charector_DungeonActionScreen::Run:
				ActionRunScene();
				break;
			case Charector_DungeonActionScreen::InBack:
				break;
			case Charector_DungeonActionScreen::DungeonOut:
				ActionDungeonOutScene();
				break;
			default:
				break;
			}
			break;
			/*PlayerInDuneon Out
			*/
		case OUT:
			switch (PlayerMoving.PlayerOutEnvironment){
			case Charector_OutEnvironment::Hotel:
				PlayerHotelScene();
				break;
			case Charector_OutEnvironment::Shop:
				PlayerShopScene();
				break;
			case Charector_OutEnvironment::DungeonEntrance:
				PlayerDungeonEntranceScene();
				break;
			}
			switch (PlayerMoving.PlayerOutActionScreen){
			case Charector_OutPlayActionScreen::BuyScene:
				ActionBuyScene();
				break;
			case Charector_OutPlayActionScreen::SellScene:
				ActionSellScene();
				break;
			case Charector_OutPlayActionScreen::Rooms:
				ActionRoomScene();
				break;
			case Charector_OutPlayActionScreen::DungeonEnter:
				ActionDungeonEnterScene();
				break;
			case Charector_OutPlayActionScreen::OutBack:
				break;
			}
			break;
			/*PlayerInDuneon default
			*/
		default:
			break;
		}
	}
	return StartScene;
}
/*던전 외부에 화면을 담은 씬들
*/
void GamePlay::PlayerHotelScene(){
	char* Hotelprintsign = "It is in the Hotel. Where are you going?";
	std::vector<std::string> HotelButtontext = { "shop", "Rooms", "DungeonEnter" };
	int ReturnButtonValue[3] = { 0, 2, 1 };
	std::stringstream streamprice;
	int RoomPrice = GameRule.GamePlayer.getLever() * 5;
	streamprice << HotelButtontext.at(1).c_str() << " : " <<RoomPrice;
	switch (createPlayerInterface(GamePlay::GameRule.GamePlayer, Hotelprintsign, HotelButtontext, ReturnButtonValue))
	{
	case 0:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Shop;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
		break;
	case 2:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::Rooms;
		break;
	case 1:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::DungeonEntrance;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
		break;
	default:
		break;
	}
}
void GamePlay::PlayerShopScene(){
	char* Shopprintsign = "It is in the shop . Are you actually buying the item? Are you selling it?";
	std::vector<std::string> Buttontext = { "ItemBuy", "ItemSell", "Back" };
	int ReturnButtonValue[3] = { 0, 1, 4 };
	int getReturn = createPlayerInterface(GamePlay::GameRule.GamePlayer, Shopprintsign, Buttontext, ReturnButtonValue);
	switch (getReturn){
	case 4:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
	default:
		PlayerMoving.PlayerOutActionScreen = (Charector_OutPlayActionScreen)getReturn;
		break;
	}
}
void GamePlay::PlayerDungeonEntranceScene(){
	char* DungeonEntrancesign = "It is in front of the dungeon. Where do you want to go?";
	std::vector<std::string> Buttontext = { "hotel", "Dungeon" };
	int ReturnButtonValue[2] = { 0, 1 };
	switch (createPlayerInterface(GamePlay::GameRule.GamePlayer, DungeonEntrancesign, Buttontext, ReturnButtonValue))
	{
	case 0:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
		break;
	case 1:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::DungeonEntrance;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::DungeonEnter;
		break;
	default:
		break;
	}
}

/*던전 외부 액션 화면을 담은 씬들
*/
void GamePlay::ActionBuyScene(){
	Allitem allitem;
	char* ItemBuysign = "Choose the item you want to buy";
	std::vector<std::string> BuyButtontext;
	for (int i = 0; i < allitem.getItemSize(Armor); i++){
		std::stringstream streamButton;
		streamButton << allitem.getItemInformation(Usagetype::Armor, i).ItemName << " : " << allitem.getItemInformation(Usagetype::Armor, i).Itemprice;
		BuyButtontext.push_back(streamButton.str());
	}
	for (int i = 0; i < allitem.getItemSize(weapon); i++){
		std::stringstream streamButton;
		streamButton << allitem.getItemInformation(Usagetype::weapon, i).ItemName << " : " << allitem.getItemInformation(Usagetype::weapon, i).Itemprice;
		BuyButtontext.push_back(streamButton.str());
	}
	BuyButtontext.push_back("Back");
	int* BuyreturnButton = (int*)malloc(BuyButtontext.size() * sizeof(int));
	for (int i = 0; i < BuyButtontext.size(); i++){
		BuyreturnButton[i] = i;
	}
	int returnButton = createPlayerInterface(GamePlay::GameRule.GamePlayer, ItemBuysign, BuyButtontext, BuyreturnButton);

	if (returnButton == BuyButtontext.size() - 1){
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Shop;
		return;
	}
	else{
		ItemInformation* ItemData;
		if (returnButton < allitem.getItemSize(Usagetype::Armor)){
			ItemInformation insteadData = allitem.getItemInformation(Usagetype::Armor, returnButton);
			ItemData = &insteadData;
		}
		else{
			ItemInformation insteadData = allitem.getItemInformation(Usagetype::weapon, returnButton - allitem.getItemSize(Usagetype::Armor));
			ItemData = &insteadData;
		}
		if (ItemData->Itemprice > GameRule.GamePlayer.getMoney()){
			printf("I do not have enough money.");
			getch();
			PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Shop;
			PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
		}
		else{
			GameRule.BuyequipmentWearAction(*ItemData);
		}
	}
}
void GamePlay::ActionSellScene(){
	char* Sellprintsign = "What item are you going to sell?";
	std::vector<std::string> SellButtonText = GameRule.getstringequipmentstate();
	SellButtonText.push_back("Back");
	int* ReturnButtonValue = (int*)malloc(SellButtonText.size()*sizeof(int));
	for (int i = 0; i < SellButtonText.size(); i++){
		ReturnButtonValue[i] = i;
	}
	int getReturnValue = createPlayerInterface(GamePlay::GameRule.GamePlayer, Sellprintsign, SellButtonText, ReturnButtonValue);
	if (getReturnValue == SellButtonText.size() - 1){
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Shop;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
	}
	else{
		GameRule.SellAction(getReturnValue);
	}
}
void GamePlay::ActionRoomScene(){
	// 레벨에 따라 돈을 더받능 시스템으로 가자 그럼 잘자 내일이거 꼭봐라
	char* Roomprintsign = "Do you want to save?";
	std::vector<std::string>  RoomButtonText;
	int* Returnvalue =0;
	int RoomPrice = GameRule.GamePlayer.getLever()*5;
	if (RoomPrice > GameRule.GamePlayer.getMoney()){
		printf("\nYou do not have much money.");
		getch();
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
	}
	else{
		GameRule.GamePlayer.setMoney(-RoomPrice);
		if (savefilesize > 0){
			RoomButtonText = std::vector<std::string>(savefilesize);
			for (int i = 0; i < savefilesize; i++){
				std::stringstream iposition;
				iposition << "CharData" << i << ".txt";
				std::string fileposition = iposition.str();
				std::ifstream charData = std::ifstream(fileposition.c_str(), std::ios::in);

				char instead[50];

				charData.getline(instead, 50);
				RoomButtonText.at(i) = " Lver : ";
				RoomButtonText.at(i).append(instead);

				charData.getline(instead, 50);
				RoomButtonText.at(i).append(" mony : ");
				RoomButtonText.at(i).append(instead);
				charData.close();
			}
			RoomButtonText.push_back("Blank");
			RoomButtonText.push_back("Back");

			Returnvalue = (int*)malloc(RoomButtonText.size()*sizeof(int*));
			for (int i = 0; i < RoomButtonText.size(); i++){
				Returnvalue[i] = i;
			}

		}
		else{
			RoomButtonText.push_back("Blank");
			RoomButtonText.push_back("Back");
			Returnvalue = (int*)malloc(RoomButtonText.size()*sizeof(int*));
			for (int i = 0; i < RoomButtonText.size(); i++){
				Returnvalue[i] = i;
			}
		}
		int getReturnvalue = createInterface(Roomprintsign, RoomButtonText, Returnvalue);
		if (getReturnvalue == RoomButtonText.size() - 1){
			PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
			PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
		}
		else{
			if (getReturnvalue == RoomButtonText.size() - 2){
				savefilesize++;
			}
			if (createQuestionmessage("Do you want to save the data here?")){
				GameRule.SaveAction(getReturnvalue);
				if (savefilesize == 0){
					savefilesize++;
				}
			}
			else{
				PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
				PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
			}
		}
		free(Returnvalue);
	}
}
void GamePlay::ActionDungeonEnterScene(){
	char* DungeonEnterprintsign = "Set the Difficulty in Dungeons";
	std::vector<std::string> DungeonEnterButtonText = { "Easy", "Normal", "Hard","Back" };
	int ReturnButtonValue[4] = { 0, 1,2,3 };
	switch (createPlayerInterface(GamePlay::GameRule.GamePlayer, DungeonEnterprintsign, DungeonEnterButtonText, ReturnButtonValue))
	{
	case 0:
		PlayerInDungeon = IN;
		GamePlay::GameRule.PlayerMap.Randomcreate(Difficulty::Easy);
		break;
	case 1:
		PlayerInDungeon = IN;
		GamePlay::GameRule.PlayerMap.Randomcreate(Difficulty::Normal);
		break;
	case 2:
		PlayerInDungeon = IN;
		GamePlay::GameRule.PlayerMap.Randomcreate(Difficulty::Hard);
		break;
	case 3:
		PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::Hotel;
		PlayerMoving.PlayerOutActionScreen = Charector_OutPlayActionScreen::OutBack;
		break;
	default:
		break;
	}
}

/*던전 내부 환경의 화면을 담은 씬들
*/
void GamePlay::PlayerDiedScene(){
	char* Diedprintsign = "You Died";
	std::vector<std::string> Buttontext = { "StarteScene" };
	int ReturnButtonValue[1] = { 5 };
	PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)createPlayerInterface(GamePlay::GameRule.GamePlayer, Diedprintsign, Buttontext, ReturnButtonValue);
}
void GamePlay::PlayerPeacefulScene(){
	char* Peaceprintsign = "Peaceful state. What choice do you make?";
	std::vector<std::string> Buttontext = { "Rest", "Run", "Map", "Back" };
	int ReturnButtonValue[4] = { 1, 3,2, 4 };
	PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)createPlayerInterface(GamePlay::GameRule.GamePlayer,Peaceprintsign, Buttontext, ReturnButtonValue);
}
void GamePlay::PlayerBattleScene(){
	char* Battleprintsign = "Monster appeared. What choice do you make?";
	std::vector<std::string> Buttontext = { "Rest", "Attack", "Run", "Map", "Back" };
	int ReturnButtonValue[5] = {1,0,3,2,4};
	int getReturnVaule = createPlayerInterface(GamePlay::GameRule.GamePlayer, Battleprintsign, Buttontext, ReturnButtonValue);
	switch (getReturnVaule)
	{
	case 1:
		if (GameRule.zapAction()){
			PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)getReturnVaule;
		}
		else{
			PlayerMoving.PlayerDungeonEnvironment = Charector_DungeonEnvironment::Died;
			PlayerMoving.PlayerDungeonActionScreen = Charector_DungeonActionScreen::InBack;
		}
		break;
	default:
		PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)getReturnVaule;
		break;
	}
}
void GamePlay::PlayerTrapScene(){
	char* Trapprintsign = "It's a trap. What choice do you make?";
	std::vector<std::string> TrapButtontext = { "Rest", "Run", "Map", "Back", };
	int ReturnButtonValue[4] = { 1, 3, 2, 4 };
	PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)createPlayerInterface(GamePlay::GameRule.GamePlayer, Trapprintsign, TrapButtontext, ReturnButtonValue);
}
void GamePlay::PlayerDungeonExitScene(){
	char* Exitprintsign = "Dungeon in and out. What choice do you make?";
	std::vector<std::string> ExitButtonText = { "Rest", "Run", "Map", "Back"," DungeonOut" };
	int ReturnButtonValue[5] = { 1, 3, 2, 4 ,5};
	PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)createPlayerInterface(GamePlay::GameRule.GamePlayer, Exitprintsign, ExitButtonText, ReturnButtonValue);
}

void GamePlay::PlayertreasureBoxScene(){
	char* treasureBoxprintsign = "It is a room with a treasure chest. What choice do you make?";
	std::vector<std::string> treasureBoxButtonText = { "Rest", "Run", "Map", "Back"};
	int ReturnButtonValue[5] = { 1, 3, 2, 4 };
	PlayerMoving.PlayerDungeonActionScreen = (Charector_DungeonActionScreen)createPlayerInterface(GamePlay::GameRule.GamePlayer, treasureBoxprintsign, treasureBoxButtonText, ReturnButtonValue);
}
/*던전 내부 액션 화면을 담은 씬들
*/
/*액션을 어떡할지 고민이다
ActionAttackScene에서 전투를 모두 처리할지
아니면 추가로 액션만 하는 함수를 만들지
*/
void GamePlay::ActionAttackScene(){
	int arrReturnsize = GameRule.getdata().size();
	if (arrReturnsize > 0){
	char* Attackprintsign = "Which Monster to Attack";
	std::vector<std::string> AttackButtontext = std::vector<std::string>(GameRule.getdata().size());
	for (int i = 0; i < arrReturnsize; i++){
		std::stringstream stramButtontext;
			stramButtontext << "Number: " << i << " HP: " << GameRule.getdata().at(i).MonsterHp;
			AttackButtontext.at(i) = stramButtontext.str();
		}
		int* ReturnButtonValue = (int*)malloc(arrReturnsize*sizeof(int));
		for (int i = 0; i < arrReturnsize; i++){
			ReturnButtonValue[i] = i;
		}
		GameRule.AttackAction(createPlayerInterface(GamePlay::GameRule.GamePlayer, Attackprintsign, AttackButtontext, ReturnButtonValue));
		free(ReturnButtonValue);

		if (GameRule.zapAction()){
			PlayerMoving.PlayerDungeonEnvironment = Charector_DungeonEnvironment::Battle;
		}
		else{
			PlayerMoving.PlayerDungeonEnvironment = Charector_DungeonEnvironment::Died;
		}
	}
	else{
		printf("\nThere are no living monsters.");
		PlayerMoving.PlayerDungeonEnvironment = Charector_DungeonEnvironment::Battle;
		getch();
	}
}
void GamePlay::ActionRestScene(){
	GameRule.RestAction();
}
void GamePlay::ActionMapScene(){
	GameRule.MapAction();
}
void GamePlay::ActionRunScene(){
	printf("\nChoose where you want to go\n");
	printf("Up : 8  Down : 2  Left : 4  Right : 6\n");
	int switcharrsize = 4;
	char switcharr[4] = { '8', '2', '4', '6' };
	Location Direction;
	switch (switchfuction(switcharrsize, switcharr)){
	case '8':
		Direction.X = 0;
		Direction.Y = -1;
		break;
	case '2':
		Direction.X = 0;
		Direction.Y = 1;
		break;
	case '4':
		Direction.X = -1;
		Direction.Y = 0;
		break;
	case '6':
		Direction.X = 1;
		Direction.Y = 0;
		break;
	default:
		break;
	}
	PlayerMoving.PlayerDungeonEnvironment =	GameRule.RunState(Direction);
}
void GamePlay::ActionDungeonOutScene(){
	PlayerMoving.PlayerOutEnvironment = Charector_OutEnvironment::DungeonEntrance;
	PlayerInDungeon = OUT;
}

int createPlayerInterface(Player Playerdata, char* printfsign, std::vector<std::string> ButtonText, int ReturnButtonValue[]){
	int LineBreak = 4;
	int Arrow = 0;

	int switcharrsize = 5;
	char switcharr[5] = { '0', '8', '2','4','6' };

	int CharectorData[5] = { Playerdata.getLever(), Playerdata.getMAXHp(), Playerdata.getHP(), Playerdata.getPower(), Playerdata.getMoney() };
	char* PlayerSign[5] = { "Lever : ", "Max Hp : ", "Hp : ", "Power : ", "Money : " };
	while (true)
	{
		system("cls");
		for (int i = 0; i < 5; i++){
			printf(PlayerSign[i]);
			printf("%d\n", CharectorData[i]);
		}
		printf("\n\n\n\n %s \n\n\n\n\n\n\n\n\n\n", printfsign);
		printf("     ");
		for (int i = 0; i < ButtonText.size(); i++){
			printf("  ");
			printf(ButtonText.at(i).c_str());
			if (Arrow == i){
				printf("<-");
			}
			if (i == LineBreak-1){
				LineBreak += 4;
				printf("\n");
			}
		}
		LineBreak = 4;
		switch (switchfuction(switcharrsize, switcharr))
		{
		case '0':
			return ReturnButtonValue[Arrow];
		case '8':
			if (0 <= Arrow - LineBreak){ Arrow -= LineBreak; }
			break;
		case '2':
			if (ButtonText.size() - 1 >= Arrow + LineBreak){ Arrow += LineBreak; }
			break;
		case '4':
			if (0 < Arrow){ --Arrow; }
			break;
		case '6':
			if (ButtonText.size() - 1 > Arrow){ ++Arrow; }
			break;
		default:
			break;
		}
	}
}


/*switch의 값을 리턴하는 함수
나중에 이 cpp 파일이 GameRule로 옴겨지면 switchfuction1함수를 없애고 그곳에 있는 switchfuction함수를 사용하자 
*/