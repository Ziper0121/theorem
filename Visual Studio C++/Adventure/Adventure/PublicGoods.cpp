#include "PublicGoods.h"
#include <conio.h>
#include <ctime>


int createInterface(char* printSign, std::vector<std::string> ButtonText, int returnValue[]){
	int Arrow = 0;
	int switcharrsize = 3;
	char switcharr[3] = { '0', '8', '2' };
	while (true){
		system("cls");
		printf("\n\n\n\n");
		printf("                                    ");
		printf(printSign);
		printf("\n\n\n\n\n");
		for (int i = 0; i < ButtonText.size(); i++){
			if (Arrow == i){
				printf("   ");
			}
			printf("                                    ");
			printf(ButtonText.at(i).c_str());
			if (Arrow == i){
				printf("<-");
			}
			printf("\n");
		}
		switch (switchfuction(switcharrsize, switcharr)){
		case '0':
			return returnValue[Arrow];
		case '2':
			/*arrsize가 0부터 시작하는 숫자가 아니기 때문에 -1을 하였다.
			*/
			if (Arrow < ButtonText.size() - 1){ Arrow += 1; }
			break;
		case '8':
			if (Arrow > 0){ Arrow -= 1; }
			break;
		}
	}
}
PositionButton createDataUsageInterface(char* PrintSign, std::vector<std::string> Playerprofile, GameScene_State ButtonScene[2], char* MessageSign){
	int Arrow = 0;
	int switcharrsize = 3;
	char switcharr[3] = { '0', '8', '2' };
	while (true){
		system("cls");
		printf("\n\n\n\n");
		printf("                                    ");
		printf(PrintSign);
		printf("\n\n\n\n\n");
		for (int i = 0; i < Playerprofile.size(); i++){
			printf("                                    ");
			if (Arrow == i){
				printf("  ");
			}
			printf(Playerprofile.at(i).c_str());
			if (Arrow == i){
				printf("<-");
			}
			printf("\n");
		}
		switch (switchfuction(switcharrsize, switcharr))
		{
		case '0':
			PositionButton PB;
			PB.ArrowPosition = Arrow;
			if (Arrow == Playerprofile.size() - 1){
				PB.ScenePosition = ButtonScene[0];
			}
			else{
				PB.ScenePosition = ButtonScene[1];
				PB.ProsandCons = createQuestionmessage(MessageSign);
			}
			return PB;
		case '8':
			if (0 < Arrow){ --Arrow; }
			break;
		case '2':
			if (Playerprofile.size() - 1 > Arrow){ ++Arrow; }
			break;
		}
	}
}

bool createQuestionmessage(char* printsign){
	int Arrow = 0;
	char* YesNo[2] = { "Yes", "No" };
	bool Presence[2] = { true, false };
	int switcharrsize = 3;
	char switcharr[3] = { '0', '8', '2' };
	while (true){
		system("cls");
		printf("\n\n\n\n");
		printf("                                    ");
		printf(printsign);
		printf("\n\n\n\n\n");
		for (int i = 0; i < 2; i++){
			printf("                                    ");
			if (Arrow == i) { printf(" "); }
			printf("%s", YesNo[i]);
			if (Arrow == i) { printf("<-"); }
			printf("\n");
		}
		switch (switchfuction(switcharrsize, switcharr)){
		case '0':
			return Presence[Arrow];
		case '2':
			/*arrsize가 0부터 시작하는 숫자가 아니기 때문에 -1을 하였다.
			*/
			if (Arrow < 1){ Arrow++; }
			break;
		case '8':
			if (Arrow > 0){ Arrow--; }
			break;
		}
	}
	return true;
}

char switchfuction(int size, char arr[]){
	int PlayerKey = 0;
	while (true){
		int num = getch();
		for (int i = 0; i < size ; i++){
			if (num == arr[i]){
				return arr[i];
			}
		}
	}
}