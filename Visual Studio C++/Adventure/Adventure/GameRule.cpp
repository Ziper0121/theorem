#include "GameRule.h"
#include "PlayScene.h"
#include <ctime>
#include <sstream>
#include <conio.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <ostream>
#include <istream>

//Attack, Rest, Map, Run, Back, DungeonOut 
/*
Died , Peace, Battle, Trap, DungeonExit ;
Attack, Rest, Map, Run, Back, DungeonOut ;

Shop, DungeonEntrance ;
eBuyScene, SellScene, DungeonEnter ;
*/
/* PlayScene에 switch에서 설정하는 것과
PlayScene에 함수내부에서 설정을 하거나
*/

void PlayerInteraction::BuyequipmentWearAction(ItemInformation ItemData){
	char* mountingprintsging = "Where to install.";

	std::vector<std::string> PlayerEquipmentState = getstringequipmentstate();
	int* ReturnButtonValue = (int*)malloc(GamePlayer.getequipmentSpace()* sizeof(int));

	for (int i = 0; i < GamePlayer.getequipmentSpace(); i++){
		ReturnButtonValue[i] = i;
	}
	int EquipmentPostion = createPlayerInterface(GamePlayer, mountingprintsging, PlayerEquipmentState, ReturnButtonValue);
	GamePlayer.setMoney(-ItemData.Itemprice);

	GamePlayer.setequipment(EquipmentPostion, ItemData.itemtype, ItemData.Number);
}
void PlayerInteraction::connectAction(int postion){
	std::stringstream CharDatafilenumber;
	FILFEPlaeyrInformation charconnect;
	CharDatafilenumber << "CharData" << postion << ".txt";
	std::string getfilename = CharDatafilenumber.str();
	std::ifstream file = std::ifstream(getfilename.c_str(),std::ios::in);

	std::stringstream Equipmentfilenumber;
	Equipmentfilenumber << "EquipmentData" << postion << ".txt";
	getfilename = Equipmentfilenumber.str();
	std::ifstream Equipmentfile = std::ifstream(getfilename.c_str(), std::ios::in);

	char data[8];
	int transint[6];
	for (int i = 0; i < 6; i++){
		file.getline(data, 8);
		transint[i] = atoi(data);
	}

	charconnect.ConnectLever = transint[0];
	charconnect.ConnectMoney = transint[1];
	charconnect.ConnectExperience = transint[2];
	charconnect.ConnectPower = transint[3];
	charconnect.ConnectMax_HP = transint[4];
	charconnect.ConnectEquipmentSpace = transint[5];
	GamePlayer.setHP(transint[4]);

	charconnect.ConnectEquipment = std::vector<EquipmentInformation>(transint[5]);
	int num = 0;
	for (int i = 0; i < transint[5] * 2; i++){
		Equipmentfile.getline(data, 8);
		transint[0] = atoi(data);
		switch (i%2)
		{
		case 0:
			charconnect.ConnectEquipment.at(num).ItemType = (Usagetype)transint[0];
			break;
		case 1:
			charconnect.ConnectEquipment.at(num).ItemNumver = transint[0];
			num++;
			break;
		default:
			break;
		}
	}

	GamePlayer.Playerconnectcreate(charconnect);
}

void PlayerInteraction::AttackAction(int object){
	srand(time(NULL));
	int PlayerDamage = GamePlayer.getPower() * (rand() % 3);
	Monster.at(object).setHP(-PlayerDamage);
	data.at(object).MonsterHp -= PlayerDamage;
	printf("\nThe player hit '%d' %d damage", object, PlayerDamage);
	if (!Monster.at(object).getLifeanddeath()){
		int setplayerexperience = Monster.at(object).getLever()*rand() % 3;
		printf("\n\nI killed %d monster and got experience", setplayerexperience);
		GamePlayer.setExperience(setplayerexperience);
		getch();
		GamePlayer.setExperience(Monster.at(object).getLever());
		Monster.erase(Monster.begin() + object);
		data.erase(data.begin() + object);
	}
	if (Monster.size() == 0){
		PlayerMap.setBattleState(false);
	}
}

bool PlayerInteraction::zapAction(){
	srand(time(NULL));
	int Damagereceived = 0;
	for (int i = 0; i < Monster.size(); i++){
		Damagereceived = Monster.at(i).getPower() * (rand() % 3);
		GamePlayer.setHP(-Damagereceived);
		printf("\nThe Player suffered %d Damage", Damagereceived);
	}
	if (GamePlayer.getLifeanddeath()){
		getch();
		return true;
	}
	else{
		printf("\n\nit's so sick?");
		getch();
		return false;
	}
}

void PlayerInteraction::RestAction(){
	srand(time(NULL));
	int Restvalue = rand() % GamePlayer.getMAXHp();
	printf("\nYou have regained health as much as %d", Restvalue);
	GamePlayer.setHP(Restvalue);
	getch();
}
void PlayerInteraction::MapAction(){
	PlayerMap.SeeMap();
	printf("\n Back?");
	getch();
}
void PlayerInteraction::Battlestart(){
	data = PlayerMap.getBattlemonsterData();
	Monster = std::vector<Animal>(data.size());
	for (int i = 0; i < data.size(); i++){
		Monster.at(i).createMonster(data.at(i));
	}
}
void PlayerInteraction::BattleEnd(){
	for (int i = 0; i < data.size(); i++){
		data.at(i).MonsterHp = Monster.at(i).getHP();
		data.at(i).MonsterLever = Monster.at(i).getLever();
		data.at(i).MonsterMaxHp = Monster.at(i).getMAXHp();
		data.at(i).MonsterPower = Monster.at(i).getPower();
	}
	PlayerMap.setBattleMonsterData(PlayerMap.getPlayerLocation(), data);
}

void PlayerInteraction::SaveAction(int filrpart){
	int getfilesize;
	if ((fopen("SaveSize.txt", "r"))){
		std::vector<std::stringstream> playerdata = std::vector<std::stringstream>(6);

		std::stringstream CharfilePosition;
		CharfilePosition << "CharData" << filrpart << ".txt";
		std::string filename = CharfilePosition.str();
		std::ofstream charData = std::ofstream(filename.c_str(), std::ios::in | std::ios::trunc);

		std::stringstream equimentfilePosition;
		equimentfilePosition << "EquipmentData" << filrpart << ".txt";
		filename = equimentfilePosition.str();
		std::ofstream equipmentfile = std::ofstream(filename.c_str(), std::ios::in | std::ios::trunc);

		playerdata.at(0) << GamePlayer.getLever();
		playerdata.at(1) << GamePlayer.getMoney();
		playerdata.at(2) << GamePlayer.getExperience();
		playerdata.at(3) << GamePlayer.getPower();
		playerdata.at(4) << GamePlayer.getMAXHp();
		playerdata.at(5) << GamePlayer.getequipmentSpace();
		for (int i = 0; i < playerdata.size(); i++){
			std::string str = playerdata.at(i).str();
			charData.write(str.c_str(), str.size());
			charData.put('\n');
		}
		int num = 0;
		for (int i = 0; i < GamePlayer.getequipmentSpace() * 2; i++){
			std::stringstream equipmentData;
			switch (i%2)
			{
			case 0:
				equipmentData << GamePlayer.getAllEquipment().at(num).ItemType;
				break;
			case 1:
				equipmentData << GamePlayer.getAllEquipment().at(num).ItemNumver;
				num++;
				break;
			default:
				break;
			}
			std::string getstream = equipmentData.str();
			equipmentfile.write(getstream.c_str(), getstream.size());
			equipmentfile.put('\n');
		}
		FILE* savefilesize = fopen("SaveSize.txt", "r");
		getfilesize = fgetc(savefilesize);
		savefilesize = fopen("SaveSize.txt", "w");
		fputc(getfilesize + 1, savefilesize);
		fclose(savefilesize);
		GamePlayer.setHP(100000);
	}
}
void PlayerInteraction::treasureBoxRoomAction(){
	srand(time(NULL));
	int reward = 0;
	if (treasureBoxExistence){
		system("cls");
		switch (PlayerMap.getDifficulty())
		{
		case Easy:
			reward = rand() % 10;
			printf("\nThere was %d yen in the box.", reward);
			GamePlayer.setMoney(reward);
			break;
		case Normal:
			reward = rand() % 50 + 10;
			printf("\nThere was %d yen in the box.", reward);
			GamePlayer.setMoney(reward);
			break;
		case Hard:{
			reward = rand() % 100;
			printf("\nThere was %d yen in the box.", reward);
			GamePlayer.setMoney(reward);
		}
			break;
		default:
			break;
		}
		treasureBoxExistence = false;
		getch();
	}
	else{
		printf("\n\nIt was empty...");
		getch();
	}
}
std::vector<MonsterData> PlayerInteraction::getdata(){
	return data;
}
bool PlayerInteraction::TrapAction(){
	srand(time(NULL));
	int TrapDamage = GamePlayer.getHP() / (rand() % 3+2);
	printf("\nYou failed to move.\n And I received %d damage. ", TrapDamage);
	GamePlayer.setHP(-TrapDamage);
	getch();
	if (GamePlayer.getLifeanddeath()){
		return true;
	}
	else{
		printf("\n It hurts more than you think?");
		getch();
		return false;
	}
}

void PlayerInteraction::SellAction(int part){
	Allitem item;
	EquipmentInformation equipmentin = GamePlayer.getAllEquipment().at(part);
	int itemprice = item.getItemInformation(equipmentin.ItemType, equipmentin.ItemNumver).Itemprice /2;
	GamePlayer.setMoney(itemprice);
	GamePlayer.setequipment(part, Usagetype::vacant, 0);
	printf("\n         You earned %d by selling %s", itemprice, item.getItemInformation(equipmentin.ItemType, equipmentin.ItemNumver).ItemName);
	getch();
}
/*itemtype이 모드 1이였다.
*/
std::vector<std::string> PlayerInteraction::getstringequipmentstate(){
	Allitem BuyItem;
	std::vector<std::string> PlayerEquipmentState = std::vector<std::string>(GamePlayer.getequipmentSpace());
	std::vector<EquipmentInformation> playergetequipment = GamePlayer.getAllEquipment();
	for (int i = 0; i < GamePlayer.getequipmentSpace(); i++){
		if (playergetequipment.at(i).ItemType == Usagetype::vacant){
			PlayerEquipmentState.at(i) = "[ vacant ]";
		}
		else{
			std::stringstream setEquipment;
			setEquipment << "[ (" << i << ") : " << BuyItem.getItemInformation(playergetequipment.at(i).ItemType, playergetequipment.at(i).ItemNumver).ItemName << "]";
			PlayerEquipmentState.at(i) = setEquipment.str();
		}
	}
	return PlayerEquipmentState;
}
Charector_DungeonEnvironment PlayerInteraction::RunState(Location direction){
	srand(time(NULL));
	int Chancefleeing = rand() % 3;
	if (PlayerMap.getLocationMoved(direction) != RoominMap::BlockageRoom &&
		PlayerMap.getsafelocation(direction)
		){
		switch (PlayerMap.getRoomInformation(PlayerMap.getPlayerLocation()))
		{
		case BattleRoom:
			if (PlayerMap.getBattleState() && Chancefleeing){
				printf("\nYou failed to move.\n And I received damage. ");
				if (zapAction()){
					return Charector_DungeonEnvironment::Battle;
				}
				else{
					return Charector_DungeonEnvironment::Died;
				}
			}
			else{
				BattleEnd();
				PlayerMap.setPlayerLocation(direction);
				printf("I came out safely.");
				getch();
			}
			break;
		case TrapRoom:
			if (Chancefleeing){
				if (TrapAction()){
					return Charector_DungeonEnvironment::Trap;
				}
				else{
					return Charector_DungeonEnvironment::Died;
				}
			}
			else{
				printf("I came out safely.");
				getch();
				PlayerMap.setPlayerLocation(direction);
			}
			break;
		default:
			PlayerMap.setPlayerLocation(direction);
			break;
		}
		switch (PlayerMap.getRoomInformation(PlayerMap.getPlayerLocation()))
		{
		case PeaceRoom:
			return Charector_DungeonEnvironment::Peace;
		case BattleRoom:
			Battlestart();
			return Charector_DungeonEnvironment::Battle;
			break;
		case TrapRoom:
			if (TrapAction()){
				return Charector_DungeonEnvironment::Trap;
			}
			else{
				return Charector_DungeonEnvironment::Died;
			}
		case treasureBoxRoom:
			treasureBoxRoomAction();
			return Charector_DungeonEnvironment::treasureBox;
		case entranceRoom:
			return Charector_DungeonEnvironment::DungeonExit;
		default:
			break;
		}
	}
	else{
		switch (PlayerMap.getRoomInformation(PlayerMap.getPlayerLocation())){
		case PeaceRoom:
			return Charector_DungeonEnvironment::Peace;
		case BattleRoom:
			printf("\nYou failed to move.\n And I received damage. ");
			if (zapAction()){
				return Charector_DungeonEnvironment::Battle;
			}
			else{
				return Charector_DungeonEnvironment::Died;
			}
			return Charector_DungeonEnvironment::Battle;
		case TrapRoom:
			if (TrapAction()){
				return Charector_DungeonEnvironment::Trap;
			}
			else{
				return Charector_DungeonEnvironment::Died;
			}
		case treasureBoxRoom:
			return Charector_DungeonEnvironment::treasureBox;
		case entranceRoom:
			return Charector_DungeonEnvironment::DungeonExit;
		default:
			break;
		}
	}
}