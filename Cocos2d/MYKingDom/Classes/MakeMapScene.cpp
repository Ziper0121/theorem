#include "MakeMapScene.h"

/*
NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자
NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자
NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자
NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자
NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자
NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자NEAR = ALL은 없애자
*/
#define BLOCK_HALF_WIDTH mgame_starter.mallMapBlock.at(i)->getPositionX() - mgame_starter.mallMapBlock.at(i)->getScaleX()

#define BLOCK_HALF_HEIGHT mgame_starter.mallMapBlock.at(i)->getPositionY() - mgame_starter.mallMapBlock.at(i)->getScaleY()


#define MOVE_BLOCK_SCALE_WIDTH mmove_Location_Block.at(i).mlocation_Block.at(k)->getPositionX() - mmove_Location_Block.at(i).mlocation_Block.at(k)->getScaleX()

#define MOVE_BLOCK_SCALE_HEIGHT mmove_Location_Block.at(i).mlocation_Block.at(k)->getPositionY() - mmove_Location_Block.at(i).mlocation_Block.at(k)->getScaleY()

#define GAMESTAGE_BLOCK mgame_starter.mmapNowBlockInformation.Block_Kinds.at(mchoise_Block)

#define MOVEBLOCK_AT ((Moving_Block*)mgame_starter.mmapNowBlockInformation.Block_Kinds.at(i))

#define GAMESTAGE_MOVEBLOCK ((Moving_Block*)mgame_starter.mmapNowBlockInformation.Block_Kinds.at(mchoise_Block))
#define GAMESTAGE_CUTTERBLOCK ((Damage_Block*)mgame_starter.mmapNowBlockInformation.Block_Kinds.at(mchoise_Block))

#define MOVEBLOCK_BACK ((Moving_Block*)mgame_starter.mmapNowBlockInformation.Block_Kinds.back())
#define CUTTERBLOCK_BACK ((Damage_Block*)mgame_starter.mmapNowBlockInformation.Block_Kinds.back())
/*
Game Algorithm과 다른 것*/
#define BLOCKKIND_MOVINGBLOCK ((Moving_Block*)mmapNowBlockInformation.Block_Kinds.at(mchoise_Move_Block))

#define PLAYERCHAR_MOVE mgame_starter.mplayerCharacter.Charactor_MoveData
#define PLAYERCHAR_JUMP PLAYERCHAR_MOVE.Charactor_JumpPower
#define PLAYERCHAR_WORKING PLAYERCHAR_MOVE.Charactor_WorkingSpeed

#define MOVE_LOCATION_W 10.0f
#define MOVE_LOCATION_H 20.0f

#define CLICK_B 0X1
#define CLICK_M 0X2

/*
*/
Scene* MakeMapScene::creategmaeMapScene(){
	auto scene = Scene::create();
	auto layer = MakeMapScene::create();
	scene->addChild(layer);
	return scene;
}


/*

************************************************* 위치 설정을 체계적으로 하자
기본 레이어

새로만들기 === 완성
불러오기 ===== 완성
저장하기 ===== 완성

블럭 설정 ===== 완성
플레이어 설정 = 고민중
몬스터 설정
배경 설정

게임 실행
뒤로가기
*/

Move_Loaction_Block Move_Loaction_Block::operator=(Move_Loaction_Block& other){
	return Move_Loaction_Block(other.mconnect_Block_Num);
}

bool MakeMapScene::init(){
	if (!Layer::init()){
		return false;
	}
	mresetMapUI();

	mmouse_state = 0;

	mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mTargetOneLayerUI), ONE_LAYER);

	auto MouseEventLisnter = EventListenerMouse::create();
	MouseEventLisnter->onMouseDown = std::bind(&MakeMapScene::onMouseDown, this, std::placeholders::_1);
	MouseEventLisnter->onMouseUp = std::bind(&MakeMapScene::onMouseUP, this, std::placeholders::_1);
	MouseEventLisnter->onMouseMove = std::bind(&MakeMapScene::onMouseMove, this, std::placeholders::_1);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(MouseEventLisnter, this);

	return true;
}
void MakeMapScene::mresetMapUI(){

	mcreating_Qustion = Layer::create();
	mcreating_MoveLocationBlock = Layer::create();
	mcreating_Include_Scheudle = Layer::create();

	this->addChild(mcreating_Qustion);
	addChild(mgame_starter.mgameAlgorithm_Layer);
	mgame_starter.mgameAlgorithm_Layer->addChild(mcreating_MoveLocationBlock);

#pragma region ONE_LAYER_TARGETING

	mTargetOneLayerUI.UIBenchmark = Vec2(-50.0f, 850.0f);
	mTargetOneLayerUI.UIMargin = Size(22.0f, 0.0f);
	mTargetOneLayerUI.UISize = Size(80.0f, 80.0f);
	mTargetOneLayerUI.TestUIEncho = 1.0f;

	mTargetOneLayerUI.UIText.push_back("New Map");
	mTargetOneLayerUI.UIText.push_back("Load Map");
	mTargetOneLayerUI.UIText.push_back("Save Map");
	mTargetOneLayerUI.UIText.push_back("Block Setting");
	mTargetOneLayerUI.UIText.push_back("Player Setting");
	mTargetOneLayerUI.UIText.push_back("Monster \n Setting");
	mTargetOneLayerUI.UIText.push_back("BackGround \n Setting");
	mTargetOneLayerUI.UIText.push_back("Playe Game");
	mTargetOneLayerUI.UIText.push_back("Back");


	for (auto i = mTargetOneLayerUI.UIText.begin(); i < mTargetOneLayerUI.UIText.end(); i++){
		mTargetOneLayerUI.UIType.push_back(eUIKind::BUTTON);
		mTargetOneLayerUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		});
	}

	mTargetOneLayerUI.UIType.at(7) = eUIKind::CHECKBOX;

	mTargetOneLayerUI.CheckState.push_back(false);

	mTargetOneLayerUI.MyUserInterfaceEvent.at(0) = [&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapCreateTwoLayerQuestion), TWO_LAYER);

	};

	mTargetOneLayerUI.MyUserInterfaceEvent.at(1) = [&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);


		_finddatai64_t MapDataPorther_FILEName;
		intptr_t hFile;

		mmapDataFile_Size = 0;

		mmapLoadTwoLayerChoiceUI.UIType.clear();
		mmapLoadTwoLayerChoiceUI.UIText.clear();
		mmapLoadTwoLayerChoiceUI.MyUserInterfaceEvent.clear();

		hFile = _findfirsti64("Map_Data_FILE/*.bin*", &MapDataPorther_FILEName);
		if (hFile == -1){

			mmapLoadTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
			mmapLoadTwoLayerChoiceUI.UIText.push_back("No");

			mmapLoadTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
				mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

			});
			mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapLoadTwoLayerChoiceUI), TWO_LAYER);

			return;
		}

		mmapData_FILEName = (char**)malloc(sizeof(char*) * 1);
		mmapData_FILEName[0] = (char*)malloc(sizeof(char) * 260);

		memcpy(mmapData_FILEName[0], MapDataPorther_FILEName.name, sizeof(MapDataPorther_FILEName.name));

		mmapLoadTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
		mmapLoadTwoLayerChoiceUI.UIText.push_back(mmapData_FILEName[0]);
		mmapLoadTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
			mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

			mmapData_FILENum = ref->_ID;
			if (mmap_Exist){
				mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapLoadThreeLayerQuestion), THREE_LAYER);
			}
			else{
				mmove_Location_BlockSetting();
			}

		});
		mmapDataFile_Size++;

		for (int i = 1; _findnexti64(hFile, &MapDataPorther_FILEName) == 0; i++){

			mmapData_FILEName = (char**)realloc(mmapData_FILEName, sizeof(char*) *(i + 1));
			mmapData_FILEName[i] = (char*)malloc(sizeof(char) * 260);

			memcpy(mmapData_FILEName[i], MapDataPorther_FILEName.name, sizeof(MapDataPorther_FILEName.name));

			mmapLoadTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
			mmapLoadTwoLayerChoiceUI.UIText.push_back(mmapData_FILEName[i]);

			mmapLoadTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
				mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

				mmapData_FILENum = ref->_ID;

				if (mmap_Exist){
					mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapLoadThreeLayerQuestion), THREE_LAYER);
				}
				else{
					mmove_Location_BlockSetting();
				}

			});

			mmapDataFile_Size++;
		}

		mmapLoadTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
		mmapLoadTwoLayerChoiceUI.UIText.push_back("No");

		mmapLoadTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
			mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
		});

		_findclose(hFile);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapLoadTwoLayerChoiceUI), TWO_LAYER);
	};
	mTargetOneLayerUI.MyUserInterfaceEvent.at(2) = [&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		if (!mmap_Exist){
			return;
		}
		_finddatai64_t MapDataPorther_FILEName;
		intptr_t hFile;

		mmapDataFile_Size = 0;

		hFile = _findfirsti64("Map_Data_FILE/*.bin*", &MapDataPorther_FILEName);
		if (hFile == -1){

			mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapSaveTwoLayerChoiceUI), TWO_LAYER);

			return;
		}

		mmapData_FILEName = (char**)malloc(sizeof(char*) * 1);
		mmapData_FILEName[0] = (char*)malloc(sizeof(char) * 260);

		hFile = _findfirsti64("Map_Data_FILE/*.bin*", &MapDataPorther_FILEName);
		memcpy(mmapData_FILEName[0], MapDataPorther_FILEName.name, sizeof(MapDataPorther_FILEName.name));
		mmapDataFile_Size++;

		for (int i = 1; _findnexti64(hFile, &MapDataPorther_FILEName) == 0; i++){
			mmapData_FILEName = (char**)realloc(mmapData_FILEName, sizeof(char*) * (1 + i));
			mmapData_FILEName[i] = (char*)malloc(sizeof(char) * 260);

			memcpy(mmapData_FILEName[i], MapDataPorther_FILEName.name, sizeof(MapDataPorther_FILEName.name));
			mmapDataFile_Size++;
		}

		_findclose(hFile);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mmapSaveTwoLayerChoiceUI), TWO_LAYER);

	};
	mTargetOneLayerUI.MyUserInterfaceEvent.at(3) = [&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mblockTargetTwoLayerChoiceUI), TWO_LAYER);

	};
	mTargetOneLayerUI.MyUserInterfaceEvent.at(4) = [&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerTargetTwoLayerChoiceUI), TWO_LAYER);

	};
	mTargetOneLayerUI.MyUserInterfaceEvent.at(7) = [&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		if (((CheckBox*)ref)->getSelectedState() && mmap_Exist){
			mcreating_Include_Scheudle->schedule(schedule_selector(MakeMapScene::mmove_GameAlgorithm));
		}
		else{
			mcreating_Include_Scheudle->unschedule(schedule_selector(MakeMapScene::mmove_GameAlgorithm));
		}
	};
	mTargetOneLayerUI.MyUserInterfaceEvent.at(8) = [&](Ref* ref){
	};
#pragma endregion ONE_LAYER_TARGETING

#pragma region TWO_NEWMAP_LAYER_QUESTION

	mmapCreateTwoLayerQuestion.UIMargin = Size(30.0f, 0.0f);
	mmapCreateTwoLayerQuestion.UISize = Size(100.0f, 30.0f);
	mmapCreateTwoLayerQuestion.TestUIEncho = 1.0f;
	mmapCreateTwoLayerQuestion.UIBenchmark = Vec2(500.0f - mmapCreateTwoLayerQuestion.UIMargin.width - mmapCreateTwoLayerQuestion.UISize.width, 500.0f);

	mmapCreateTwoLayerQuestion.UIType.push_back(eUIKind::BUTTON);
	mmapCreateTwoLayerQuestion.UIType.push_back(eUIKind::BUTTON);

	mmapCreateTwoLayerQuestion.UIText.push_back("No");
	mmapCreateTwoLayerQuestion.UIText.push_back("Yes");

	mmapCreateTwoLayerQuestion.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
	});
	mmapCreateTwoLayerQuestion.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		mmap_Exist = true;

		mremove_MakingMap_AllBlock();
		mgame_starter.mplayerCharacter.mcharactorData_BascisSetting();
		mgame_starter.mmapNowBlockInformation.Player_StartPosition = Vec2(500.0f, 700.0f);
		mgame_starter.mstageAddBlock(BASIC_BIT);
		mgame_starter.mplayerQuadrantPointSetting();
		CCLOG("Charactor_Avatar = %f %f", mgame_starter.mplayerCharacter.Charactor_Avatar->getPositionX(), mgame_starter.mplayerCharacter.Charactor_Avatar->getPositionY());
	});

#pragma endregion TWO_NEWMAP_LAYER_QUESTION

#pragma region THREE_LODE_LAYER_QUESTION

	mmapLoadThreeLayerQuestion.UIMargin = Size(30.0f, 0.0f);
	mmapLoadThreeLayerQuestion.UISize = Size(100.0f, 30.0f);
	mmapLoadThreeLayerQuestion.TestUIEncho = 1.0f;
	mmapLoadThreeLayerQuestion.UIBenchmark = Vec2(500.0f - mmapLoadThreeLayerQuestion.UIMargin.width - mmapLoadThreeLayerQuestion.UISize.width, 500.0f);

	mmapLoadThreeLayerQuestion.UIType.push_back(eUIKind::BUTTON);
	mmapLoadThreeLayerQuestion.UIType.push_back(eUIKind::BUTTON);

	mmapLoadThreeLayerQuestion.UIText.push_back("No");
	mmapLoadThreeLayerQuestion.UIText.push_back("Yes");

	mmapLoadThreeLayerQuestion.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
	});
	mmapLoadThreeLayerQuestion.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		mmap_Exist = true;

		mremove_MakingMap_AllBlock();

		mmove_Location_BlockSetting();
	});

#pragma endregion THREE_LODE_LAYER_QUESTION

#pragma region TWO_LOADFILE_LAYER_QUESTION

	mmapLoadTwoLayerChoiceUI.UIMargin = Size(0.0f, -10.0f);
	mmapLoadTwoLayerChoiceUI.UISize = Size(100.0f, 30.0f);
	mmapLoadTwoLayerChoiceUI.TestUIEncho = 1.0f;
	mmapLoadTwoLayerChoiceUI.UIBenchmark = Vec2(500.0f - mmapLoadTwoLayerChoiceUI.UIMargin.width - mmapLoadTwoLayerChoiceUI.UISize.width,
		mTargetOneLayerUI.UIBenchmark.y - mmapLoadTwoLayerChoiceUI.UISize.height * 3);
#pragma endregion TWO_LOADFILE_LAYER_QUESTION

#pragma region TWO_SAVEFILE_LAYER_QUESTION

	mmapSaveTwoLayerChoiceUI.UIMargin = Size(30.0f, 0.0f);
	mmapSaveTwoLayerChoiceUI.UISize = Size(100.0f, 30.0f);
	mmapSaveTwoLayerChoiceUI.TestUIEncho = 1.0f;
	mmapSaveTwoLayerChoiceUI.UIBenchmark = Vec2(500.0f - mmapSaveTwoLayerChoiceUI.UIMargin.width - mmapSaveTwoLayerChoiceUI.UISize.width *2.0, 500.0f);

	mmapSaveTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mmapSaveTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);

	mmapSaveTwoLayerChoiceUI.UIText.push_back("No");
	mmapSaveTwoLayerChoiceUI.UIText.push_back("Yes");

	mmapSaveTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
	});
	mmapSaveTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

		std::stringstream MakeMap_Name;
		MakeMap_Name.str("MakeMap_StageData0.bin");
		int RemoveNum_Name = 0;

		for (int i = 0; i < mmapDataFile_Size; i++){
			if (!MakeMap_Name.str().compare(mmapData_FILEName[i])){
				RemoveNum_Name++;
				MakeMap_Name.str("");
				MakeMap_Name << "MakeMap_StageData" << RemoveNum_Name << ".bin";
			}
		}
		mgame_starter.mmapNowBlockInformation.mgameStage_SaveBlock(MakeMap_Name.str().c_str());

	});

#pragma endregion TWO_SAVEFILE_LAYER_QUESTION


#pragma region TWO_BLOCK_LAYER_TARGETING

	/*
	블럭 생성
	블럭 제거
	블럭 크기
	블럭 위치
	무브 블럭
	피해 블럭*/


	mblockTargetTwoLayerChoiceUI.UIMargin = Size(30.0f, 0.0f);
	mblockTargetTwoLayerChoiceUI.UISize = Size(100.0f, 30.0f);
	mblockTargetTwoLayerChoiceUI.TestUIEncho = 1.0f;
	mblockTargetTwoLayerChoiceUI.UIBenchmark = Vec2(0.0f, mTargetOneLayerUI.UIBenchmark.y - mTargetOneLayerUI.UISize.height);

	mblockTargetTwoLayerChoiceUI.UIText.push_back("Create");
	mblockTargetTwoLayerChoiceUI.UIText.push_back("Remove");
	mblockTargetTwoLayerChoiceUI.UIText.push_back("Size");
	mblockTargetTwoLayerChoiceUI.UIText.push_back("Position");
	mblockTargetTwoLayerChoiceUI.UIText.push_back("Move");
	mblockTargetTwoLayerChoiceUI.UIText.push_back("Damage");

	for (auto i = mblockTargetTwoLayerChoiceUI.UIText.begin(); i < mblockTargetTwoLayerChoiceUI.UIText.end(); i++){
		if (!strcmp("Damage", *i)){
			mblockTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::SLIDER);
		}
		else if (!strcmp("Remove", *i)){
			mblockTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::CHECKBOX);
		}
		else{
			mblockTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
		}
	}

	mblockTargetTwoLayerChoiceUI.CheckState.push_back(false);

	mblockTargetTwoLayerChoiceUI.SliderNow.push_back(1);

	mblockTargetTwoLayerChoiceUI.SliderMax.push_back(100);

	mblockTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);
		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mblockCreateThreeLayerChoiceUI), THREE_LAYER);

	});
	mblockTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		mremove_Block_State = ((CheckBox*)ref)->getSelectedState();
	});
	mblockTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mblockSizeThreeLayerSettingUI), THREE_LAYER);

	});
	mblockTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mblockPositionThreeLayerSettingUI), THREE_LAYER);

	});
	mblockTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mblockMoveThreeLayerSettingUI), THREE_LAYER);

	});
	mblockTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		if (mgame_starter.mallMapBlock.size() > 0){
			GAMESTAGE_CUTTERBLOCK->Block_Damage = ((Slider*)ref)->getPercent();
			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockTargetTwoLayerChoiceUI.UIText.back());
		}

	});

#pragma endregion TWO_BLOCK_LAYER_TARGETING

#pragma region THREE_NEWBLOCK_LAYER_CHOICE

	mblockCreateThreeLayerChoiceUI.UIMargin = Size(40.0f, 0.0f);
	mblockCreateThreeLayerChoiceUI.UISize = Size(40.0f, 40.0f);
	mblockCreateThreeLayerChoiceUI.TestUIEncho = 1.0f;
	mblockCreateThreeLayerChoiceUI.UIBenchmark = Vec2(0.0f, mblockTargetTwoLayerChoiceUI.UIBenchmark.y - mblockCreateThreeLayerChoiceUI.UISize.height);

	mblockCreateThreeLayerChoiceUI.UIText.push_back("Basic");
	mblockCreateThreeLayerChoiceUI.UIText.push_back("Cutter");
	mblockCreateThreeLayerChoiceUI.UIText.push_back("Move");

	mblockCreateThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mblockCreateThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mblockCreateThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);

	mblockCreateThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mchoise_Block = mgame_starter.mallMapBlock.size();
		mgame_starter.mstageAddBlock(BASIC_BIT);
	});
	mblockCreateThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mchoise_Block = mgame_starter.mallMapBlock.size();
		mgame_starter.mstageAddBlock(DAMAGE_BIT);
	});
	mblockCreateThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mchoise_Block = mgame_starter.mallMapBlock.size();
		mgame_starter.mstageAddBlock(MOVE_BIT);

		mchoise_Move_Location = 0;
		mmove_Location_Block.push_back(Move_Loaction_Block(mchoise_Block));
		mmove_Location_Block.back().mlocation_Block.push_back(Sprite::create("Move_Location_Block.png"));
		mmove_Location_Block.back().mlocation_Block.back()->
			setPosition(mgame_starter.mallMapBlock.at(mchoise_Block)->getPosition() + Vec2(MOVE_LOCATION_W * CONST_X_PERCENT, 0.0f));
		mmove_Location_Block.back().mlocation_Block.back()->setAnchorPoint(Vec2(1.0f, 1.0f));
		mmove_Location_Block.back().mlocation_Block.back()->setScaleX(MOVE_LOCATION_W * CONST_X_PERCENT);
		mmove_Location_Block.back().mlocation_Block.back()->setScaleY(MOVE_LOCATION_H * CONST_Y_PERCENT);
		mcreating_MoveLocationBlock->addChild(mmove_Location_Block.back().mlocation_Block.back());

	});

#pragma endregion THREE_NEWBLOCK_LAYER_CHOICE

#pragma region THREE_BLOCK_SIZE_LAYER_SETTING

	mblockSizeThreeLayerSettingUI.UIMargin = Size(0.0f, 20.0f);
	mblockSizeThreeLayerSettingUI.UISize = Size(100.0f, 30.0f);
	mblockSizeThreeLayerSettingUI.TestUIEncho = -1.0f;
	mblockSizeThreeLayerSettingUI.UIBenchmark = Vec2(mTargetOneLayerUI.UISize.width * 6.2, mblockTargetTwoLayerChoiceUI.UIBenchmark.y - mblockSizeThreeLayerSettingUI.UISize.height);

	mblockSizeThreeLayerSettingUI.UIText.push_back("X");
	mblockSizeThreeLayerSettingUI.UIText.push_back("Y");

	mblockSizeThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mblockSizeThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);

	mblockSizeThreeLayerSettingUI.SliderMax.push_back(1000);
	mblockSizeThreeLayerSettingUI.SliderMax.push_back(1000);

	mblockSizeThreeLayerSettingUI.SliderNow.push_back(100);
	mblockSizeThreeLayerSettingUI.SliderNow.push_back(100);
	/*
	slider가 형재 블럭으 상태를 확인하고 퍼센트를 만들자
	블럭이 생산이 안되있을 경우 잠거두던가 값이 변경이 안되게하자
	*/
	mblockSizeThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (mgame_starter.mallMapBlock.size()> 0){
			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockSizeThreeLayerSettingUI.UIText.at(0));
			mgame_starter.mallMapBlock.at(mchoise_Block)->setScaleX(((Slider*)ref)->getPercent());
			mgame_starter.mmapNowBlockInformation.Block_Size.at(mchoise_Block).width = ((Slider*)ref)->getPercent();
		}
	});
	mblockSizeThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (mgame_starter.mallMapBlock.size()> 0){
			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockSizeThreeLayerSettingUI.UIText.at(1));
			mgame_starter.mallMapBlock.at(mchoise_Block)->setScaleY(((Slider*)ref)->getPercent());
			mgame_starter.mmapNowBlockInformation.Block_Size.at(mchoise_Block).height = ((Slider*)ref)->getPercent();
		}
	});

#pragma endregion THREE_BLOCK_SIZE_LAYER_SETTING

#pragma region THREE_BLOCK_POSITION_LAYER_SETTING

	mblockPositionThreeLayerSettingUI.UIMargin = Size(0.0f, 20.0f);
	mblockPositionThreeLayerSettingUI.UISize = Size(100.0f, 20.0f);
	mblockPositionThreeLayerSettingUI.TestUIEncho = -1.0f;
	mblockPositionThreeLayerSettingUI.UIBenchmark = Vec2(mTargetOneLayerUI.UISize.width * 8.2, mblockTargetTwoLayerChoiceUI.UIBenchmark.y - mTargetOneLayerUI.UISize.height);

	mblockPositionThreeLayerSettingUI.UIText.push_back("Width");
	mblockPositionThreeLayerSettingUI.UIText.push_back("Height");

	int ThreeLayerBlock_PositionSliderSize = mblockPositionThreeLayerSettingUI.UIText.size();
	for (int i = 0; i < ThreeLayerBlock_PositionSliderSize; i++){

		mblockPositionThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
		mblockPositionThreeLayerSettingUI.SliderMax.push_back(1000);
		mblockPositionThreeLayerSettingUI.SliderNow.push_back(500);
	}

	/*
	slider가 형재 블럭으 상태를 확인하고 퍼센트를 만들자
	블럭이 생산이 안되있을 경우 잠거두던가 값이 변경이 안되게하자
	*/
	mblockPositionThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (mgame_starter.mallMapBlock.size()> 0){
			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockPositionThreeLayerSettingUI.UIText.at(0));
			mgame_starter.mallMapBlock.at(mchoise_Block)->setPositionX(((Slider*)ref)->getPercent() * CONST_X_PERCENT);
			mgame_starter.mmapNowBlockInformation.Block_Position.at(mchoise_Block).x = ((Slider*)ref)->getPercent();
		}
	});
	mblockPositionThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (mgame_starter.mallMapBlock.size() > 0){
			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockPositionThreeLayerSettingUI.UIText.at(1));
			mgame_starter.mallMapBlock.at(mchoise_Block)->setPositionY(((Slider*)ref)->getPercent()* CONST_Y_PERCENT);
			mgame_starter.mmapNowBlockInformation.Block_Position.at(mchoise_Block).y = ((Slider*)ref)->getPercent();
		}
	});

#pragma endregion THREE_BLOCK_POSITION_LAYER_SETTING

#pragma region THREE_BLOCK_MOVE_LAYER_TARGETING

	mblockMoveThreeLayerSettingUI.UIMargin = Size(40.0f, 0.0f);
	mblockMoveThreeLayerSettingUI.UISize = Size(100.0f, 20.0f);
	mblockMoveThreeLayerSettingUI.TestUIEncho = 1.0f;
	mblockMoveThreeLayerSettingUI.UIBenchmark = Vec2(mblockTargetTwoLayerChoiceUI.UISize.width * 3.0f, mblockTargetTwoLayerChoiceUI.UIBenchmark.y - mblockMoveThreeLayerSettingUI.UISize.height * 2);

	mblockMoveThreeLayerSettingUI.UIText.push_back("Create");
	mblockMoveThreeLayerSettingUI.UIText.push_back("Position");
	mblockMoveThreeLayerSettingUI.UIText.push_back("Encho");
	mblockMoveThreeLayerSettingUI.UIText.push_back("Speed");
	mblockMoveThreeLayerSettingUI.UIText.push_back("Remove");

	mblockMoveThreeLayerSettingUI.UIType.push_back(eUIKind::BUTTON);
	mblockMoveThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mblockMoveThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mblockMoveThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mblockMoveThreeLayerSettingUI.UIType.push_back(eUIKind::CHECKBOX);

	mblockMoveThreeLayerSettingUI.CheckState.push_back(false);

	mblockMoveThreeLayerSettingUI.SliderMax.push_back(1000);
	mblockMoveThreeLayerSettingUI.SliderMax.push_back(7);
	mblockMoveThreeLayerSettingUI.SliderMax.push_back(10);

	mblockMoveThreeLayerSettingUI.SliderNow.push_back(10);
	mblockMoveThreeLayerSettingUI.SliderNow.push_back(2);
	mblockMoveThreeLayerSettingUI.SliderNow.push_back(2);

	mblockMoveThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (mgame_starter.mallMapBlock.size() == 0 || GAMESTAGE_BLOCK->Block_Attribute != MOVE_BIT){
			return;
		}

		int Choise_LocationBlock = -1;
		mmoveLocationBenchMark = Vec2(0.0f, 0.0f);

		for (auto i = mmove_Location_Block.begin(); i < mmove_Location_Block.end(); i++){
			if ((*i).mconnect_Block_Num == mchoise_Block){
				mmoveLocationBenchMark = (*i).mlocation_Block.back()->getPosition();
				Choise_LocationBlock++;
				break;
			}
		}
		/*
		Vec2를 비교하지 않는 이유는 block의 position값이 (0,0)일수가 있어서*/

		GAMESTAGE_MOVEBLOCK->Moving_Echo.push_back(Vec2(1.0f, 0.0f));
		GAMESTAGE_MOVEBLOCK->Moving_Location.push_back(10.0f * CONST_X_PERCENT);

		mchoise_Move_Location = mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.size();

		mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.push_back(Sprite::create("Move_Location_Block.png"));
		mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.back()->
			setPosition(mmoveLocationBenchMark + Vec2(MOVE_LOCATION_W * CONST_X_PERCENT, 0.0f));
		mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.back()->setAnchorPoint(Vec2(1.0f, 1.0f));
		mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.back()->setScaleX(MOVE_LOCATION_W * CONST_X_PERCENT);
		mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.back()->setScaleY(MOVE_LOCATION_H * CONST_Y_PERCENT);
		mcreating_MoveLocationBlock->addChild(mmove_Location_Block.at(Choise_LocationBlock).mlocation_Block.back());

		mchoise_Move_Connect_Block = Choise_LocationBlock;

	});
	/*
	move블럭이 대각선으로 이동시 각도를재고 속도를 맞추자*/
	mblockMoveThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (GAMESTAGE_MOVEBLOCK->Moving_Location.size() == 0 
			|| GAMESTAGE_BLOCK->Block_Attribute != MOVE_BIT
			|| mchoise_Move_Connect_Block == -1){
			return;
		}

		GAMESTAGE_MOVEBLOCK->Moving_Location.at(mchoise_Move_Location) = ((Slider*)ref)->getPercent();

		Vec2 Moving_Position = GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Location);
		Moving_Position.x *= CONST_X_PERCENT;
		Moving_Position.y *= CONST_Y_PERCENT;

		Vec2 Move_Block_AddGetPosition = mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.at(mchoise_Move_Location)->getPosition();
		mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.at(mchoise_Move_Location)
			->setPosition(mmoveLocationBenchMark + ((Slider*)ref)->getPercent() * Moving_Position);

		for (auto i = mmove_Location_Block.at(mchoise_Move_Location).mlocation_Block.begin();
			i < mmove_Location_Block.at(mchoise_Move_Location).mlocation_Block.end(); i++){
			(*i)->setPosition((*i)->getPosition() + ((Slider*)ref)->getPercent() * Moving_Position);
		}

		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockMoveThreeLayerSettingUI.UIText.at(1));

	});
	mblockMoveThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);

		if (GAMESTAGE_MOVEBLOCK->Moving_Location.size() == 0
			|| GAMESTAGE_BLOCK->Block_Attribute != MOVE_BIT
			|| mchoise_Move_Connect_Block == -1){
			return;
		}

		if (GAMESTAGE_MOVEBLOCK->Moving_Echo.size() > 0){
			switch (((Slider*)ref)->getPercent()){
			case 0:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(0.0f, 1.0f);
				break;
			case 1:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(1.0f, 1.0f);
				break;
			case 2:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(1.0f, 0.0f);
				break;
			case 3:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(1.0f, -1.0f);
				break;
			case 4:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(0.0f, -1.0f);
				break;
			case 5:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(-1.0f, -1.0f);
				break;
			case 6:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(-1.0f, 0.0f);
				break;
			case 7:
				GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) = Vec2(-1.0f, 1.0f);
				break;
			default:
				break;
			}

			Vec2 Moving_Position = GAMESTAGE_MOVEBLOCK->Moving_Echo.at(mchoise_Move_Connect_Block) *
				GAMESTAGE_MOVEBLOCK->Moving_Location.at(mchoise_Move_Connect_Block);
			Moving_Position.x *= CONST_X_PERCENT;
			Moving_Position.y *= CONST_Y_PERCENT;
			Vec2 Move_Block_AddGetPosition = mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.at(mchoise_Move_Location)->getPosition();
			mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.at(mchoise_Move_Location)
				->setPosition(mmoveLocationBenchMark + Moving_Position);

			for (auto i = mmove_Location_Block.begin(); i < mmove_Location_Block.end(); i++){
				(*i).mlocation_Block.at(mchoise_Move_Location)
					->setPosition((*i).mlocation_Block.at(mchoise_Move_Location)->getPosition() + Moving_Position);
			}

			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockMoveThreeLayerSettingUI.UIText.at(2));
		}
	});
	mblockMoveThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		if (GAMESTAGE_MOVEBLOCK->Moving_Location.size() == 0
			|| GAMESTAGE_BLOCK->Block_Attribute != MOVE_BIT
			|| mchoise_Move_Connect_Block == -1){
			return;
		}
		if (mmove_Location_Block.size()> 0){
			GAMESTAGE_MOVEBLOCK->Moving_Speed = ((Slider*)ref)->getPercent();
			mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mblockMoveThreeLayerSettingUI.UIText.at(3));
		}
	});
	mblockMoveThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);

		mremove_MoveLocationBlock_State = ((CheckBox*)ref)->getSelectedState();

	});

#pragma endregion THREE_BLOCK_MOVE_LAYER_TARGETING


#pragma region TWO_PLAYER_LAYER_TARGETING

	mplayerTargetTwoLayerChoiceUI.UIMargin = Size(30.0f, 0.0f);
	mplayerTargetTwoLayerChoiceUI.UISize = Size(100.0f, 30.0f);
	mplayerTargetTwoLayerChoiceUI.TestUIEncho = 1.0f;
	mplayerTargetTwoLayerChoiceUI.UIBenchmark = Vec2(0.0f, mTargetOneLayerUI.UIBenchmark.y - mTargetOneLayerUI.UISize.height);

	mplayerTargetTwoLayerChoiceUI.UIText.push_back("Life");
	mplayerTargetTwoLayerChoiceUI.UIText.push_back("Position");
	mplayerTargetTwoLayerChoiceUI.UIText.push_back("Scale");
	mplayerTargetTwoLayerChoiceUI.UIText.push_back("Speed");
	mplayerTargetTwoLayerChoiceUI.UIText.push_back("Jump");
	mplayerTargetTwoLayerChoiceUI.UIText.push_back("CharLoad");
	mplayerTargetTwoLayerChoiceUI.UIText.push_back("CharSave");

	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::SLIDER);
	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
	mplayerTargetTwoLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);

	mplayerTargetTwoLayerChoiceUI.SliderMax.push_back(1000);

	mplayerTargetTwoLayerChoiceUI.SliderNow.push_back(100);

	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);
		mgame_starter.mplayerCharacter.Charactor_LifeParts = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerTargetTwoLayerChoiceUI.UIText.at(0));
	});
	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerPositionThreeLayerSettingUI), THREE_LAYER);

	});
	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){

		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);
		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerSizeThreeLayerSettingUI), THREE_LAYER);

	});
	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){

		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerSpeedThreeLayerSettingUI), THREE_LAYER);
	});
	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		;
		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerJumpThreeLayerSettingUI), THREE_LAYER);

	});
	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){

		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		_finddatai64_t PlayerDataPorther_FILEName;
		mplayerDataPorther_Size = 0;
		std::stringstream AddCharDataFile_Name;
		intptr_t hFile;

		mplayerLoadThreeLayerChoiceUI.UIType.clear();
		mplayerLoadThreeLayerChoiceUI.UIText.clear();
		mplayerLoadThreeLayerChoiceUI.MyUserInterfaceEvent.clear();

		hFile = _findfirsti64("Char_Data_FILE/*.bin*", &PlayerDataPorther_FILEName);
		if (hFile == -1){

			mplayerLoadThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
			mplayerLoadThreeLayerChoiceUI.UIText.push_back("No");

			mplayerLoadThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
				mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
			});
			mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerLoadThreeLayerChoiceUI), THREE_LAYER);
			return;
		}

		mplayerData_FILEName = (char**)malloc(sizeof(char*) * 1);
		mplayerData_FILEName[0] = (char*)malloc(sizeof(char) * 260);
		mplayerDataPorther_Size++;

		AddCharDataFile_Name.str("");
		AddCharDataFile_Name << "Char_Data_FILE/" << PlayerDataPorther_FILEName.name;
		memcpy(mplayerData_FILEName[0], AddCharDataFile_Name.str().c_str(), sizeof(PlayerDataPorther_FILEName.name));

		mplayerLoadThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
		mplayerLoadThreeLayerChoiceUI.UIText.push_back(mplayerData_FILEName[0]);

		mplayerLoadThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){

			mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
			if (mplayer_Exist){
				mplayerData_FILENum = ref->_ID;
				mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerLoadThreeLayerQuestion), THREE_LAYER);
			}
			else{
				FILE* StageMap_File = fopen(mplayerData_FILEName[ref->_ID], "rb");
				fread(&mgame_starter.mmapNowBlockInformation, sizeof(GameStageBlock), 1, StageMap_File);
				fclose(StageMap_File);
			}

		});

		for (int i = 1; _findnexti64(hFile, &PlayerDataPorther_FILEName) == 0; i++){

			mplayerData_FILEName = (char**)realloc(mplayerData_FILEName, sizeof(char*) * (i + 1));
			mplayerData_FILEName[i] = (char*)malloc(sizeof(char) * 260);

			AddCharDataFile_Name.str("");
			AddCharDataFile_Name << "Char_Data_FILE/" << PlayerDataPorther_FILEName.name;
			memcpy(mplayerData_FILEName[i], AddCharDataFile_Name.str().c_str(), sizeof(PlayerDataPorther_FILEName.name));

			mplayerLoadThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
			mplayerLoadThreeLayerChoiceUI.UIText.push_back(mplayerData_FILEName[i]);

			mplayerLoadThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
				mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
				if (mplayer_Exist){
					mplayerData_FILENum = ref->_ID;
					mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerLoadThreeLayerQuestion), THREE_LAYER);
				}
				else{
					FILE* StageMap_File = fopen(mplayerData_FILEName[ref->_ID], "rb");
					fread(&mgame_starter.mmapNowBlockInformation, sizeof(GameStageBlock), 1, StageMap_File);
					fclose(StageMap_File);
				}
			});
		}

		mplayerLoadThreeLayerChoiceUI.UIType.push_back(eUIKind::BUTTON);
		mplayerLoadThreeLayerChoiceUI.UIText.push_back("No");

		mplayerLoadThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
			mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);
		});

		_findclose(hFile);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerLoadThreeLayerChoiceUI), THREE_LAYER);

	});
	mplayerTargetTwoLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){

		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);
		_finddatai64_t PlayerDataPorther_FILEName;
		mplayerDataPorther_Size = 0;
		std::stringstream AddCharDataFile_Name;
		intptr_t hFile;

		hFile = _findfirsti64("Char_Data_FILE/*.bin*", &PlayerDataPorther_FILEName);
		if (hFile == -1){
			mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerSaveThreeLayerChoiceUI), THREE_LAYER);
			return;
		}

		mplayerDataPorther_Size++;
		while (_findnexti64(hFile, &PlayerDataPorther_FILEName) == 0)mplayerDataPorther_Size++;

		_findclose(hFile);

		mplayerData_FILEName = (char**)malloc(sizeof(char*) * mplayerDataPorther_Size);
		for (int i = 0; i < mplayerDataPorther_Size; i++){
			mplayerData_FILEName[i] = (char*)malloc(sizeof(char) * 260);
		}

		hFile = _findfirsti64("Char_Data_FILE/*.bin*", &PlayerDataPorther_FILEName);
		AddCharDataFile_Name.str("");
		AddCharDataFile_Name << "Char_Data_FILE/" << PlayerDataPorther_FILEName.name;
		memcpy(mplayerData_FILEName[0], AddCharDataFile_Name.str().c_str(), sizeof(PlayerDataPorther_FILEName.name));

		for (int i = 1; _findnexti64(hFile, &PlayerDataPorther_FILEName) == 0; i++){
			AddCharDataFile_Name.str("");
			AddCharDataFile_Name << "Char_Data_FILE/" << PlayerDataPorther_FILEName.name;
			memcpy(mplayerData_FILEName[i], AddCharDataFile_Name.str().c_str(), sizeof(PlayerDataPorther_FILEName.name));
		}

		_findclose(hFile);

		mcreating_Qustion->addChild(mmapCreateSceneUI.AddUIInforMation(mplayerSaveThreeLayerChoiceUI), THREE_LAYER);

	});

#pragma endregion TWO_PLAYER_LAYER_TARGETING

#pragma region THREE_LODE_LAYER_QUESTION

	mplayerLoadThreeLayerQuestion.UIMargin = Size(30.0f, 0.0f);
	mplayerLoadThreeLayerQuestion.UISize = Size(100.0f, 30.0f);
	mplayerLoadThreeLayerQuestion.TestUIEncho = 1.0f;
	mplayerLoadThreeLayerQuestion.UIBenchmark = Vec2(500.0f - mplayerLoadThreeLayerQuestion.UIMargin.width - mplayerLoadThreeLayerQuestion.UISize.width, 500.0f);

	mplayerLoadThreeLayerQuestion.UIType.push_back(eUIKind::BUTTON);
	mplayerLoadThreeLayerQuestion.UIType.push_back(eUIKind::BUTTON);

	mplayerLoadThreeLayerQuestion.UIText.push_back("No");
	mplayerLoadThreeLayerQuestion.UIText.push_back("Yes");

	mplayerLoadThreeLayerQuestion.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);

	});
	mplayerLoadThreeLayerQuestion.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);

		mmap_Exist = true;

		mgame_starter.mremoveStage_AllBlock();

		FILE* StageMap_File = fopen(mplayerData_FILEName[mplayerData_FILENum], "rb");
		fread(&mgame_starter.mmapNowBlockInformation, sizeof(GameStageBlock), 1, StageMap_File);
		fclose(StageMap_File);

	});

#pragma endregion THREE_LODE_LAYER_QUESTION

#pragma region THREE_PLAYER_POSITION_LAYER_TARGETING

	mplayerPositionThreeLayerSettingUI.UIMargin = Size(30.0f, 0.0f);
	mplayerPositionThreeLayerSettingUI.UISize = Size(100.0f, 30.0f);
	mplayerPositionThreeLayerSettingUI.TestUIEncho = 1.0f;
	mplayerPositionThreeLayerSettingUI.UIBenchmark = Vec2(500.0f, 500.0f);

	mplayerPositionThreeLayerSettingUI.UIText.push_back("Start");
	mplayerPositionThreeLayerSettingUI.UIText.push_back("X");
	mplayerPositionThreeLayerSettingUI.UIText.push_back("Y");

	mplayerPositionThreeLayerSettingUI.UIType.push_back(eUIKind::CHECKBOX);
	mplayerPositionThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mplayerPositionThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);

	mplayerPositionThreeLayerSettingUI.CheckState.push_back(false);

	mplayerPositionThreeLayerSettingUI.SliderMax.push_back(1000);
	mplayerPositionThreeLayerSettingUI.SliderMax.push_back(1000);

	mplayerPositionThreeLayerSettingUI.SliderNow.push_back(10);
	mplayerPositionThreeLayerSettingUI.SliderNow.push_back(10);

	mplayerPositionThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mplayer_StartPosition_State = ((CheckBox*)ref)->getSelectedState();
	});

	mplayerPositionThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		int refPercent = ((Slider*)ref)->getPercent();
		if (mplayer_StartPosition_State){
			mgame_starter.mmapNowBlockInformation.Player_StartPosition.x = refPercent;
		}
		else{
			mgame_starter.mplayerCharacter.Charactor_Avatar->setPositionX(refPercent);
		}
		mmapCreateSceneUI.setPlayerUITesxtLabel(refPercent, mplayerPositionThreeLayerSettingUI.UIText.at(1));
	});
	mplayerPositionThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		int refPercent = ((Slider*)ref)->getPercent();
		if (mplayer_StartPosition_State){
			mgame_starter.mmapNowBlockInformation.Player_StartPosition.y = refPercent;
		}
		else{
			mgame_starter.mplayerCharacter.Charactor_Avatar->setPositionY(refPercent);
		}
		mmapCreateSceneUI.setPlayerUITesxtLabel(refPercent, mplayerPositionThreeLayerSettingUI.UIText.at(2));
	});


#pragma endregion THREE_PLAYER_POSITION_LAYER_TARGETING

#pragma region THREE_PLAYER_SIZE_LAYER_TARGETING

	mplayerSizeThreeLayerSettingUI.UIMargin = Size(30.0f, 0.0f);
	mplayerSizeThreeLayerSettingUI.UISize = Size(100.0f, 30.0f);
	mplayerSizeThreeLayerSettingUI.TestUIEncho = 1.0f;
	mplayerSizeThreeLayerSettingUI.UIBenchmark = Vec2(0.0f, mplayerTargetTwoLayerChoiceUI.UIBenchmark.y - mplayerTargetTwoLayerChoiceUI.UISize.height);

	mplayerSizeThreeLayerSettingUI.UIText.push_back("Width");
	mplayerSizeThreeLayerSettingUI.UIText.push_back("Height");

	mplayerSizeThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mplayerSizeThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);

	mplayerSizeThreeLayerSettingUI.SliderMax.push_back(1000);
	mplayerSizeThreeLayerSettingUI.SliderMax.push_back(1000);

	mplayerSizeThreeLayerSettingUI.SliderNow.push_back(10);
	mplayerSizeThreeLayerSettingUI.SliderNow.push_back(10);

	mplayerSizeThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mgame_starter.mplayerCharacter.Charactor_Avatar->setScaleX(((Slider*)ref)->getPercent());
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerSizeThreeLayerSettingUI.UIText.at(0));
	});

	mplayerSizeThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mgame_starter.mplayerCharacter.Charactor_Avatar->setScaleY(((Slider*)ref)->getPercent());
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerSizeThreeLayerSettingUI.UIText.at(1));
	});


#pragma endregion THREE_PLAYER_SIZE_LAYER_TARGETING

#pragma region THREE_PLAYER_SPEED_LAYER_TARGETING

	mplayerSpeedThreeLayerSettingUI.UIMargin = Size(30.0f, 0.0f);
	mplayerSpeedThreeLayerSettingUI.UISize = Size(100.0f, 30.0f);
	mplayerSpeedThreeLayerSettingUI.TestUIEncho = 1.0f;
	mplayerSpeedThreeLayerSettingUI.UIBenchmark = Vec2(0.0f, mplayerTargetTwoLayerChoiceUI.UIBenchmark.y - mplayerTargetTwoLayerChoiceUI.UISize.height);

	mplayerSpeedThreeLayerSettingUI.UIText.push_back("Second Speed");
	mplayerSpeedThreeLayerSettingUI.UIText.push_back("Run Speed");
	mplayerSpeedThreeLayerSettingUI.UIText.push_back("Stop Speed");

	mplayerSpeedThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mplayerSpeedThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mplayerSpeedThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);

	mplayerSpeedThreeLayerSettingUI.SliderMax.push_back(1000);
	mplayerSpeedThreeLayerSettingUI.SliderMax.push_back(1000);
	mplayerSpeedThreeLayerSettingUI.SliderMax.push_back(1000);

	mplayerSpeedThreeLayerSettingUI.SliderNow.push_back(10);
	mplayerSpeedThreeLayerSettingUI.SliderNow.push_back(10);
	mplayerSpeedThreeLayerSettingUI.SliderNow.push_back(10);

	mplayerSpeedThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		PLAYERCHAR_WORKING.Charactor_WorkingSecondSpeed = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerSpeedThreeLayerSettingUI.UIText.at(0));
	});
	mplayerSpeedThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		PLAYERCHAR_WORKING.Charactor_WorkingMaxSpeed = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerSpeedThreeLayerSettingUI.UIText.at(1));
	});
	mplayerSpeedThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		PLAYERCHAR_WORKING.Charactor_StopSpeed = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerSpeedThreeLayerSettingUI.UIText.at(2));
	});

#pragma endregion THREE_PLAYER_SPEED_LAYER_TARGETING

#pragma region THREE_PLAYER_JUMP_LAYER_TARGETING

	mplayerJumpThreeLayerSettingUI.UIMargin = Size(30.0f, 0.0f);
	mplayerJumpThreeLayerSettingUI.UISize = Size(100.0f, 30.0f);
	mplayerJumpThreeLayerSettingUI.TestUIEncho = 1.0f;
	mplayerJumpThreeLayerSettingUI.UIBenchmark = Vec2(0.0f, mplayerTargetTwoLayerChoiceUI.UIBenchmark.y - mplayerTargetTwoLayerChoiceUI.UISize.height);

	mplayerJumpThreeLayerSettingUI.UIText.push_back("Jump Power");
	mplayerJumpThreeLayerSettingUI.UIText.push_back("Gravity Power");
	mplayerJumpThreeLayerSettingUI.UIText.push_back("Gravity Max\n Power");

	mplayerJumpThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mplayerJumpThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);
	mplayerJumpThreeLayerSettingUI.UIType.push_back(eUIKind::SLIDER);

	mplayerJumpThreeLayerSettingUI.SliderMax.push_back(1000);
	mplayerJumpThreeLayerSettingUI.SliderMax.push_back(1000);
	mplayerJumpThreeLayerSettingUI.SliderMax.push_back(1000);

	mplayerJumpThreeLayerSettingUI.SliderNow.push_back(10);
	mplayerJumpThreeLayerSettingUI.SliderNow.push_back(10);
	mplayerJumpThreeLayerSettingUI.SliderNow.push_back(10);

	mplayerJumpThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		PLAYERCHAR_JUMP.Charator_JumpPower = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerJumpThreeLayerSettingUI.UIText.at(1));
	});
	mplayerJumpThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mgame_starter.Gravity_SecondPower = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerJumpThreeLayerSettingUI.UIText.at(1));
	});
	mplayerJumpThreeLayerSettingUI.MyUserInterfaceEvent.push_back([&](Ref* ref){
		mmapCreateSceneUI.RemovePlayerInterface(FOUR_LAYER);
		mgame_starter.Gravity_MaxPower = ((Slider*)ref)->getPercent();
		mmapCreateSceneUI.setPlayerUITesxtLabel(((Slider*)ref)->getPercent(), mplayerJumpThreeLayerSettingUI.UIText.at(1));
	});

#pragma endregion THREE_PLAYER_JUMP_LAYER_TARGETING

#pragma region FOUR_PLAYER_LOADFILE_LAYER_QUESTION

	mplayerLoadThreeLayerChoiceUI.UIMargin = Size(30.0f, 0.0f);
	mplayerLoadThreeLayerChoiceUI.UISize = Size(100.0f, 30.0f);
	mplayerLoadThreeLayerChoiceUI.TestUIEncho = 1.0f;
	mplayerLoadThreeLayerChoiceUI.UIBenchmark = Vec2(500.0f - mplayerLoadThreeLayerChoiceUI.UIMargin.width - mplayerLoadThreeLayerChoiceUI.UISize.width *2.0, 500.0f);


#pragma endregion FOUR_PLAYER_LOADFILE_LAYER_QUESTION

#pragma region FOUR_PLAYER_SAVEFILE_LAYER_QUESTION

	mplayerSaveThreeLayerChoiceUI.UIMargin = Size(30.0f, 0.0f);
	mplayerSaveThreeLayerChoiceUI.UISize = Size(100.0f, 30.0f);
	mplayerSaveThreeLayerChoiceUI.TestUIEncho = 1.0f;
	mplayerSaveThreeLayerChoiceUI.UIBenchmark = Vec2(500.0f - mplayerSaveThreeLayerChoiceUI.UIMargin.width - mplayerSaveThreeLayerChoiceUI.UISize.width *2.0, 500.0f);

	mplayerSaveThreeLayerChoiceUI.UIText.push_back("No");
	mplayerSaveThreeLayerChoiceUI.UIText.push_back("Yes");

	mplayerSaveThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);
	});
	mplayerSaveThreeLayerChoiceUI.MyUserInterfaceEvent.push_back([&](Ref*){
		mmapCreateSceneUI.RemovePlayerInterface(THREE_LAYER);

		std::stringstream CharDataFileName;
		CharDataFileName.str("PlayerCharactor_ALLData0.bin");
		int RemoveNum_Name = 0;

		for (int i = 0; i < mplayerDataPorther_Size; i++){
			if (!CharDataFileName.str().compare(mplayerData_FILEName[i])){
				RemoveNum_Name++;
				CharDataFileName.str("");
				CharDataFileName << "Char_Data_FILE/PlayerCharactor_ALLData" << RemoveNum_Name << ".bin";
			}
		}

		FILE* PlayerCharactor_File = fopen(CharDataFileName.str().c_str(), "rb");
		CharactorData* Into_FileCharData = (CharactorData*)malloc(sizeof(CharactorData));
		Into_FileCharData = &mgame_starter.mplayerCharacter;
		fwrite(Into_FileCharData, sizeof(CharactorData), 1, PlayerCharactor_File);

		fclose(PlayerCharactor_File);
		free(Into_FileCharData);

	});

#pragma endregion FOUR_PLAYER_SAVEFILE_LAYER_QUESTION

}
/*
새로운 맵 만들기
player의 위치를 재생성
map과 player의 데이터를 저장해놓고싶다.
*/

/*
game algorithm의 함수를 불러올때 sprite의 값을 변경한다. */
void MakeMapScene::mmove_Location_BlockSetting(){

	mmap_Exist = true;

	mgame_starter.mmapNowBlockInformation.mgameStage_LoadBlock(mmapData_FILEName[mmapData_FILENum]);
	mgame_starter.mallMapBlock = mgame_starter.mmapNowBlockInformation.mreturnBlock_Setting();
	mgame_starter.mplayerNearBlock = mgame_starter.mallMapBlock;

	for (auto i = mgame_starter.mallMapBlock.begin(); i < mgame_starter.mallMapBlock.end(); i++){
		mgame_starter.mgameAlgorithm_Layer->addChild(*i);
	}

	mgame_starter.mplayerCharacter.Charactor_Avatar = Sprite::create("playerchar.png");
	mgame_starter.mplayerCharacter.Charactor_Avatar->setAnchorPoint(Vec2(1.0f, 1.0f));
	mgame_starter.mplayerCharacter.Charactor_Avatar->setScaleX(CONST_X_PERCENT * 20.0f);
	mgame_starter.mplayerCharacter.Charactor_Avatar->setScaleY(CONST_Y_PERCENT * 40.0f);
	mgame_starter.mplayerCharacter.Charactor_Avatar->setPositionX(CONST_X_PERCENT * mgame_starter.mmapNowBlockInformation.Player_StartPosition.x);
	mgame_starter.mplayerCharacter.Charactor_Avatar->setPositionY(CONST_Y_PERCENT * mgame_starter.mmapNowBlockInformation.Player_StartPosition.y);
	mgame_starter.mgameAlgorithm_Layer->addChild(mgame_starter.mplayerCharacter.Charactor_Avatar);
	
	for (int i = 0; i < mgame_starter.mmapNowBlockInformation.Block_Kinds.size(); i++){
		if (mgame_starter.mmapNowBlockInformation.Block_Kinds.at(i)->Block_Attribute & MOVE_BIT){
			mmove_Location_Block.push_back(Move_Loaction_Block(i));
			for (int k = 0; k < MOVEBLOCK_AT->Moving_Echo.size(); k++){
				mmove_Location_Block.back().mlocation_Block.push_back(Sprite::create("playerchar.png"));
				mmove_Location_Block.back().mlocation_Block.back()->setAnchorPoint(Vec2(1.0f, 1.0f));
				mmove_Location_Block.back().mlocation_Block.back()->setScaleX(CONST_X_PERCENT * MOVE_LOCATION_W);
				mmove_Location_Block.back().mlocation_Block.back()->setScaleY(CONST_Y_PERCENT * MOVE_LOCATION_H);
				
				mmove_Location_Block.back().mlocation_Block.back()->setPositionX
					(mgame_starter.mallMapBlock.at(i)->getPositionX() +
					(CONST_X_PERCENT * MOVE_LOCATION_W * (k + 1) * MOVEBLOCK_AT->Moving_Echo.at(k).x));
				mmove_Location_Block.back().mlocation_Block.back()->setPositionX
					(mgame_starter.mallMapBlock.at(i)->getPositionY() +
					(CONST_Y_PERCENT * MOVE_LOCATION_H * (k + 1) * MOVEBLOCK_AT->Moving_Echo.at(k).y));

				mcreating_MoveLocationBlock->addChild(mmove_Location_Block.back().mlocation_Block.back());
			}
		}
	}

}
void MakeMapScene::mremove_MakingMap_AllBlock(){

	if (mmove_Location_Block.size() > 0){
		mgame_starter.mremoveStage_AllBlock();
		mmove_Location_Block.clear();
	}
	if (mgame_starter.mallMapBlock.size() > 0){
		mgame_starter.mremoveStage_AllBlock();
	}
}

void MakeMapScene::onMouseDown(Event* mevent){

	EventMouse* MouseEvent = (EventMouse*)mevent;

	/*
	조금더 지식이 필요
	2번쨰 레이어의 button을 누르면 이곳이 실행된다.
	방법 1 : mouse이밴트를 최고 상위 layer로 바꾼다.

	mmapCreateSceneUI.RemovePlayerInterface(TWO_LAYER);

	*/

	for (int i = 0; i < mgame_starter.mallMapBlock.size(); i++){
		if (MouseEvent->getCursorX() < mgame_starter.mallMapBlock.at(i)->getPositionX()&& MouseEvent->getCursorX() > BLOCK_HALF_WIDTH
			&& MouseEvent->getCursorY() <mgame_starter.mallMapBlock.at(i)->getPositionY() && MouseEvent->getCursorY() > BLOCK_HALF_HEIGHT){

			mchoise_Move_Connect_Block = -1;
			mchoise_Move_Location = -1;

			mchoise_Block = i;

			if (mgame_starter.mmapNowBlockInformation.Block_Kinds.at(mchoise_Block)->Block_Attribute & MOVE_BIT){
				mchoise_Move_Connect_Block = i;
				mchoise_Move_Location = 0;
			}

			if (mremove_Block_State
				&& mchoise_Move_Connect_Block >= 0
				&& mmove_Location_Block.at(i).mconnect_Block_Num == mchoise_Block){

				mmove_Location_Block.erase(mmove_Location_Block.begin() + i);
				mchoise_Move_Connect_Block = -1;
				mgame_starter.mremoveStage_OneBlock(mchoise_Block);
			}
			mmouseMoveLocation = Vec2(MouseEvent->getCursorX(), MouseEvent->getCursorY());
			mmouse_state = CLICK_B;
			return;
		}
	}
	for (int i = 0; i < mmove_Location_Block.size(); i++){
		for (int k = 0; k < mmove_Location_Block.at(i).mlocation_Block.size(); k++){
			if (MouseEvent->getCursorX() < mmove_Location_Block.at(i).mlocation_Block.at(k)->getPositionX()
				&& MouseEvent->getCursorX() > MOVE_BLOCK_SCALE_WIDTH
				&& MouseEvent->getCursorY() < mmove_Location_Block.at(i).mlocation_Block.at(k)->getPositionY()
				&& MouseEvent->getCursorY() > MOVE_BLOCK_SCALE_HEIGHT){

				mchoise_Move_Connect_Block = i;
				mchoise_Move_Location = k;

				mchoise_Block = mmove_Location_Block.at(i).mconnect_Block_Num;

				if (mremove_MoveLocationBlock_State){

					mcreating_MoveLocationBlock->removeChild(mmove_Location_Block.at(i).mlocation_Block.at(k));
					mmove_Location_Block.at(i).mlocation_Block.erase(mmove_Location_Block.at(i).mlocation_Block.begin() + mchoise_Move_Location);
					mchoise_Move_Location--;
				}
				if (mmove_Location_Block.at(i).mlocation_Block.size() == 0){
					mmoveLocationBenchMark = mgame_starter.mallMapBlock.at(mmove_Location_Block.at(i).mconnect_Block_Num)->getPosition();
				}
				else{
					mmoveLocationBenchMark = mmove_Location_Block.at(i).mlocation_Block.at(mchoise_Move_Location)->getPosition();
				}
				mmouse_state = CLICK_M;
				CCLOG("%d", mmouse_state);
				return;
			}
		}
	}
}
void MakeMapScene::onMouseUP(Event* mevent){

	mmouse_state = 0;

}

void MakeMapScene::onMouseMove(Event* mevent){

	if (mmouse_state == 0 || mgame_starter.mallMapBlock.size() ==0){
		return;
	}

	EventMouse* MouseEvent = (EventMouse*)mevent;

	Vec2 MouseMovePercent = Vec2(MouseEvent->getCursorX(), MouseEvent->getCursorY()) - mmouseMoveLocation;

	Vec2 Block_PercentAdd_Percent = Vec2(0.0f, 0.0f);

	Vec2 Move_BenchoPoint = Vec2(10.0f, 10.0f);

	mmouseMovePercentLocaition += MouseMovePercent;
	MouseMovePercent = Vec2(0.0f, 0.0f);

	if (mmouse_state & CLICK_M){
			Move_BenchoPoint = mgame_starter.mallMapBlock.at(mchoise_Block)->getPosition()
				- mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.at(mchoise_Move_Location)->getPosition();
			if (Move_BenchoPoint.x > 0.0f && Move_BenchoPoint.y > 0.0f){

			}
			if (Move_BenchoPoint.x > Move_BenchoPoint.y){
				Move_BenchoPoint.x = 10.0f;
				Move_BenchoPoint.y = 10.0f;
			}

	}

	/*
	마우스로 블럭을 움직일이는 알고리즘*/
	while (true)
	{
		if (mmouseMovePercentLocaition.x > CONST_X_PERCENT * Move_BenchoPoint.x && mmouseMovePercentLocaition.x > 0){
			MouseMovePercent.x += CONST_X_PERCENT * Move_BenchoPoint.x;
			mmouseMovePercentLocaition.x -= CONST_X_PERCENT * Move_BenchoPoint.x;
			Block_PercentAdd_Percent.x += Move_BenchoPoint.x;
		}
		else if (mmouseMovePercentLocaition.x < CONST_X_PERCENT * Move_BenchoPoint.x && mmouseMovePercentLocaition.x < 0){
			MouseMovePercent.x -= CONST_X_PERCENT * Move_BenchoPoint.x;
			mmouseMovePercentLocaition.x += CONST_X_PERCENT * Move_BenchoPoint.x;
			Block_PercentAdd_Percent.x -= Move_BenchoPoint.x;
		}
		else{
			break;
		}
	}
	while (true)
	{
		if (mmouseMovePercentLocaition.y > CONST_Y_PERCENT * Move_BenchoPoint.y && mmouseMovePercentLocaition.y > 0){
			MouseMovePercent.y += CONST_Y_PERCENT * Move_BenchoPoint.y;
			mmouseMovePercentLocaition.y -= CONST_Y_PERCENT * Move_BenchoPoint.y;
			Block_PercentAdd_Percent.y += Move_BenchoPoint.y;
		}
		else if (mmouseMovePercentLocaition.y < CONST_Y_PERCENT * Move_BenchoPoint.y && mmouseMovePercentLocaition.y < 0){
			MouseMovePercent.y -= CONST_Y_PERCENT * Move_BenchoPoint.y;
			mmouseMovePercentLocaition.y += CONST_Y_PERCENT * Move_BenchoPoint.y;
			Block_PercentAdd_Percent.y -= Move_BenchoPoint.y;
		}
		else{
			break;
		}
	}

	if (mmouse_state & CLICK_M){

		for (auto i = mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.begin();
			i < mmove_Location_Block.at(mchoise_Move_Connect_Block).mlocation_Block.end(); i++){
			 (*i)->setPosition(MouseMovePercent + (*i)->getPosition());
		}

		return;
	}

	mgame_starter.mallMapBlock.at(mchoise_Block)->setPosition(MouseMovePercent
		+ mgame_starter.mallMapBlock.at(mchoise_Block)->getPosition());
	/*
	세이브 블럭을 저장할때 Block_Position에 값으로 저장한다.*/
	mgame_starter.mmapNowBlockInformation.Block_Position.at(mchoise_Block) += Block_PercentAdd_Percent;
	
	/*
	move블럭이 움직일경우 같이 연결된 location block도 움직인다.*/
	if (GAMESTAGE_BLOCK->Block_Attribute & MOVE_BIT){
		for (auto i = mmove_Location_Block.begin(); i < mmove_Location_Block.end(); i++){
			for (auto k = (*i).mlocation_Block.begin(); k < (*i).mlocation_Block.end() && (*i).mconnect_Block_Num == mchoise_Block; k++){
				(*k)->setPosition(MouseMovePercent + (*k)->getPosition());
			}
		}
	}

	mmouseMoveLocation = Vec2(MouseEvent->getCursorX(), MouseEvent->getCursorY());

}

void MakeMapScene::mmove_GameAlgorithm(float dt){
	mgame_starter.mplayerMovementSchedule(dt);
	mgame_starter.mmapMoveBlockWorkSchedule(dt);
}
/*
나중에
맵의 정보를 label에서 받아 저장하면 그 저장한 것을 전달학수 있는 함수

지금
블럭을 맞게 생성하는 것

*/