#include "MapDesign.h"

#define MOVEBLOCK_AT(NUM) ((Moving_Block*)Block_Kinds.at(NUM))
#define DAMAGEBLOCK_AT(NUM) ((Damage_Block*)Block_Kinds.at(NUM))

#define MOVEBLOCK_BACK ((Moving_Block*)Block_Kinds.back())
#define DAMAGEBLOCK_BACK ((Damage_Block*)Block_Kinds.back())

Sprite* GameStageBlock::maddBlock(int into_BlockKind){

	Sprite* returnSprite;

	float BlockCreatePosition = 500.0f;
	float BlockScale = 100.0f;

	char* Block_Image;

	switch (into_BlockKind)
	{
	case BASIC_BIT:
		Block_Kinds.push_back(new Basic_Block);
		returnSprite = Sprite::create("Basic_Block.png");
		break;
	case DAMAGE_BIT:
		Block_Kinds.push_back(new Damage_Block);
		returnSprite = Sprite::create("Damage_Block.png");
		DAMAGEBLOCK_BACK->Block_Damage = 1;
		break;
	case MOVE_BIT:
		Block_Kinds.push_back(new Moving_Block);
		returnSprite = Sprite::create("Move_Block.png");

		MOVEBLOCK_BACK->Moving_Speed = 2.0f;
		MOVEBLOCK_BACK->Moving_Location.push_back(10.0f);
		MOVEBLOCK_BACK->Moving_Echo.push_back(Vec2(1.0f,0.0f));
		break;
	default:
		assert(("maddBlock into_BlockKind가 값을 초과하였습니다.", 0));
		break;
	}
	
	Block_Kinds.back()->Block_Attribute = into_BlockKind;

	Block_Size.push_back(Size(BlockScale, BlockScale*2.0f));
	Block_Position.push_back(Vec2(BlockCreatePosition, BlockCreatePosition));
	Block_Visible.push_back(false);

	returnSprite->setAnchorPoint(Vec2(1.0f, 1.0f));

	returnSprite->setPosition(Vec2(BlockCreatePosition *CONST_X_PERCENT, BlockCreatePosition*CONST_Y_PERCENT));

	returnSprite->setScaleX(Block_Size.back().width * CONST_X_PERCENT);
	returnSprite->setScaleY(Block_Size.back().height * CONST_Y_PERCENT );

	return returnSprite;
}

std::vector<Sprite*> GameStageBlock::mreturnBlock_Setting(){

	std::vector<Sprite*> Making_Block = std::vector<Sprite*>(0);

	for (int i = 0; i < Block_Size.size(); i++){

		switch (Block_Kinds.at(i)->Block_Attribute)
		{
		case BASIC_BIT:
			Making_Block.push_back(Sprite::create("Basic_Block.png"));
			break;
		case DAMAGE_BIT:
			Making_Block.push_back(Sprite::create("Cutter_Block.png"));
			break;
		case MOVE_BIT:
			Making_Block.push_back(Sprite::create("Move_Block.png"));
			break;
		default:
			assert(("maddBlock into_BlockKind가 값을 초과하였습니다.", 0));
			break;
		}

		Making_Block.back()->setPosition(Vec2(Block_Position.at(i).x *CONST_X_PERCENT, Block_Position.at(i).y *CONST_Y_PERCENT));

		Making_Block.back()->setAnchorPoint(Vec2(1.0f, 1.0f));

		Making_Block.back()->setScaleX(Block_Size.at(i).width * CONST_X_PERCENT);
		Making_Block.back()->setScaleY(Block_Size.at(i).height  * CONST_Y_PERCENT);
	}
	return Making_Block;
}

void GameStageBlock::mgameStage_SaveBlock(const char* saveFileName){
	std::stringstream FileName;
	FileName << "Map_Data_FILE/" << saveFileName;
	FILE* StageBLock_Save = fopen(saveFileName, "wb");
	fprintf(StageBLock_Save, "%d\n", Block_Kinds.size());
	for (int i = 0; i < Block_Kinds.size(); i++){
		fprintf(StageBLock_Save, "%d\n", Block_Kinds.at(i)->Block_Attribute);

		switch (Block_Kinds.at(i)->Block_Attribute){
		case 1:
			fprintf(StageBLock_Save, "\n");
			break;
		case 2:
			fprintf(StageBLock_Save, "%d\n", DAMAGEBLOCK_AT(i)->Block_Damage);
			break;
		case 4:
			fprintf(StageBLock_Save, "%d\n", MOVEBLOCK_AT(i)->Moving_Location.size());
			for (int k = 0; k < MOVEBLOCK_AT(i)->Moving_Location.size(); k++){
				fprintf(StageBLock_Save, "%f %f\n", MOVEBLOCK_AT(i)->Moving_Echo.at(k).x, MOVEBLOCK_AT(i)->Moving_Echo.at(k).y);
				fprintf(StageBLock_Save, "%f\n", MOVEBLOCK_AT(i)->Moving_Location.at(k));
			}
			fprintf(StageBLock_Save, "%f\n", MOVEBLOCK_AT(i)->Moving_Speed);
			break;
		}
	}
	fprintf(StageBLock_Save, "%d\n", Block_Visible.size());
	for (int i = 0; i < Block_Visible.size(); i++){
		fprintf(StageBLock_Save, "%d\n", Block_Visible.at(i));
		fprintf(StageBLock_Save, "%f %f\n", Block_Size.at(i).width, Block_Size.at(i).height);
		fprintf(StageBLock_Save, "%f %f\n", Block_Position.at(i).x, Block_Position.at(i).y);
	}
	fprintf(StageBLock_Save, "%f %f\n", Player_StartPosition.x, Player_StartPosition.y);
	fclose(StageBLock_Save);
}
void GameStageBlock::mgameStage_LoadBlock(char* loadFileName){
	std::stringstream FileName;
	FileName << "Map_Data_FILE/" << loadFileName;
	FILE* StageBLock_Load = fopen(FileName.str().c_str(), "rb");
	int StageLoad_AllSize = 0;
	int StageBlock_Location = 0;
	fscanf(StageBLock_Load, "%d\n", &StageLoad_AllSize);
	Block_Kinds = std::vector<Basic_Block*>(StageLoad_AllSize);

	float StageBlock_One = 0.0f;

	float StageBlock_X = 0.0f;
	float StageBlock_Y = 0.0f;

	for (int i = 0; i < Block_Kinds.size(); i++){
		fscanf(StageBLock_Load, "%d\n", &StageBlock_Location);

		switch (StageBlock_Location){
		case 1:
			Block_Kinds.at(i) = new Basic_Block;
			fscanf(StageBLock_Load, "\n", "");
			break;
		case 2:
			Block_Kinds.at(i) = new Damage_Block;
			fscanf(StageBLock_Load, "%d\n", &DAMAGEBLOCK_AT(i)->Block_Damage);
			break;
		case 4:
			Block_Kinds.at(i) = new Moving_Block;
			fscanf(StageBLock_Load, "%d\n", &StageLoad_AllSize);

			for (int k = 0; k < StageLoad_AllSize; k++){
				fscanf(StageBLock_Load, "%f %f\n", &StageBlock_X, &StageBlock_Y);
				MOVEBLOCK_AT(i)->Moving_Echo.push_back(Vec2(StageBlock_X, StageBlock_Y));
				fscanf(StageBLock_Load, "%f\n", &StageBlock_One);
				MOVEBLOCK_AT(i)->Moving_Location.push_back(StageBlock_One);
			}
			fscanf(StageBLock_Load, "%f\n", &MOVEBLOCK_AT(i)->Moving_Speed);
			break;
		}
		Block_Kinds.at(i)->Block_Attribute = StageBlock_Location;
	}
	fscanf(StageBLock_Load, "%d\n", &StageLoad_AllSize);

	for (int i = 0; i < StageLoad_AllSize; i++){
		fscanf(StageBLock_Load, "%d\n", &StageBlock_One);
		Block_Visible.push_back(StageBlock_One);

		fscanf(StageBLock_Load, "%f %f\n", &StageBlock_X, &StageBlock_Y);
		Block_Size.push_back(Size(StageBlock_X, StageBlock_Y));

		fscanf(StageBLock_Load, "%f %f\n", &StageBlock_X, &StageBlock_Y);
		Block_Position.push_back(Size(StageBlock_X, StageBlock_Y));
	}
	fscanf(StageBLock_Load, "%f %f\n", &StageBlock_X, &StageBlock_Y);
	Player_StartPosition = Size(StageBlock_X, StageBlock_Y);

	fclose(StageBLock_Load);
}

void GameStageBlock::mremoveAllGameStageBlock(){
	Player_StartPosition = Vec2(500.0f, 500.0f);
	Block_Position.clear();
	Block_Visible.clear();
	for (auto i = Block_Kinds.begin(); i < Block_Kinds.end(); i++){
		delete(*i);
	}
	Block_Kinds.clear();
	Block_Size.clear();
}
void GameStageBlock::mremoveOneGameStageBlock(int into_BlockNum){

	Block_Position.erase(Block_Position.begin() + into_BlockNum);
	Block_Visible.erase(Block_Visible.begin() + into_BlockNum);
	delete(Block_Kinds.at(into_BlockNum));
	Block_Kinds.erase(Block_Kinds.begin() + into_BlockNum);
	Block_Size.erase(Block_Size.begin() + into_BlockNum);

}