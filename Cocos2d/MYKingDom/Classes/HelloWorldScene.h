#pragma once

#include "MY_UserInterface.h"

class HelloWorld : public cocos2d::LayerColor
{
public:
	static cocos2d::Scene* createScene();

    virtual bool init();
    CREATE_FUNC(HelloWorld);

private:

	IFrameUserInterface muserinterface;

	UIVariable mplayerStartSceneUI;
	UIVariable mplayerContinueSceneUI;
	UIVariable mplayerOptionSceneUI;

	void resetHelloWorldALLUIValue();


	/*
	Player Ui EvnetLisnter*/
	/*
	button*/
	void startSceneStartButtonEvent(Ref* ref);
	void startSceneContinueButtonEvent(Ref* ref);
	void startSceneOptionButtonEvent(Ref* ref);
	void startSceneExitButtonEvent(Ref* ref);

	void continueSceneBackButtonEvent(Ref* ref);

	void optionSceneBacgroundSoundSliderEvent(Ref* ref, Slider::EventType et);
	void optionSceneActionSoundSliderEvent(Ref* ref, Slider::EventType et);
	void optionSceneScreenSizeSliderEvent(Ref* ref, Slider::EventType et);
	void optionSceneBackButtonEvent(Ref* ref);

};

bool InputContinueSceneFileData(char* fileName, std::vector<char*>* outputplayerdata);