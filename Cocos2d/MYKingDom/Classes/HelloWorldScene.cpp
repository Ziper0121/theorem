#include "HelloWorldScene.h"
#include "MakeMapScene.h"
#include <cocostudio/CocoStudio.h>
#include <iostream>
#include <fstream>
#include <string>

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::create();
	auto layer = HelloWorld::create();
	scene->addChild(layer);
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}
	resetHelloWorldALLUIValue();

	addChild(muserinterface.AddUIInforMation(mplayerStartSceneUI));
	
    return true;
}
/*"Big_Angled.ttf"
"Codystar-Light.ttf"
"DrumagStudioNF.ttf"
"gomarice_no_continue.ttf"
"Messages.ttf"
"gomarice_syouwa_retro_pop.ttf"
"Monofett.ttf"
"Oh,Maria.ttf"
"Sofija.ttf" 
*/
void HelloWorld::resetHelloWorldALLUIValue(){

#pragma region OptionScene_UI

	int OptionSceneUiSize = 4;

	mplayerOptionSceneUI.UIBenchmark = Vec2(500, 500);
	mplayerOptionSceneUI.UIMargin = Size(0.0f, 30.0f);
	mplayerOptionSceneUI.UISize = Size(800.0f, 80.0f);
	mplayerOptionSceneUI.TestUIEncho = -1.0f;

	/*
	배경소리
	액션소리

	화면크기
	뒤로가기
	*/
	mplayerOptionSceneUI.UIText = std::vector<char*>(OptionSceneUiSize);
	mplayerOptionSceneUI.UIText.at(0) = "Bacground Sound";
	mplayerOptionSceneUI.UIText.at(1) = "Action Sound";
	mplayerOptionSceneUI.UIText.at(2) = "Screen Size";
	mplayerOptionSceneUI.UIText.at(3) = "Back";

	mplayerOptionSceneUI.UIType = std::vector<eUIKind>(OptionSceneUiSize);
	mplayerOptionSceneUI.UIType.at(0) = eUIKind::SLIDER;
	mplayerOptionSceneUI.UIType.at(1) = eUIKind::SLIDER;
	mplayerOptionSceneUI.UIType.at(2) = eUIKind::SLIDER;
	mplayerOptionSceneUI.UIType.at(3) = eUIKind::BUTTON;

	mplayerOptionSceneUI.SliderMax = std::vector<int>(OptionSceneUiSize - 1);
	mplayerOptionSceneUI.SliderMax.at(0) = 100;
	mplayerOptionSceneUI.SliderMax.at(1) = 100;
	mplayerOptionSceneUI.SliderMax.at(2) = 100;
	mplayerOptionSceneUI.SliderNow = std::vector<int>(OptionSceneUiSize - 1);
	mplayerOptionSceneUI.SliderNow.at(0) = 100;
	mplayerOptionSceneUI.SliderNow.at(1) = 100;
	mplayerOptionSceneUI.SliderNow.at(2) = 100;

	mplayerOptionSceneUI.MySliderEvent = std::vector<std::function<void(Ref*, Slider::EventType)>>(3);
	mplayerOptionSceneUI.MySliderEvent.at(0) =
		std::bind(&HelloWorld::optionSceneBacgroundSoundSliderEvent, this, std::placeholders::_1, std::placeholders::_2);
	mplayerOptionSceneUI.MySliderEvent.at(1) =
		std::bind(&HelloWorld::optionSceneActionSoundSliderEvent, this, std::placeholders::_1, std::placeholders::_2);
	mplayerOptionSceneUI.MySliderEvent.at(2) =
		std::bind(&HelloWorld::optionSceneScreenSizeSliderEvent, this, std::placeholders::_1, std::placeholders::_2);

	mplayerOptionSceneUI.MyUserInterfaceEvent = std::vector<std::function<void(Ref*)>>(1);
	mplayerOptionSceneUI.MyUserInterfaceEvent.at(0) =
		std::bind(&HelloWorld::optionSceneBackButtonEvent, this, std::placeholders::_1);
#pragma endregion OptionScene_UI

#pragma region ContinueScene_UI

	/*
	PlayerTextDatas는 플레이어의 저장 기록을  
	파일에서 텍스트로 받는 변수이다.*/
	std::vector<char*> PlayerTextData;

	mplayerContinueSceneUI.UIBenchmark = Vec2(500, 500);
	mplayerContinueSceneUI.UIMargin = Size(0.0f, 30.0f);
	mplayerContinueSceneUI.UISize = Size(800.0f, 80.0f);
	mplayerContinueSceneUI.TestUIEncho = -1.0f;

	if (InputContinueSceneFileData("PlayerData.txt", &PlayerTextData)){
		int ContinueUITextSize = PlayerTextData.size();

		mplayerContinueSceneUI.UIText = PlayerTextData;
		mplayerContinueSceneUI.UIType = std::vector<eUIKind>(0);
		mplayerContinueSceneUI.MyUserInterfaceEvent = std::vector<std::function<void(Ref*)>>(1);

		for (int i = 0; i < ContinueUITextSize; i++){
			mplayerContinueSceneUI.UIType.push_back(eUIKind::BUTTON);
			mplayerContinueSceneUI.MyUserInterfaceEvent.push_back([=](Ref* ref){

			});
		}

	}
	/*
	필요한 모든것*/
	mplayerContinueSceneUI.UIText.push_back("Back");
	mplayerContinueSceneUI.UIType.push_back(eUIKind::BUTTON);
	mplayerContinueSceneUI.MyUserInterfaceEvent.push_back(std::bind(&HelloWorld::continueSceneBackButtonEvent, this, std::placeholders::_1));


#pragma endregion ContinueScene_UI

#pragma region StartScene_UI

	int StartSceneUISize = 4;

	mplayerStartSceneUI.UIBenchmark = Vec2(500, 500);
	mplayerStartSceneUI.UIMargin = Size(0.0f, 30.0f);
	mplayerStartSceneUI.UISize = Size(80.0f, 80.0f);
	mplayerStartSceneUI.TestUIEncho = -1.0f;
	
	mplayerStartSceneUI.UIText = std::vector<char*>(StartSceneUISize);
	mplayerStartSceneUI.UIText.at(0) = "New Player.bin";
	mplayerStartSceneUI.UIText.at(1) = "test";
	mplayerStartSceneUI.UIText.at(2) = "Option";
	mplayerStartSceneUI.UIText.at(3) = "End";

	mplayerStartSceneUI.UIType = std::vector<eUIKind>(StartSceneUISize);
	mplayerStartSceneUI.UIType.at(0) = eUIKind::BUTTON;
	mplayerStartSceneUI.UIType.at(1) = eUIKind::BUTTON;
	mplayerStartSceneUI.UIType.at(2) = eUIKind::BUTTON;
	mplayerStartSceneUI.UIType.at(3) = eUIKind::BUTTON;

	mplayerStartSceneUI.MyUserInterfaceEvent = std::vector<std::function<void(Ref*)>>(StartSceneUISize);
	mplayerStartSceneUI.MyUserInterfaceEvent.at(0) = std::bind(&HelloWorld::startSceneStartButtonEvent, this, std::placeholders::_1);
	mplayerStartSceneUI.MyUserInterfaceEvent.at(1) = std::bind(&HelloWorld::startSceneContinueButtonEvent, this, std::placeholders::_1);
	mplayerStartSceneUI.MyUserInterfaceEvent.at(2) = std::bind(&HelloWorld::startSceneOptionButtonEvent, this, std::placeholders::_1);
	mplayerStartSceneUI.MyUserInterfaceEvent.at(3) = std::bind(&HelloWorld::startSceneExitButtonEvent, this, std::placeholders::_1);
#pragma endregion StartScene_UI

}
void HelloWorld::startSceneStartButtonEvent(Ref* ref){
}
void HelloWorld::startSceneContinueButtonEvent(Ref* ref){
	Director::getInstance()->pushScene(MakeMapScene::creategmaeMapScene());
}
void HelloWorld::startSceneOptionButtonEvent(Ref* ref){
	muserinterface.RemovePlayerInterface();
	addChild(muserinterface.AddUIInforMation(mplayerOptionSceneUI));
}
void HelloWorld::startSceneExitButtonEvent(Ref* ref){
	Director::getInstance()->sharedDirector()->end();
}

void HelloWorld::continueSceneBackButtonEvent(Ref* ref){
	muserinterface.RemovePlayerInterface();	
	addChild(muserinterface.AddUIInforMation(mplayerStartSceneUI));
}

void HelloWorld::optionSceneBacgroundSoundSliderEvent(Ref* ref, Slider::EventType et){
	
}
void HelloWorld::optionSceneActionSoundSliderEvent(Ref* ref, Slider::EventType et){}
void HelloWorld::optionSceneScreenSizeSliderEvent(Ref* ref, Slider::EventType et){}
void HelloWorld::optionSceneBackButtonEvent(Ref* ref){}

bool InputContinueSceneFileData(char* fileName, std::vector<char*>* outputplayerdata){
	std::ifstream readPlayerData(fileName, std::ios::in);
	std::string PlayerTextData;

	if (readPlayerData.is_open()){
		while (std::getline(readPlayerData, PlayerTextData)){
			outputplayerdata->push_back((char*)PlayerTextData.c_str());
		}
		readPlayerData.close();
		return true;
	}
	return false;
}