#include "MY_UserInterface.h"
#include <Windows.h>
#include <CommCtrl.h>

/*
define으로 여러가지의 size를 만들어서 비율을 만들자 
함수를 만들어서 모든 ui와 캐릭터의 사이즈를 변경하는 것을 만들자
그리고 size를 변경할때 함수안에 define된 함수를 넣자 
*/

float PlayScreenSize[2] = { 1400.0f, 700.0f };

Slider* SliderCreate(int NowPercent, int MaxPercent){
	Slider* slider = Slider::create("CheckBoxOff.png","CheckBoxOn.png");
	slider->setMaxPercent(MaxPercent);
	slider->setPercent(NowPercent);
	slider->setSizePercent(Vec2(10.0f, 10.0f));
	return slider;
}
CheckBox* CheckBoxCreate(bool nowstate){
	CheckBox* checkbox = CheckBox::create("CheckBoxOff.png", "CheckBoxOn.png");
	checkbox->setSelectedState(nowstate);
	return checkbox;
}

Layer* IFrameUserInterface::AddUIInforMation(UIVariable outerUIValue){

	PlayerUILayer.push_back(Layer::create());

	outerUIValue.UISize.width *= CONST_X_PERCENT;
	outerUIValue.UISize.height *= CONST_Y_PERCENT;

	outerUIValue.UIBenchmark.x *= CONST_X_PERCENT;
	outerUIValue.UIBenchmark.y *= CONST_Y_PERCENT;

	outerUIValue.UIMargin.width *= CONST_X_PERCENT;
	outerUIValue.UIMargin.height *= CONST_Y_PERCENT;

	int UINumber = 0;
	int CheckState = 0;
	int SliderNumber = 0;

	Size UIaddPosition = Size(0.0f, 0.0f);

	if (outerUIValue.UIMargin.width > 0){
		UIaddPosition.width = outerUIValue.UISize.width + outerUIValue.UIMargin.width;
		UIaddPosition.width *= outerUIValue.TestUIEncho;
	}
	else{
		UIaddPosition.height = outerUIValue.UISize.height + outerUIValue.UIMargin.height;
		UIaddPosition.height *= outerUIValue.TestUIEncho;
	}
	for (int i = 0; i < outerUIValue.UIType.size(); i++){
		
		std::stringstream fontName;
		fontName << outerUIValue.UIText.at(i);

		switch (outerUIValue.UIType.at(i))
		{
		case eUIKind::BUTTON:
			PlayerInterface.push_back(Button::create("CheckBoxOff.png"));
			break;
		case eUIKind::CHECKBOX:
			PlayerInterface.push_back(CheckBoxCreate(outerUIValue.CheckState.at(CheckState)));
			CheckState++;
			break;
		case eUIKind::SLIDER:
			PlayerInterface.push_back(SliderCreate(outerUIValue.SliderNow.at(SliderNumber), outerUIValue.SliderMax.at(SliderNumber)));
			break;
		default:
			assert(0);
			break;
		}

		PlayerInterface.back()->setAnchorPoint(Vec2(0.5f, 0.0f));
		PlayerInterface.back()->ignoreContentAdaptWithSize(false);
		PlayerInterface.back()->setContentSize(outerUIValue.UISize);

		PlayerInterface.back()->setPosition(outerUIValue.UIBenchmark + UIaddPosition * (i + 1));

		PlayerInterface.back()->_ID = i;

		PlayerInterface.back()->addClickEventListener(outerUIValue.MyUserInterfaceEvent.at(i));

		if (outerUIValue.UIType.at(i) == eUIKind::SLIDER){
			char TemporaryValue[5];
			sprintf(TemporaryValue, "%d", outerUIValue.SliderNow.at(SliderNumber));
			fontName.str(TemporaryValue);
			SliderNumber++;
		}

		/*Label
		fontsize를 조정해야한다 자동으로 만들고 싶다.
		핵심은 fontsize다 */

		PlayerUITextLabel.push_back(Label::create(fontName.str().c_str(), "Big_Angled.ttf",20.0f));
		PlayerUITextLabel.back()->setAnchorPoint(Vec2(0.5f, 0.5f));
		PlayerUITextLabel.back()->setTextColor(Color4B::YELLOW);
		PlayerUITextLabel.back()->setPosition(Vec2(PlayerInterface.back()->getPosition().x,
			PlayerInterface.back()->getPosition().y + outerUIValue.UISize.height / 2));

		PlayerUILayer.back()->addChild(PlayerInterface.back(), 0, outerUIValue.UIText.at(i));
		PlayerUILayer.back()->addChild(PlayerUITextLabel.back(),1);
	}
	//MYKingDom.exe has triggered a breakpoint.
	return PlayerUILayer.back();
}

void IFrameUserInterface::RemovePlayerInterface(int LayerNum){
	if (PlayerUILayer.size() > LayerNum && LayerNum >= 0){

		int LayerContainedSize = 0;

		for (auto i = PlayerUILayer.begin() + LayerNum; i < PlayerUILayer.end(); i++){
			LayerContainedSize += (*i)->getChildren().size() / 2;
		}

		PlayerInterface.erase(PlayerInterface.begin() + LayerContainedSize, PlayerInterface.end());
		PlayerUITextLabel.erase(PlayerUITextLabel.begin() + LayerContainedSize, PlayerUITextLabel.end());

		while (LayerNum < PlayerUILayer.size())
		{
			PlayerUILayer.back()->removeFromParentAndCleanup(true);
			PlayerUILayer.pop_back();
		}
	}
}
/*
slider만 검사하고 순서대로 나열된 곳에 값을주자
전체에서 값을주자
*/
void IFrameUserInterface::setPlayerUITesxtLabel(int input_Slider_Number,std::string input_WidName){
	if (PlayerUILayer.size() > 0){
		for (int i = 0; i < PlayerInterface.size(); i++){
			if (!PlayerInterface.at(i)->getName().compare(input_WidName.c_str())){
				char TemporaryItoa[5];
				sprintf(TemporaryItoa, "%d", input_Slider_Number);
				PlayerUITextLabel.at(i)->setString(TemporaryItoa);
				break;
			}
		}
	}
}
