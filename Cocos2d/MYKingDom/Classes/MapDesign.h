#pragma once

#include "MY_UserInterface.h"

#define BASIC_BIT  0x1
#define DAMAGE_BIT  0x2
#define MOVE_BIT 0x4

#define ONE_LAYER 0
#define TWO_LAYER 1
#define THREE_LAYER 2
#define FOUR_LAYER 3

struct Basic_Block{

	int Block_Attribute;

};
struct Damage_Block : public Basic_Block{

	int Block_Damage = 0;

};
struct Moving_Block : public Basic_Block{

	std::vector<float> Moving_Location;
	std::vector<Vec2> Moving_Echo;
	float Moving_Speed = 1;

};

class GameStageBlock{

public:
	Vec2 Player_StartPosition;

	/*
	ture일경우 소멸후 생성*/
	std::vector<bool> Block_Visible;
	std::vector<Basic_Block*> Block_Kinds;

	std::vector<Size> Block_Size;
	std::vector<Vec2> Block_Position;

	Sprite* maddBlock(int into_BlockKind);
	std::vector<Sprite*> mreturnBlock_Setting();

	void mgameStage_SaveBlock(const char* saveFileName);
	void mgameStage_LoadBlock(char* loadFileName);

	/*
	할당된 Vector값을 정리해준다.*/
	void mremoveAllGameStageBlock();
	void mremoveOneGameStageBlock(int into_BlockNum);

};
