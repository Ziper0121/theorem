#pragma once

#include "MapDesign.h"
#include "MY_UserInterface.h"
#define CONST_PERCENTAGE 0.001f

#define CONST_X_PERCENT PlayScreenSize[0] * CONST_PERCENTAGE
#define CONST_Y_PERCENT  PlayScreenSize[1] * CONST_PERCENTAGE

#define PLAYER_WORKINGSPEED PlayScreenSize[0] * CONST_PERCENTAGE
#define PLAYER_JUMPPOWER  PlayScreenSize[1] * CONST_PERCENTAGE
#define PLAYER_ROPEPOWER  PlayScreenSize[1] * CONST_PERCENTAGE

#define WALK_ACTION 0x1
#define RUN_ACTION 0x2
#define JUMP_ACTION 0x4
#define FALL_ACTION 0x8
#define ROPE_ACTION 0x16
#define BIT_THIRTY_TWO 0x32
#define BIT_SIXTY_FOUR 0x64

#define UP_BUMPING 0x1
#define DOWN_BUMPING 0x2
#define LEFT_BUMPING 0x4
#define RIGHT_BUMPING 0x8
/*
sprite만 * percent가 적용되는 것이다
int나 백터는 설정이 되지 않는다*/
struct CharactorSpeedData{

	/*
	반향들*/

	// charector가 움직이는 방향
	int Charactor_Walk_Encho = 0;

	/* 미 사용
	player가 달릴 반향 */
	int Charactor_DoubleClickArrow = 0;

	float CharactorWorking_ProgressPower = 0.0f;

	/*
	Charactor Working Speed*/
	float Charactor_WorkingSecondSpeed = 0.2f;
	float Charactor_WorkingMaxSpeed = 3.0f;
	float Charactor_StopSpeed = 0.2f;
};

struct CharactorPowerData{


	/* 
	추가 액션의 힘*/
	float Charactor_SuperJumpRunningPower = 0;

	/*
	미사용*/
	float Charactor_Working_ProgressGradually = 0.0f;
	float Charactor_RunningPower = 0.0f;

	/*
	Jump power*/
	float Charator_JumpPower =  0.0f;

	/*
	특수 상태*/
	bool Charactor_RunningJump = false;
	bool UpBumpingOffsetJumpPower = false;
};


struct CharactorMoveData{

	/*
	가로막힐경우 세울 위치
	그리고 모든 player의 움직을을 관장하는 vector*/
	Vec2 Charactor_AllMoverPower;

	/*
	캐릭터의 한번 움직임의 값을 받는 power */
	Vec2 Charactor_MovePower;
	
	/*
	player가 부딪힌 위치*/
	int Charactor_BumpingEcho = 0;

	/*
	움직이는 속도와 파워 변수들*/
	CharactorSpeedData Charactor_WorkingSpeed;
	CharactorPowerData Charactor_JumpPower;
};

struct CharactorData{

	void mcharactorData_BascisSetting();

	int Charactor_LifeParts;

	Sprite* Charactor_Avatar;

	CharactorMoveData Charactor_MoveData;
	
	/*
	플레이어가 무적 상태*/
	bool Charactor_InvincibilityState = false;

	// 무적 시간
	float Charactor_InvincibilityTime = 0;
	int Charactor_InvincibilityAddTime = 0;
	int Charactor_InvincibilityMaxTime = 0;

	/*
	지금 진행중인 액션들을 값*/
	int Charactor_ProceedingActionState = 0;
};


class Game_Algorithm{
public:
	Game_Algorithm(){
		mgameAlgorithm_Layer = Layer::create();
	}

	/*
	player의 움직임과 충돌처리를 담당하는 스케줄*/
	void mplayerMovementSchedule(float dt);
	/*
	MoveBlock를 관리하는 스케줄*/
	void mmapMoveBlockWorkSchedule(float dt);

	virtual void onMouseUp(Event* mevent);
	virtual void onMouseDown(Event* mevent);

	virtual	void onKeyPressed(EventKeyboard::KeyCode kcode, Event* ref);
	virtual void onKeyReleased(EventKeyboard::KeyCode kcode, Event* ref);

	/*
	player 재생성*/
	void mplayerReset();
	/*
	player의 처음 위치에서 검사하는 함수*/
	void mplayerQuadrantPointSetting();

	/*
	블럭 추가*/
	void mstageAddBlock(int into_kind);

	/*
	블럭 삭제*/
	void mremoveStage_AllBlock();
	void mremoveStage_OneBlock(int into_BlockNum );

	CharactorData mplayerCharacter;

	GameStageBlock mmapNowBlockInformation;

	std::vector<Sprite*> mallMapBlock;

	std::vector<Sprite*> mplayerNearBlock;

	/*
	중력의 관한 정보*/
	float Gravity_SecondPower = -0.4f;
	float Gravity_ProgressPower = 0.0f;
	float Gravity_MaxPower = 4.0f;

	/*
	게임알고리즘의 밑에 레이어*/
	Layer* mgameAlgorithm_Layer;

	/*
	로프의 정보들*/
	Sprite* mplayerCharacterRope;

	Vec2 mplayerRopePosition;
	Vec2 mplayerRopePower;

	bool mplayerRopeLaunch = false;


	/*플레이어가 지금 실행시킨 액션을 전달*/
	int mplayerInActionState = 0;

	/*
	키보드 값
	좌우 점프 로프 오르락내리락*/
	EventKeyboard::KeyCode mplayerArrowKeyCode[5];

	// player가 누른 키보드 값
	int mplayer_WalkArrow = 0;

	int mmapNumber = 0;
	int mstageNumber = 0;

	/*
	map의 대한 번호들을 저장하는 변수들
	따로 검사하여 속도를 조금이나마 올리기 위한 존재
	*/
	std::vector<int> mmapMoveBlockNum;
	std::vector<int> mmapcutterBlockNum;

	/*
	무브 블럭의 레일번호  길이  반향*/
	std::vector<int> mmapMoveBlockLocationNum;
	std::vector<float> mmapMoveBlockMovesDistans;
	std::vector<int> mmapMoveBlockLocationEcho;

	/*
	무시할 블럭 투명상태*/
	std::vector<int> mblock_CheckIgnore;
	std::vector<int> mblock_GetIgnore;

	int mforMoveBlockNum = 0;

	std::vector<int> mblock_RegenerationTime;
	std::vector<int> mblock_ExtinctionTime;

	int mblock_RegenerationMaxTime = 100;
	int mblock_ExtinctionMaxTime = 150;
};
