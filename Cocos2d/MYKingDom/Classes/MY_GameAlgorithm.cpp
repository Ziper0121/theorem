#include "MY_GameAlgorithm.h"
/*
필요한 것

할수 있는것

데이터
플레이어 정보
목숨 (데스의 숫자)
클리어 위치

글씨크기 변경

알고리즘
맵      :  만드는중 111111111111111111111

블럭 검사를 좀더 빠르게 만들자

player주변 블럭 탐색

맵을 만드는 알고리즘 : 만드는중 2222222222222222222222

파일
continue를 만들자

**************
기술이 필요한것

그랩

*/
/*
near은 다 없애야한다.*/

#define BLOCKKIND_MOVINGBLOCK ((Moving_Block*)mmapNowBlockInformation.Block_Kinds.at(mmapMoveBlockNum.at(mforMoveBlockNum)))
#define MOVEBLOCK_MOVING_SPEED ((Moving_Block*)mmapNowBlockInformation.Block_Kinds.at(mmapMoveBlockNum.at(mforMoveBlockNum)))->Moving_Speed

#define MOVEBLOCK_AT(BLOCK_NUM) ((Moving_Block*)mmapNowBlockInformation.Block_Kinds.at(BLOCK_NUM))
#define CUTTERBLOCK_AT(BLOCK_NUM) ((Damage_Block*)mmapNowBlockInformation.Block_Kinds.at(BLOCK_NUM))

#define CHAR_MOVE mplayerCharacter.Charactor_MoveData
#define CHAR_JUMP CHAR_MOVE.Charactor_JumpPower
#define CHAR_WORKING CHAR_MOVE.Charactor_WorkingSpeed


void CharactorData::mcharactorData_BascisSetting(){
	Charactor_LifeParts = 10.0f;

	Charactor_MoveData.Charactor_AllMoverPower = Vec2(0.0f, 0.0f);
	Charactor_MoveData.Charactor_MovePower = Vec2(0.0f, 0.0f);
	Charactor_MoveData.Charactor_BumpingEcho = 0.0f;

	Charactor_MoveData.Charactor_JumpPower.Charator_JumpPower = 8.0f;
	Charactor_MoveData.Charactor_JumpPower.Charactor_RunningJump = false;
	Charactor_MoveData.Charactor_JumpPower.UpBumpingOffsetJumpPower = false;

	Charactor_MoveData.Charactor_WorkingSpeed.Charactor_WorkingSecondSpeed = 0.2f;
	Charactor_MoveData.Charactor_WorkingSpeed.Charactor_WorkingMaxSpeed = 3.0f;
	Charactor_MoveData.Charactor_WorkingSpeed.Charactor_Walk_Encho = 0.2f;

	Charactor_InvincibilityState = false;
	Charactor_InvincibilityTime = 0.0f;
	Charactor_InvincibilityAddTime = 0.2f;
	Charactor_InvincibilityMaxTime = 25.0f;

	Charactor_ProceedingActionState = 0.0f;
}

void Game_Algorithm::mplayerMovementSchedule(float dt){
	
	CHAR_MOVE.Charactor_MovePower.x = 0.0f;
	CHAR_MOVE.Charactor_MovePower.y = 0.0f;

	mplayerCharacter.Charactor_ProceedingActionState |= mplayerInActionState;

#pragma region REGION_ROPE_ACTION



#pragma endregion REGION_ROPE_ACTION

#pragma region REGION_WAKING_ACTION

	/*
	걸으면서 점점 빨라지는 것과
	달리는 것을 고려중이다.*/

	if (mplayerCharacter.Charactor_ProceedingActionState & WALK_ACTION){
		CHAR_WORKING.CharactorWorking_ProgressPower += CHAR_WORKING.Charactor_WorkingSecondSpeed;

		CHAR_WORKING.Charactor_Walk_Encho = mplayer_WalkArrow;

		if (CHAR_WORKING.CharactorWorking_ProgressPower > CHAR_WORKING.Charactor_WorkingMaxSpeed){
			CHAR_WORKING.CharactorWorking_ProgressPower = CHAR_WORKING.Charactor_WorkingMaxSpeed;
		}
	}
	else{
		CHAR_WORKING.CharactorWorking_ProgressPower -= CHAR_WORKING.Charactor_StopSpeed;

		if (CHAR_WORKING.CharactorWorking_ProgressPower < 0){
			CHAR_WORKING.CharactorWorking_ProgressPower = 0;
		}
	}

	CHAR_MOVE.Charactor_MovePower.x += CHAR_WORKING.Charactor_Walk_Encho * CHAR_WORKING.CharactorWorking_ProgressPower * PLAYER_WORKINGSPEED;

#pragma endregion REGION_WAKING_ACTION

#pragma region REGION_JUMP_ACTION

	if (mplayerCharacter.Charactor_ProceedingActionState & FALL_ACTION){
		Gravity_ProgressPower += Gravity_SecondPower;
		if (mplayerCharacter.Charactor_ProceedingActionState & JUMP_ACTION){
			CHAR_MOVE.Charactor_MovePower.y += CHAR_JUMP.Charator_JumpPower;
		}
		if (CHAR_MOVE.Charactor_MovePower.y - Gravity_ProgressPower < Gravity_MaxPower){
			Gravity_ProgressPower = Gravity_MaxPower;
		}
		CHAR_MOVE.Charactor_MovePower.y += Gravity_ProgressPower * PLAYER_JUMPPOWER;
	}

#pragma endregion REGION_JUMP_ACTION

#pragma region REGION_CRASH_ACTION

	int TemporaryPlayerBumpingEcho = 0;
	CHAR_MOVE.Charactor_BumpingEcho = 0;

	Vec2 CheckPlayerBumpingPosition = Vec2(0.0f, 0.0f);
	Vec2 addCharMoveBlock = Vec2(0.0f, 0.0f);
	Vec2 PlayerCharMovePosition = mplayerCharacter.Charactor_Avatar->getPosition() + CHAR_MOVE.Charactor_MovePower;

	/*
	mplayerMove가 움직일수 없을 경우를 대비해서 대첵을 새우자(가로막혀있을 경우)*/
	CHAR_MOVE.Charactor_AllMoverPower = Vec2(0.0f, 0.0f);

	bool ForFunctionBumpingEcho = false;

	for (int i = 0; i < mplayerNearBlock.size(); i++){

		/*
		무시할 블럭을 소까내는 for문*/
		for (int k = 0; k < mblock_RegenerationTime.size(); k++){
			if (mblock_CheckIgnore.at(k) == i){
				i++;
				k = 0;
				continue;
			}
		}

		if (i >= mplayerNearBlock.size()){
			break;
		}

		ForFunctionBumpingEcho = false;

		Vec2 QuadranBlockPosition = mplayerNearBlock.at(i)->getPosition();

		if (mplayerCharacter.Charactor_Avatar->getPosition().x - mplayerCharacter.Charactor_Avatar->getScaleX() < QuadranBlockPosition.x
			&& mplayerCharacter.Charactor_Avatar->getPosition().x > QuadranBlockPosition.x - mplayerNearBlock.at(i)->getScaleX()){

			if (PlayerCharMovePosition.y - mplayerCharacter.Charactor_Avatar->getScaleY() <= QuadranBlockPosition.y
				&& PlayerCharMovePosition.y > QuadranBlockPosition.y){

				CHAR_JUMP.Charactor_RunningJump = false;
				ForFunctionBumpingEcho = true;
				CHAR_JUMP.Charactor_SuperJumpRunningPower = 2;
				TemporaryPlayerBumpingEcho |= DOWN_BUMPING;
				CheckPlayerBumpingPosition.y = QuadranBlockPosition.y + mplayerCharacter.Charactor_Avatar->getScaleY();

				for (int k = 0; k < mmapMoveBlockNum.size(); k++){
					if (i == mmapMoveBlockNum.at(k)){
						addCharMoveBlock += MULTIPLICATION_VEC2((BLOCKKIND_MOVINGBLOCK->Moving_Echo.at(mmapMoveBlockLocationNum.at(mforMoveBlockNum)) * MOVEBLOCK_MOVING_SPEED));
						break;
					}
				}

			}
			else if (PlayerCharMovePosition.y > QuadranBlockPosition.y - mplayerNearBlock.at(i)->getScaleY()
				&& PlayerCharMovePosition.y < QuadranBlockPosition.y){

				CHAR_JUMP.UpBumpingOffsetJumpPower = true;

				ForFunctionBumpingEcho = true;
				TemporaryPlayerBumpingEcho |= UP_BUMPING;
				CheckPlayerBumpingPosition.y = QuadranBlockPosition.y - mplayerNearBlock.at(i)->getScaleY();
			}
		}
		else if (mplayerCharacter.Charactor_Avatar->getPosition().y - mplayerCharacter.Charactor_Avatar->getScaleY() < QuadranBlockPosition.y
			&& mplayerCharacter.Charactor_Avatar->getPosition().y > QuadranBlockPosition.y - mplayerNearBlock.at(i)->getScaleY()){
			if (PlayerCharMovePosition.x - mplayerCharacter.Charactor_Avatar->getScaleX() < QuadranBlockPosition.x
				&& PlayerCharMovePosition.x > QuadranBlockPosition.x){

				ForFunctionBumpingEcho = true;
				TemporaryPlayerBumpingEcho |= RIGHT_BUMPING;
				CheckPlayerBumpingPosition.x = QuadranBlockPosition.x + mplayerCharacter.Charactor_Avatar->getScaleX();

			}
			else if (PlayerCharMovePosition.x > QuadranBlockPosition.x - mplayerNearBlock.at(i)->getScaleX()
				&& PlayerCharMovePosition.x < QuadranBlockPosition.x){

				ForFunctionBumpingEcho = true;
				TemporaryPlayerBumpingEcho |= LEFT_BUMPING;
				CheckPlayerBumpingPosition.x = QuadranBlockPosition.x - mplayerNearBlock.at(i)->getScaleX();
			}
		}

		/*
		이곳에 애니메이션을 추가하자 팡 터지는 파티클로*/
		if (ForFunctionBumpingEcho){
			for (int k = 0; k < mmapcutterBlockNum.size() && !mplayerCharacter.Charactor_InvincibilityState; k++){
				if (mmapcutterBlockNum.at(k) == i){
					mplayerCharacter.Charactor_InvincibilityState = true;
					mplayerCharacter.Charactor_LifeParts -= ((Damage_Block*)mmapNowBlockInformation.Block_Kinds.at(i))->Block_Damage;

					auto Player_DamageImpact = ParticleExplosion::create();
					Player_DamageImpact->setEmissionRate(700.0f);
					Player_DamageImpact->setSpeed(50.0f);
					Player_DamageImpact->setPosition(PlayerCharMovePosition);
					Player_DamageImpact->setScale(1.2f);
					Player_DamageImpact->setLife(1.0f);
					mgameAlgorithm_Layer->addChild(Player_DamageImpact);

					break;
				}
			}

			if (!mmapNowBlockInformation.Block_Visible.at(i)){
				for (int k = 0; k < mblock_GetIgnore.size(); k++){
					if (mblock_GetIgnore.at(k) == i){
						break;
					}
					else if (k == mblock_GetIgnore.size() - 1){
						mblock_ExtinctionTime.push_back(0);
						mblock_GetIgnore.push_back(i);
					}
				}
				if (mblock_GetIgnore.size() == 0){
					mblock_ExtinctionTime.push_back(0);
					mblock_GetIgnore.push_back(i);
				}
			}
		}
	}

	CHAR_MOVE.Charactor_BumpingEcho |= TemporaryPlayerBumpingEcho;

	CHAR_MOVE.Charactor_AllMoverPower.x = PlayerCharMovePosition.x;
	CHAR_MOVE.Charactor_AllMoverPower.y = PlayerCharMovePosition.y;

	if (CHAR_MOVE.Charactor_BumpingEcho & LEFT_BUMPING + RIGHT_BUMPING){
		CHAR_MOVE.Charactor_AllMoverPower.x = CheckPlayerBumpingPosition.x;
	}
	if (CHAR_MOVE.Charactor_BumpingEcho & UP_BUMPING){
		mplayerCharacter.Charactor_ProceedingActionState ^= JUMP_ACTION;
		CHAR_MOVE.Charactor_AllMoverPower.y = CheckPlayerBumpingPosition.y;
	}
	if (CHAR_MOVE.Charactor_BumpingEcho & DOWN_BUMPING){
		Gravity_ProgressPower = 0;
		mplayerCharacter.Charactor_ProceedingActionState ^= FALL_ACTION;
		CHAR_MOVE.Charactor_AllMoverPower.y = CheckPlayerBumpingPosition.y;

		if (mplayerCharacter.Charactor_ProceedingActionState & JUMP_ACTION){
			mplayerCharacter.Charactor_ProceedingActionState ^= JUMP_ACTION;
		}

	}
	else{
		mplayerCharacter.Charactor_ProceedingActionState |= FALL_ACTION;
	}

	CHAR_MOVE.Charactor_AllMoverPower += addCharMoveBlock;

#pragma endregion REGION_CRASH_ACTION

	mplayerCharacter.Charactor_Avatar->setPosition(CHAR_MOVE.Charactor_AllMoverPower);

	mplayerInActionState = 0;

	for (int i = 0; i < mblock_ExtinctionTime.size(); i++){
		if (mblock_ExtinctionTime.at(i) < mblock_RegenerationMaxTime){
			mblock_ExtinctionTime.at(i)++;
		}
		else{
			mplayerNearBlock.at(mblock_GetIgnore.at(i))->setVisible(false);
			mblock_CheckIgnore.push_back(mblock_GetIgnore.at(i));

			mblock_RegenerationTime.push_back(0);

			mblock_GetIgnore.erase(mblock_GetIgnore.begin() + i);
			mblock_ExtinctionTime.erase(mblock_ExtinctionTime.begin() + i);
		}
	}

	for (int i = 0; i < mblock_RegenerationTime.size(); i++){
		if (mblock_RegenerationTime.at(i) < mblock_RegenerationMaxTime){
			mblock_RegenerationTime.at(i)++;
		}
		else{
			mplayerNearBlock.at(mblock_CheckIgnore.at(i))->setVisible(true);
			mblock_RegenerationTime.erase(mblock_RegenerationTime.begin() + i);
			mblock_CheckIgnore.erase(mblock_CheckIgnore.begin() + i);
		}
	}

	if (mplayerCharacter.Charactor_InvincibilityState & !mplayerCharacter.Charactor_InvincibilityAddTime){
		mplayerCharacter.Charactor_InvincibilityAddTime = 1;

		mplayerInActionState |= JUMP_ACTION;
		mplayerInActionState |= FALL_ACTION;
		CHAR_MOVE.Charactor_BumpingEcho ^= DOWN_BUMPING;

	}
	else if (mplayerCharacter.Charactor_InvincibilityTime > mplayerCharacter.Charactor_InvincibilityMaxTime){

		mplayerCharacter.Charactor_InvincibilityTime = 0.0f;
		mplayerCharacter.Charactor_InvincibilityAddTime = 0;
		mplayerCharacter.Charactor_InvincibilityState = false;

	}
	mplayerCharacter.Charactor_InvincibilityTime += mplayerCharacter.Charactor_InvincibilityAddTime;

}

void Game_Algorithm::mmapMoveBlockWorkSchedule(float dt){

	for (int i = 0; i < mmapMoveBlockNum.size(); i++){
		mforMoveBlockNum = i;

		if (BLOCKKIND_MOVINGBLOCK->Moving_Location.at(mmapMoveBlockLocationNum.at(mforMoveBlockNum))
	> mmapMoveBlockMovesDistans.at(mforMoveBlockNum) + MOVEBLOCK_MOVING_SPEED){

			mplayerNearBlock.at(mmapMoveBlockNum.at(mforMoveBlockNum))->setPosition(mplayerNearBlock.at(mmapMoveBlockNum.at(mforMoveBlockNum))->getPosition() +
				MULTIPLICATION_VEC2((BLOCKKIND_MOVINGBLOCK->Moving_Echo.at(mmapMoveBlockLocationNum.at(mforMoveBlockNum)) * MOVEBLOCK_MOVING_SPEED)));
			mmapMoveBlockMovesDistans.at(mforMoveBlockNum) += MOVEBLOCK_MOVING_SPEED;

		}
		else{

			float OverDistance = mmapMoveBlockMovesDistans.at(mforMoveBlockNum) + MOVEBLOCK_MOVING_SPEED
				- BLOCKKIND_MOVINGBLOCK->Moving_Location.at(mmapMoveBlockLocationNum.at(mforMoveBlockNum));

			float OverSpeedDifference = MOVEBLOCK_MOVING_SPEED - OverDistance;

			mplayerNearBlock.at(mmapMoveBlockNum.at(mforMoveBlockNum))->setPosition(mplayerNearBlock.at(mmapMoveBlockNum.at(mforMoveBlockNum))->getPosition() +
				MULTIPLICATION_VEC2((BLOCKKIND_MOVINGBLOCK->Moving_Echo.at(mmapMoveBlockLocationNum.at(mforMoveBlockNum)) * OverSpeedDifference)));

			mmapMoveBlockMovesDistans.at(mforMoveBlockNum) = 0.0f;

			if (BLOCKKIND_MOVINGBLOCK->Moving_Location.size() == 1){
				BLOCKKIND_MOVINGBLOCK->Moving_Echo.at(mmapMoveBlockLocationNum.at(mforMoveBlockNum)) *= -1;
				continue;
			}
			if (BLOCKKIND_MOVINGBLOCK->Moving_Location.size() <= mmapMoveBlockLocationNum.at(mforMoveBlockNum) + mmapMoveBlockLocationEcho.at(mforMoveBlockNum)
				|| mmapMoveBlockLocationNum.at(mforMoveBlockNum) + mmapMoveBlockLocationEcho.at(mforMoveBlockNum) < 0){
				for (int k = 0; k < BLOCKKIND_MOVINGBLOCK->Moving_Echo.size(); k++){
					BLOCKKIND_MOVINGBLOCK->Moving_Echo.at(k) *= -1;
				}
				mmapMoveBlockLocationEcho.at(mforMoveBlockNum) *= -1;
			}
			else if ((int)BLOCKKIND_MOVINGBLOCK->Moving_Location.size() > mmapMoveBlockLocationNum.at(mforMoveBlockNum)){
				mmapMoveBlockLocationNum.at(mforMoveBlockNum) += mmapMoveBlockLocationEcho.at(mforMoveBlockNum);
			}
		}
	}
}

void Game_Algorithm::mplayerQuadrantPointSetting(){

	for (int i = 0; i < mplayerNearBlock.size(); i++){
		switch (mmapNowBlockInformation.Block_Kinds.at(i)->Block_Attribute)
		{
		case DAMAGE_BIT:
			mmapcutterBlockNum.push_back(i);
			break;
		case MOVE_BIT:
			mmapMoveBlockNum.push_back(i);

			mmapMoveBlockLocationNum.push_back(0);
			mmapMoveBlockLocationEcho.push_back(1);
			mmapMoveBlockMovesDistans.push_back(0.0f);

			break;
		default:
			break;
		}
	}

	mplayerCharacter.Charactor_Avatar = Sprite::create("playerchar.png");
	mplayerCharacter.Charactor_Avatar->setAnchorPoint(Vec2(1.0f, 1.0f));
	mplayerCharacter.Charactor_Avatar->setScaleX(CONST_X_PERCENT * 20.0f);
	mplayerCharacter.Charactor_Avatar->setScaleY(CONST_Y_PERCENT * 40.0f);
	mplayerCharacter.Charactor_Avatar->setPositionX(CONST_X_PERCENT * mmapNowBlockInformation.Player_StartPosition.x);
	mplayerCharacter.Charactor_Avatar->setPositionY(CONST_Y_PERCENT * mmapNowBlockInformation.Player_StartPosition.y);
	mgameAlgorithm_Layer->addChild(mplayerCharacter.Charactor_Avatar);

	mplayerArrowKeyCode[0] = EventKeyboard::KeyCode::KEY_A;
	mplayerArrowKeyCode[1] = EventKeyboard::KeyCode::KEY_D;
	mplayerArrowKeyCode[2] = EventKeyboard::KeyCode::KEY_SPACE;

	for (int i = 0; i < mplayerNearBlock.size(); i++){

		float QuadranBlockHeight = mplayerNearBlock.at(i)->getPosition().y;
		float QuadranBlockWidth = mplayerNearBlock.at(i)->getPosition().x;

		if (mplayerCharacter.Charactor_Avatar->getPositionY() - mplayerCharacter.Charactor_Avatar->getScaleY() == QuadranBlockHeight){
			if (mplayerCharacter.Charactor_Avatar->getPosition().x - mplayerCharacter.Charactor_Avatar->getScaleX() < QuadranBlockWidth
				&& mplayerCharacter.Charactor_Avatar->getPosition().x > QuadranBlockWidth - mplayerNearBlock.at(i)->getScaleX()){
				CHAR_MOVE.Charactor_BumpingEcho |= DOWN_BUMPING;
				return;
			}
		}

	}
	mplayerCharacter.Charactor_ProceedingActionState |= FALL_ACTION;
}
void Game_Algorithm::onMouseUp(Event* mevent){
	if (mplayerCharacter.Charactor_ProceedingActionState & ROPE_ACTION){
		mplayerCharacter.Charactor_ProceedingActionState ^= ROPE_ACTION;
		mplayerRopeLaunch = false;
	}
}
void Game_Algorithm::onMouseDown(Event* mevent){
	/*
	로프문제
	로프를 내가 쏠경우 움직이면 줄의 기리가 한정적이니 당겨져야 하지 않나?
	두 점 사이의 거리를 재서 당긴다. 이쪽이 마은에 든다.
	두번째 로프가 같이 움직인다.

	CCLOG("DOWN MOUSE");
	EventMouse* MouseEvent = (EventMouse*)mevent;
	if (!mplayerRopeLaunch){
	mplayerRopePosition = MouseEvent->getLocation();
	mplayerCharacter.Charactor_AvatarRope->setPosition(mplayerCharacter.Charactor_Avatar->getPosition());
	mplayerRopePower = Vec2(mplayerRopePosition - mplayerCharacter.Charactor_Avatar->getPosition());

	float TriangleRadian = atan2f(mplayerRopePower.y, mplayerRopePower.x) * 180 / 3.141592;

	mplayerRopePower = Vec2(cos(TriangleRadian) * PLAYER_ROPEPOWER, sin(TriangleRadian)* PLAYER_ROPEPOWER);

	mplayerRopeLaunch = true;
	}
	*/
}

void Game_Algorithm::onKeyPressed(EventKeyboard::KeyCode kcode, Event* ref){
	for (int i = 0; i < 3; i++){
		if (kcode == mplayerArrowKeyCode[i]){
			switch (i)
			{
			case 0:
				CCLOG("========TOUCH LEFT BUTTON");
				mplayer_WalkArrow = -1;
				mplayerInActionState |= WALK_ACTION;
				continue;
			case 1:
				CCLOG("TOUCH RIGHT BUTTON========");
				mplayer_WalkArrow = 1;
				mplayerInActionState |= WALK_ACTION;
				continue;
			case 2:
				CCLOG("========TOUCH JUMP BUTTON========");
				if (CHAR_MOVE.Charactor_BumpingEcho & DOWN_BUMPING){
					mplayerInActionState |= JUMP_ACTION;
					mplayerInActionState |= FALL_ACTION;
					CHAR_MOVE.Charactor_BumpingEcho ^= DOWN_BUMPING;
				}
				continue;
			default:
				break;
			}
		}
	}
}
void Game_Algorithm::onKeyReleased(EventKeyboard::KeyCode kcode, Event* ref){
	for (int i = 0; i < 2; i++){
		if (kcode == mplayerArrowKeyCode[i]){
			switch (i)
			{
			case 0:
				CCLOG("========OFF RIGHT BUTTON");
				if (mplayer_WalkArrow < 0){
					mplayer_WalkArrow = 0;
					mplayerCharacter.Charactor_ProceedingActionState ^= WALK_ACTION;
				}
				break;
			case 1:
				CCLOG("========OFF RIGHT BUTTON");
				if (mplayer_WalkArrow > 0){
					mplayer_WalkArrow = 0;
					mplayerCharacter.Charactor_ProceedingActionState ^= WALK_ACTION;
				}
				break;
			default:
				break;
			}
		}
	}
}


void Game_Algorithm::mplayerReset(){
	mplayerCharacter.Charactor_Avatar = Sprite::create("playerchar.png");
	mplayerCharacter.Charactor_LifeParts = 10;

	mplayerCharacter.Charactor_InvincibilityAddTime = 2; 
	mplayerCharacter.Charactor_InvincibilityMaxTime = 2;
	mplayerCharacter.Charactor_InvincibilityState = false;
}

void Game_Algorithm::mstageAddBlock(int into_kind){

	mallMapBlock.push_back(mmapNowBlockInformation.maddBlock(into_kind));
	/*
	near함수를 완성하면 없애자
	나중에 없애자*/
	mplayerNearBlock.push_back(mallMapBlock.back());
	mgameAlgorithm_Layer->addChild(mallMapBlock.back());

}

void Game_Algorithm::mremoveStage_AllBlock(){

	mgameAlgorithm_Layer->removeAllChildren(); 

	mallMapBlock.clear();
	mplayerNearBlock.clear();

	mmapNowBlockInformation.mremoveAllGameStageBlock();
}
void Game_Algorithm::mremoveStage_OneBlock(int into_BlockNum){
	mgameAlgorithm_Layer->removeChild(mallMapBlock.at(into_BlockNum), true);
	mgameAlgorithm_Layer->removeChild(mplayerNearBlock.at(into_BlockNum), true);

	mallMapBlock.erase(mallMapBlock.begin() + into_BlockNum);
	mplayerNearBlock.erase(mplayerNearBlock.begin() + into_BlockNum);

	mmapNowBlockInformation.mremoveOneGameStageBlock(into_BlockNum);

}
