#pragma once

#include <cocos2d.h>
#include <ui/CocosGUI.h>
#include <string.h>

#define TEXT_COLOR Color4B::BLACK

#define CONST_PERCENTAGE 0.001f

#define	MULTIPLICATION_VEC2(v1) Vec2(v1.x* CONST_X_PERCENT,v1.y *CONST_Y_PERCENT)

extern float PlayScreenSize[2];

#define CONST_X_PERCENT PlayScreenSize[0] *CONST_PERCENTAGE
#define CONST_Y_PERCENT  PlayScreenSize[1] * CONST_PERCENTAGE

static float ALLUISize[2] = { 900.0f, 64.0f };

static cocos2d::Layer* CurrentPlayerLayer;

using namespace cocos2d;
using namespace cocos2d::ui;
enum class eUIKind{
	BUTTON,
	SLIDER,
	CHECKBOX
};

struct UIVariable{

	std::vector<eUIKind> UIType;
	std::vector<char*> UIText;

	float TestUIEncho = 0.0f;

	Size UISize;
	Vec2 UIBenchmark;
	Size UIMargin;

	std::vector<int> SliderMax;
	std::vector<int> SliderNow;

	std::vector<bool> CheckState;

	std::vector<std::function<void(Ref*, Slider::EventType)>> MySliderEvent;
	std::vector<std::function<void(Ref*)>> MyUserInterfaceEvent;
};


class IFrameUserInterface{
public:
	IFrameUserInterface(){
		PlayerInterface = std::vector<Widget*>(0);
		PlayerUITextLabel = std::vector<Label*>(0);
		PlayerUILayer = std::vector<Layer*>(0);
	}
	/*
	**************************Function***************************/
	/*
	margin값을 무조권 2 이상으로 줘야한다.*/
	Layer* AddUIInforMation(UIVariable);
	/*
	이미 켜져있는 인터페이스를 제거해주는 함수 
	LayerNum은 지금 켜지는 인터페이스의 위치를 적으면된다.*/
	void RemovePlayerInterface(int LayerNum = 0);
	void setPlayerUITesxtLabel(int input_Slider_Number, std::string input_WidName);

private:
	/*

	현재 만들어진 UI들*/
	std::vector<Widget*> PlayerInterface;

	std::vector<Label*> PlayerUITextLabel;

	std::vector<Layer*> PlayerUILayer;
};

Slider* SliderCreate(int NowPercent, int MaxPercent);
CheckBox* CheckBoxCreate(bool nowstate);

