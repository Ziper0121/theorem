#pragma once
#include "MY_GameAlgorithm.h"

class GamePlay_Scene : public Layer{
public:
	static cocos2d::Scene* createGamePlay_Scene();

	CREATE_FUNC(GamePlay_Scene);

	virtual bool init();
private:
	Game_Algorithm mgame_Algorithm;
};