#pragma once

#include "MY_GameAlgorithm.h"
#include <io.h>

#include <iostream>
#include <fstream>

/*
Make Map의 경우 모든 설정을 할수 있는 것으로 만들자 그리고 값도 변경할수있게
map을 만드는 것과 game을 설정하는 것을 따로 만들어 놓자
object마다 다른 설정값을 보여주자
edtbox도 만들자
*/

/*
**************중요**************
내일 block과 locationblock의 연관관계를 좀더 생각해서 만들자 문제가 있다.
*/

/*
오늘 할일 다 못함
loaction block의 struct를 만든다. O
loaction block가 속해있는 move block을 묶는다. O 
location block을 변경해서 생긴 논리 오류를 고친다 하는중
move block을 만들때 같이 location블럭도 만들어주자
move block을 움직이면 모든 location block이 움직이는 버그를 고치자 
file 을 load할때 loaction block이 생성되게 만든다. 
*/

/*
연결된 move block의 관한 정보
*/
struct Move_Loaction_Block{
public:
	Move_Loaction_Block(int Const_Bolck) : mconnect_Block_Num(Const_Bolck) {}
	Move_Loaction_Block operator = (Move_Loaction_Block& other);

	const int mconnect_Block_Num;

	std::vector<Sprite*> mlocation_Block;
};

class MakeMapScene : public Layer {
public:

	static cocos2d::Scene* creategmaeMapScene();
	virtual bool init();
	CREATE_FUNC(MakeMapScene);

	Game_Algorithm mgame_starter;

private:
	/*
	UI 정보를 담은 함수*/
	void mresetMapUI();
	/*
	file에서 블럭을 로드할때 Move Locaiton을 생성을 해주는 함수*/
	void mmove_Location_BlockSetting();

	void mremove_MakingMap_AllBlock();

	virtual void onMouseDown(Event* mevent);
	virtual void onMouseUP(Event* mevent);
	virtual void onMouseMove(Event* mevent);

	void mmove_GameAlgorithm(float dt);

	Layer* mcreating_Qustion;
	Layer* mcreating_MoveLocationBlock;
	Layer* mcreating_Include_Scheudle;

	IFrameUserInterface mmapCreateSceneUI;

	UIVariable mTargetOneLayerUI;

	UIVariable mmapCreateTwoLayerQuestion;
	UIVariable mmapLoadTwoLayerChoiceUI;
	UIVariable mmapLoadThreeLayerQuestion;
	UIVariable mmapSaveTwoLayerChoiceUI;

	UIVariable mplayerTargetTwoLayerChoiceUI;

	UIVariable mplayerPositionThreeLayerSettingUI;
	UIVariable mplayerSizeThreeLayerSettingUI;
	UIVariable mplayerSpeedThreeLayerSettingUI;
	UIVariable mplayerJumpThreeLayerSettingUI;

	UIVariable mplayerSaveThreeLayerChoiceUI;
	UIVariable mplayerLoadThreeLayerChoiceUI;
	UIVariable mplayerLoadThreeLayerQuestion;

	UIVariable mmonsterTargetTwoLayerChoiceUI;
	UIVariable mbackGroundTargetTwoLayerChoiceUI;


	UIVariable mblockTargetTwoLayerChoiceUI;

	UIVariable mblockCreateThreeLayerChoiceUI;
	UIVariable mblockSizeThreeLayerSettingUI;
	UIVariable mblockPositionThreeLayerSettingUI;
	UIVariable mblockMoveThreeLayerSettingUI;

	UIVariable mblockMoveFourLayerSettingUI;

	std::vector<Move_Loaction_Block> mmove_Location_Block;

	Vec2 mmouseMoveLocation;

	Vec2 mmouseMovePercentLocaition = Vec2(0.0f,0.0f);

	Vec2 mmoveLocationBenchMark;


	char** mplayerData_FILEName;
	char** mmapData_FILEName;

	int mmapData_FILENum;
	int mplayerData_FILENum;

	int mmapDataFile_Size;
	int mplayerDataPorther_Size;

	int mchoise_Block = 0;

	int mchoise_Move_Connect_Block = 0;
	int mchoise_Move_Location = 0;

	int mmouse_state = 0;
	
	bool mremove_Block_State = false;
	bool mremove_MoveLocationBlock_State = false;

	bool mplayer_StartPosition_State = false;

	bool mmap_Exist = false;
	bool mplayer_Exist = false;
};