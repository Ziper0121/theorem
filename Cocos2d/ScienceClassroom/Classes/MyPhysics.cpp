#include "MyPhysics.h"

/*모든 myphsycis의 값만큼 
*/
void MyPhysics::MyPhysicscreateBlock(Objectvalue parameterobject){

	PhysicsBlock fuctioninPhsycisBlock;
	fuctioninPhsycisBlock.Appearance = Sprite::create("Block.png");
	fuctioninPhsycisBlock.Appearance->setPosition(parameterobject.Objectposition);
	fuctioninPhsycisBlock.Appearance->setScale(UNIVERSESIZE*(float)parameterobject.ObjectVolume);
	parameterobject.Objectlaeyer->addChild(fuctioninPhsycisBlock.Appearance);

	fuctioninPhsycisBlock.Location = parameterobject.Objectposition;
	fuctioninPhsycisBlock.Mass = parameterobject.ObjectMass;
	fuctioninPhsycisBlock.Volume = parameterobject.ObjectVolume;
	
	gravitationaldt.push_back(0.0f);
	gravitationaltime.push_back(2.0f);
	MyBlock.push_back(fuctioninPhsycisBlock);
}
void MyPhysics::MyPhysicsAddBlock(Sprite* Block){

}
/*중력은 시간(dt)으로 조정하자
setPosition은 일정한 속도로 움직이지만 움직이는 시간의 간격으로 설정한다. 
*/
/*
문제
object를 두개를 만들고 나면 하나는 사라진다.
추론
아마 너무 빠르기 때문에 사라지는 것 같다.
근거
vec2값이 확 증가한다.
해결 
원인
A = f/m인데 (가속도는 힘 나누기 질량이다)
내가 A = m/f로 만들었다.
해결방안
A = m/f를 A = f/m로 변경했다.
*/

float Allmanpower(PhysicsBlock matter1, PhysicsBlock matter2){
	return G * ((matter1.Mass * matter2.Mass) / BetweenTwoPoint(matter1.Appearance->getPosition().x, matter1.Appearance->getPosition().y, matter2.Appearance->getPosition().x, matter2.Appearance->getPosition().y));
}

/* inclinaftion은 block이 향할 반향을 잡아주는 함수
*/
Vec2 inclinaftion(Vec2 One, Vec2 Two, float acceleration){
	/* Triangularratio은 대상과 나의 거리와 나의 가속도를 나누어 퍼센트를 잰후에 그것을 기반으로 움직이게 하는 것이다. 
	*/
	float Triangularratio = acceleration / BetweenTwoPoint(One.x, One.y, Two.x, Two.y);
	float X = (One.x - Two.x)*Triangularratio;
	float Y = (One.y - Two.y)*Triangularratio;
	return Vec2(X, Y);
}