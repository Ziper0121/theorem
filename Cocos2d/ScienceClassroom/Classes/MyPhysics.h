#ifndef __MYPHYSICS_H__
#define __MYPHYSICS_H__

#include <cocos2d.h>
#define UNIVERSESIZE 0.1f
#define  EARTHACCELERATION (UNIVERSESIZE * 10)
#define NEWTON(kg,m,s) ((kg)*(m)*((s)*(s)))
#define GEAVITATIONALCONSTANT(kg,m,s) (6.67384*NEWTON(kg,m,s)*(m*m))

#define G 0.01f
#define BetweenTwoPoint(X1,Y1,X2,Y2) sqrt(((X1 - X2)*(X1 - X2) + (Y1 - Y2)*(Y1 - Y2)))

using namespace cocos2d;

typedef struct Object
{
	float ObjectMass;
	int ObjectVolume;
	Vec2 Objectposition;
	Layer* Objectlaeyer;
}Objectvalue;

typedef struct {
	Sprite* Appearance;
	unsigned int Mass;
	int Volume;
	Vec2 Location;
}PhysicsBlock;
/*
physicsBody는 자신의 힘을 가지고 있고 그 힘의 대해 상대방을 곱하면 되지 않을까?
자신의 mass의 값을 가지고 1의 질량을 가진 물체와 힘을 적어 나중에 곱하면 되지 않을까?
*/
/*MyPhysics는 모든 물체들의 정보를 다 가지고 있어야한다.
schedule을 한번에 돌릴려면 필요하다
물체들은 서로 끌어 당기지만 1이 안되는 힘을 가진 물체들은 가만히 커다란 물체와의 상호작용만 시키자.
*/
class MyPhysics{
public:
	MyPhysics(){
		MyBlock = std::vector<PhysicsBlock>(0);
		gravitationaldt = std::vector<float>(0);
		gravitationaltime = std::vector<float>(0);
	}
	/*중력의 영향을 받는 물체
	*/
	std::vector<PhysicsBlock> MyBlock;
	/*중력의 세기와 시간
	*/
	std::vector<float> gravitationaldt;
	std::vector<float> gravitationaltime;
	/*
	*/
	std::vector<double> Repulsion;

	void MyPhysicsAddBlock(Sprite*);
	void MyPhysicscreateBlock(Objectvalue);
};
Vec2 inclinaftion(Vec2 One, Vec2 Two, float acceleration);
float Allmanpower(PhysicsBlock matter1, PhysicsBlock matter2);
#endif