#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#define ZEROLAYOUT 0
#define ONELAYOUT 1
#define TWOLAYOUT 2
#define THREELAYOUT 3

/*내일 이거 꼭봐라
화면에 마우스 클릭을 할 경우 물체를 생성하는 식으로 하자
button을 누를 경우 다른 버튼은 활동을 못하게 하자
*/

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
	scene->addChild(layer);
	return scene; 
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
	}

	ScreanLayoutcreate();

	EventListenerTouchOneByOne* LoboratoryEvent = EventListenerTouchOneByOne::create();
	LoboratoryEvent->onTouchBegan = [](Touch* tc, Event*et){return true;};
	LoboratoryEvent->onTouchEnded = std::bind(&HelloWorld::LoboratoryLayerEvnet, this, std::placeholders::_1, std::placeholders::_2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(LoboratoryEvent, this);
	this->schedule(schedule_selector(HelloWorld::objectgravity));

    return true;
}
void sschdul(float dt){
	CCLOG("Hello");
}

void HelloWorld::onEnter(){
	Layer::onEnter();
}

void HelloWorld::ScreanLayoutcreate(){

	CoordinateplateON = false;
	PlayerbuttonClick = ButtonState::None;
	Directorsize = Director::getInstance()->getWinSize();
	Currentlysetobject.Objectlaeyer = this;
	Size Buttonsize = Sprite::create("btn_up60x60.png")->getBoundingBox().size;

	BasicEventButtoncreate();

	RightLayoutCollectioncreate();
	ScreanLayout = LayerColor::create(Color4B(100.0f,100.0f,0.0f,200.0f));
	ScreanLayout->setContentSize(Directorsize);
	ScreanLayout->setPosition(Vec2(0, 0));
	addChild(ScreanLayout);

	ScreanLayout->addChild(BasicButtonLayout, ONELAYOUT);

}

void HelloWorld::RightLayoutCollectioncreate(){
}
void HelloWorld::BasicEventButtoncreate(){
	std::vector<std::string> MyPhysicsbuttontext = { "basic", "Set\n Block", "Coordinate\n plate" };
	std::vector<std::function<void(Ref*)>> ButtonEvent = {
		std::bind(&HelloWorld::BasicButtonEvent, this, std::placeholders::_1),
		std::bind(&HelloWorld::SetBlockButtonEvent, this, std::placeholders::_1),
		std::bind(&HelloWorld::CoordinateplateButtonEvent, this, std::placeholders::_1)
	};
	BasicEventButton = createbutton(MyPhysicsbuttontext, LinearLayoutParameter::LinearGravity::BOTTOM, ButtonEvent);
	BasicButtonLayout = createlayout(Layout::Type::VERTICAL, BasicEventButton, Vec2(Directorsize.width, 0), 1);
}
void HelloWorld::ClickBlockEventButtoncreate(){
	std::vector<std::string> ClickBlockbuttontext = {"Solid","Sand","Water"};
	std::vector<std::function<void(Ref*)>> ButtonEvent = std::vector<std::function<void(Ref*)>>(0);
	ClickBlockEventButton = createbutton(ClickBlockbuttontext, LinearLayoutParameter::LinearGravity::BOTTOM, ButtonEvent);
	ClickBlockLayout = createlayout(Layout::Type::VERTICAL, ClickBlockEventButton, Vec2(Directorsize.width, 0), 2);
}
void HelloWorld::Drivingworld(float dt){
}
void HelloWorld::LoboratoryLayerEvnet(Touch* tch, Event* en){
	switch (PlayerbuttonClick){
	case ButtonState::None:
		break;
	case ButtonState::basicclick:
		Currentlysetobject.Objectposition = tch->getLocation();
		Object.MyPhysicscreateBlock(Currentlysetobject);
		PlayerbuttonClick = ButtonState::None;
		ClickBlockLayout->removeAllChildren();
		removeChild(ClickBlockLayout);
		break;
	case ButtonState::CreateBlock:
		break;
	case ButtonState::SetBlockclick:
		break;
	default:
		break;
	}
	PlayerbuttonClick = ButtonState::None;
}

void HelloWorld::BasicButtonEvent(Ref* ref){
	if (PlayerbuttonClick == ButtonState::None){
		Currentlysetobject.ObjectMass = 10000.0f;
		Currentlysetobject.ObjectVolume = 10.0f;
		ClickBlockEventButtoncreate();
		PlayerbuttonClick = ButtonState::basicclick;
		addChild(ClickBlockLayout);
	}
}
void HelloWorld::SetBlockButtonEvent(Ref* ref){
	PlayerbuttonClick = ButtonState::SetBlockclick;
}
void HelloWorld::CoordinateplateButtonEvent(Ref* ref){
	CoordinateplateON = !CoordinateplateON;
	if (CoordinateplateON){
		CoordinateplateDraw = DrawNode::create();
		for (int i = 1; i < Directorsize.height / 10; i++){
			CoordinateplateDraw->drawLine(Vec2(0, i * 10), Vec2(Directorsize.width, i * 10), Color4F::WHITE);
		}
		for (int i = 1; i < Directorsize.width / 10; i++){
			CoordinateplateDraw->drawLine(Vec2(i * 10, 0), Vec2(i * 10, Directorsize.height), Color4F::WHITE);
		}
		this->addChild(CoordinateplateDraw, ONELAYOUT);
	}
	else{
		this->removeChild(CoordinateplateDraw);
	}
}

/*
나의 질량이 상대 중력에 끌릴만한 정도의 크기인가를 재는 함수
*/
void HelloWorld::objectgravity(float dt){
	int k = 0;
	for (int i = 0; i < Object.MyBlock.size(); k++){
		if (i != k){
			Object.gravitationaldt.at(i) += dt;
			if (Object.gravitationaltime.at(i) <= Object.gravitationaldt.at(i)){
				float acceleration = Allmanpower(Object.MyBlock.at(k), Object.MyBlock.at(i)) / Object.MyBlock.at(i).Mass;
				if (acceleration >= 1){
					if (acceleration / 10.0f >= 1.0f && Object.gravitationaltime.at(i) - acceleration > 0){
						acceleration /= 10;
						Object.gravitationaltime.at(i) -= acceleration;
					}
					else if (acceleration / 10.0f >= 1.0f){
						acceleration /= 10;
					}
					Object.MyBlock.at(i).Location += inclinaftion(Object.MyBlock.at(k).Appearance->getPosition(), Object.MyBlock.at(i).Appearance->getPosition(), acceleration);
				}
				Object.gravitationaldt.at(i) = 0;
			}
		}
		Object.MyBlock.at(i).Appearance->setPosition(Object.MyBlock.at(i).Location);
		if (k == Object.MyBlock.size() - 1){
			i++;
			k = 0;
		}
	}
}
Layout* createlayout(Layout::Type parameterlayouttype, std::vector<Button*> parameterButton, Vec2 parameterLayoutposition, int parameterLayoutnumber){

	Size Buttonsize = Sprite::create("btn_up60x60.png")->getContentSize();

	Margin ButtonMargin = { 0, 0, 0, 0 };

	Layout* returnLayout = ui::Layout::create();
	returnLayout->setLayoutType(parameterlayouttype);
	returnLayout->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);

	if (parameterButton.at(0)->getLayoutParameter() != 0){
		ButtonMargin = parameterButton.at(0)->getLayoutParameter()->getMargin();
	}
	Buttonsize.width += ButtonMargin.left + ButtonMargin.right;
	Buttonsize.height += ButtonMargin.top + ButtonMargin.bottom;
	switch (parameterlayouttype){
	case Layout::Type::VERTICAL:
		parameterLayoutposition.x -= Buttonsize.width*parameterLayoutnumber;
		Buttonsize.height *= parameterButton.size();
		break;
	case Layout::Type::HORIZONTAL:
		Buttonsize.width *= parameterButton.size();
		parameterLayoutposition.x -= Buttonsize.width;
		parameterLayoutposition.y -= Buttonsize.height*parameterLayoutnumber;
		break;
	default:
		break;
	}

	returnLayout->setContentSize(Buttonsize);
	returnLayout->setPosition(parameterLayoutposition);
	
	returnLayout->setBackGroundColor(Color3B(80.0f, 80.0f, 0.0f));
	returnLayout->setBackGroundColorOpacity(250);

	for (int i = 0; i < parameterButton.size(); i++){
		returnLayout->addChild(parameterButton.at(i));
	}
	return returnLayout;
}

std::vector<Button*> createbutton(std::vector<std::string> parameterTitleText, LinearLayoutParameter::LinearGravity parameterlineartype,
	std::vector<std::function<void(Ref*)>> parameterButtonEvent){
	
	std::vector<Button*> returnbutton = std::vector<Button*>(parameterTitleText.size());

	ui::Margin Marginvalue = Margin(2.0f, 0.0f, 2.0f, 10.0f);

	LinearLayoutParameter* ButtonMargin = LinearLayoutParameter::create();
	ButtonMargin->setGravity(parameterlineartype);
	ButtonMargin->setMargin(Marginvalue);

	for (int i = 0; i < parameterTitleText.size(); i++){
		returnbutton.at(i) = Button::create("btn_up60x60.png", "btn_down60x60.png", "btn_disable60x60.png");
		returnbutton.at(i)->setTitleText(parameterTitleText.at(i).c_str());
		returnbutton.at(i)->setTitleColor(Color3B(1.0f, 0.0f, 1.0f));
		returnbutton.at(i)->setLayoutParameter(ButtonMargin);
	}

	for (int i = 0; i < parameterButtonEvent.size(); i++){
		returnbutton.at(i)->addClickEventListener(parameterButtonEvent.at(i));
	}

	return returnbutton;
}