#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
#include "MyPhysics.h"
#include "cocos2d.h"
#include "ui\CocosGUI.h"
#include <functional>
/*
해야 할것
스위치들과 슬라이더들이 유저가 정해논것 그대로 남겨두고 싶다. 
화면 터치시 ui창들이 다 닫히는 것
*/
/*
HelloWold에서 ui를 만들고 myphysics에 값들을 변경 시킬지
myphyiscs가 ui를 가지고 있어서 자체적으로 돌아가게 만들까?
*/
enum ButtonState{ None, basicclick, SetBlockclick, CreateBlock };

using namespace cocos2d;
using namespace cocos2d::ui;
class HelloWorld : public cocos2d::Layer
{
private: 
	/*화면 크기
	*/
	Size Directorsize;

	MyPhysics Object;
	Objectvalue Currentlysetobject;
	/*지금 버튼의 상태를 보는 변수
	*/
	ButtonState PlayerbuttonClick;
	/*전체 화면을 담당하는 레이아웃
	*/
	LayerColor* ScreanLayout;
	void ScreanLayoutcreate();

	/*우측 button들을 정리하는 layout
	*/
	void RightLayoutCollectioncreate();

	/*기본으로 물체를 만들거나 설정들을 할수있는 변수들
	*/
	std::vector<Button*> BasicEventButton;
	Layout* BasicButtonLayout;
	void BasicEventButtoncreate();

	/*blok버튼을 눌럿을때 나오는 버튼의 변수들
	*/
	std::vector<Button*> ClickBlockEventButton;
	Layout* ClickBlockLayout;
	void ClickBlockEventButtoncreate();

	bool CoordinateplateON = false;
	DrawNode* CoordinateplateDraw;
public:

    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(HelloWorld);
	
	void onEnter();
	
	void Drivingworld(float);
	void LoboratoryLayerEvnet(Touch*, Event*);

	void BasicButtonEvent(Ref* ref);
	void SetBlockButtonEvent(Ref* ref);
	void CoordinateplateButtonEvent(Ref* ref);

	void objectgravity(float dt);
};
/*button을 정렬하는 layout을 만드는 함수
type은 VERTICAL과 HORIZONTAL밖에 없다.
*/
Layout* createlayout(Layout::Type, std::vector<Button*>, Vec2 parameterLayoutposition, int parameterLayoutnumber);
std::vector<Button*> createbutton(std::vector<std::string>, LinearLayoutParameter::LinearGravity, std::vector<std::function<void(Ref*)>> parameterButtonEvent);
#endif // __HELLOWORLD_SCENE_H__
