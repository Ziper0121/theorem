#include "UserInterface.h"
#include <stdlib.h>
#define TEXT_DEFINE 

#define UIRIGHT 0x1
#define UILEFT 0x2
#define UIUP 0x4
#define UIDOWN 0x8

/*
이거 꼭 봐라
UIKind에 lable을 추가하여서 글자만 나오는 것을 만들어보자  
*/
/*
UI를 만드는 함수들
*/

void MyUI::ResetMyUI(){
	ScreenSize = Director::getInstance()->getWinSize();
	ScreenRatio = Size(ScreenSize.width / (ScreenSize.height + ScreenSize.width)*10.0f,
		ScreenSize.height / (ScreenSize.height + ScreenSize.width)*10.0f);
	MyUIMargin = Size(0.0f, 0.0f);
	/*
	UI Position Setting*/
	
	MyUserInterfaceLocation |= UIRIGHT | UIUP;
	/*
	UI Size Setting
	*/
	MyUIButtonSize = ScreenRatio*11.0f;
	MyUIButtonSize.height += ScreenRatio.height * 2;

	MyUISliderSize = Size(MyUIButtonSize.width * 3, ScreenRatio.height);

	MyUILabelSize = 20.0f;
	MyUI_Taget = Target_of_UI::NONE;
	SetUIStartPoint();
}
/*UI의 정보들을 담는 함수
*/
void MyUI::ResetMyUIData(){
	/*
	거꾸로인 이유는
	button들이 다음 interface의 값이 초기화가 안이루어 졌기 때문이다.
	*/
	/*
	**************************Fivelayer***************************/
	FiveLayderInformation();
	/*
	**************************FourLayer***************************/
	FourLayerInformation();
	/*
	**************************ThreeLayer***************************/
	ThreeLayerInformation();
	/*
	**************************TowLayer***************************/
	TwoLayerInformation();
	/*
	**************************OneLayer***************************/
	OneLayerInformation();
}

void MyUI::OneLayerInformation(){
	MyUIOnelayer.UIText.push_back("Microbe\nColor4F");
	MyUIOnelayer.UIText.push_back("particle");
	MyUIOnelayer.UIText.push_back("gravity");
	MyUIOnelayer.UIText.push_back("Coordinate");
	for (int i = 0; i < MyUIOnelayer.UIText.size() - 1; i++){
		MyUIOnelayer.UIType.push_back(UIKind::BUTTON);
		MyUIOnelayer.MyUserInterfaceEvent.push_back([=](Ref* ref){
			removeMyUIDrawMicrobe(1);
			CreateMyUserInterface(MyUITwolayer.at(i),i);
			MyUI_Taget = Target_of_UI(i+1);
		});
	}
	MyUIOnelayer.UIType.push_back(UIKind::CHECKBOX);
	MyUIOnelayer.CheckState.push_back(false);
	MyUIOnelayer.MyUserInterfaceEvent.push_back(std::bind(&MyUI::OneCoordinateplaneSwitch, this, std::placeholders::_1));

}
void MyUI::TwoLayerInformation(){
	/*
	**************************Microbe***************************/
	MyUITwolayer = std::vector<UIVariable>(3);
	MyUITwolayer.at(0).UIText.push_back("CreateBox");
	MyUITwolayer.at(0).UIText.push_back("Size");
	MyUITwolayer.at(0).UIText.push_back("Color");
	MyUITwolayer.at(0).UIText.push_back("Location");
	MyUITwolayer.at(0).UIType.push_back(UIKind::BUTTON);
	MyUITwolayer.at(0).MyUserInterfaceEvent.push_back(std::bind(&MyUI::ThreeMicrobeCreate, this, std::placeholders::_1));
	for (int i = 0; i < MyUITwolayer.at(0).UIText.size() - 1; i++){
		MyUITwolayer.at(0).UIType.push_back(UIKind::BUTTON);
		MyUITwolayer.at(0).MyUserInterfaceEvent.push_back([=](Ref* ref){
			if (microbe.size() > 0){
				removeMyUIDrawMicrobe(2);
				CreateMyUserInterface(MyUIThreeMicrobelayer.at(i),i);
			}
		});
	}
	/*
	**************************Particle***************************/
	MyUITwolayer.at(1).UIText.push_back("Kinds");
	MyUITwolayer.at(1).UIText.push_back("Setting");
	for (int i = 0; i < MyUITwolayer.at(1).UIText.size(); i++){
		MyUITwolayer.at(1).UIType.push_back(UIKind::BUTTON);
		MyUITwolayer.at(1).MyUserInterfaceEvent.push_back([=](Ref* ref){
			removeMyUIDrawMicrobe(2);
			CreateMyUserInterface(MyUIThreeParticlelayer.at(i),i);
		});
	}

	/*
	**************************gravity***************************/
	
	MyUITwolayer.at(2).UIText.push_back("Gravity");
	MyUITwolayer.at(2).UIText.push_back("acceleration");
	MyUITwolayer.at(2).UIText.push_back("Friction");
	for (int i = 0; i < MyUITwolayer.at(2).UIText.size(); i++){
		MyUITwolayer.at(2).UIType.push_back(UIKind::BUTTON);
		MyUITwolayer.at(2).MyUserInterfaceEvent.push_back([=](Ref* ref){
			removeMyUIDrawMicrobe(2);
			CreateMyUserInterface(MyUIThreeMyPhysicslayer.at(i),i);
		});
	}
	/*
	*****************************************************/
}
void MyUI::ThreeLayerInformation() {
	/*
	**************************Microbe***************************/
	MyUIThreeMicrobelayer = std::vector<UIVariable>(3);
	MyUIThreeMicrobelayer.at(0).UIText.push_back("AllScale");
	MyUIThreeMicrobelayer.at(0).UIText.push_back("Width");
	MyUIThreeMicrobelayer.at(0).UIText.push_back("height");
	for (int i = 0; i < MyUIThreeMicrobelayer.at(0).UIText.size(); i++){
		MyUIThreeMicrobelayer.at(0).UIType.push_back(UIKind::SLIDER);
		MyUIThreeMicrobelayer.at(0).SliderNow.push_back(1);
	}
	MyUIThreeMicrobelayer.at(0).SliderMax.push_back(10);
	MyUIThreeMicrobelayer.at(0).SliderMax.push_back(ScreenSize.width / 10);
	MyUIThreeMicrobelayer.at(0).SliderMax.push_back(ScreenSize.height / 10);

	MyUIThreeMicrobelayer.at(1).UIText.push_back("Red");
	MyUIThreeMicrobelayer.at(1).UIText.push_back("Green");
	MyUIThreeMicrobelayer.at(1).UIText.push_back("Blue");
	MyUIThreeMicrobelayer.at(1).UIText.push_back("Transparency");
	for (int i = 0; i < MyUIThreeMicrobelayer.at(1).UIText.size(); i++){
		MyUIThreeMicrobelayer.at(1).UIType.push_back(UIKind::SLIDER);
		MyUIThreeMicrobelayer.at(1).SliderMax.push_back(250);
		if (i != 3){
			MyUIThreeMicrobelayer.at(1).SliderNow.push_back(200);
		}
		else{
			MyUIThreeMicrobelayer.at(1).SliderNow.push_back(250);
		}
	}

	MyUIThreeMicrobelayer.at(2).UIText.push_back("X");
	MyUIThreeMicrobelayer.at(2).UIText.push_back("Y");
	for (int i = 0; i < MyUIThreeMicrobelayer.at(2).UIText.size(); i++){
		MyUIThreeMicrobelayer.at(2).UIType.push_back(UIKind::SLIDER);
	}
	MyUIThreeMicrobelayer.at(2).SliderMax.push_back(ScreenSize.width);
	MyUIThreeMicrobelayer.at(2).SliderMax.push_back(ScreenSize.height);
	MyUIThreeMicrobelayer.at(2).SliderNow.push_back(ScreenSize.width / 2);
	MyUIThreeMicrobelayer.at(2).SliderNow.push_back(ScreenSize.height / 2);

	for (int i = 0, k = 0; i < MyUIThreeMicrobelayer.size(); k++){
		if (k >= MyUIThreeMicrobelayer.at(i).UIText.size()){
			k = -1;
			i++;
			continue;
		}
		MyUIThreeMicrobelayer.at(i).MySliderEvent.push_back([=](Ref* ref, Slider::EventType SliderType){
			if (SliderType == Slider::EventType::ON_PERCENTAGE_CHANGED){
				setObjectvalrue(2, i, k, SLIDER_PERCENT, &MyUIThreeMicrobelayer.at(i));
			}
			else if (SliderType == Slider::EventType::ON_SLIDEBALL_UP){
				setObjectvalrue(2, i, k, SLIDER_PERCENT, &MyUIThreeMicrobelayer.at(i));
			}
		});
	}
	/*
	**************************Particle***************************/
	MyUIThreeParticlelayer = std::vector<UIVariable>(2);
	MyUIThreeParticlelayer.at(0).UIText.push_back("Fire");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Fireworks");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Sun");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Galaxy");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Flower");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Meteor");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Spiral");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Explosion");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Smoke");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Snow");
	MyUIThreeParticlelayer.at(0).UIText.push_back("Rain");
	for (int i = 0; i < MyUIThreeParticlelayer.at(0).UIText.size(); i++){
		MyUIThreeParticlelayer.at(0).UIType.push_back(UIKind::CHECKBOX);
		MyUIThreeParticlelayer.at(0).CheckState.push_back(false);
		MyUIThreeParticlelayer.at(0).MyUserInterfaceEvent.push_back([=](Ref* ref){
			setObjectvalrue(2, 0, i, CHECK_SWITCH, &MyUIThreeParticlelayer.at(0));
		});
	}

	MyUIThreeParticlelayer.at(1).UIText.push_back("Emitter");
	MyUIThreeParticlelayer.at(1).UIText.push_back("shpe");
	MyUIThreeParticlelayer.at(1).UIText.push_back("particleMode");
	for (int i = 0; i < MyUIThreeParticlelayer.at(1).UIText.size(); i++){
		MyUIThreeParticlelayer.at(1).UIType.push_back(UIKind::BUTTON);
		MyUIThreeParticlelayer.at(1).MyUserInterfaceEvent.push_back([=](Ref*){
			removeMyUIDrawMicrobe(3);
			CreateMyUserInterface(MyUIFourMyParticlelayer.at(i),i);
		});
	}
	/*
	**************************MYPhysics**************************/

	MyUIThreeMyPhysicslayer = std::vector<UIVariable>(3);
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("Gravity");
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("TOP");
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("RIGHT");
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("BOTTOM");
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("LEFT");
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("Power");
	MyUIThreeMyPhysicslayer.at(0).UIText.push_back("Piece");

	for (int i = 2; i < MyUIThreeMyPhysicslayer.at(0).UIText.size(); i++){
		MyUIThreeMyPhysicslayer.at(0).UIType.push_back(UIKind::CHECKBOX);
		MyUIThreeMyPhysicslayer.at(0).CheckState.push_back(false);
	}

	MyUIThreeMyPhysicslayer.at(0).CheckState.at(3) = true;

	MyUIThreeMyPhysicslayer.at(0).UIType.push_back(UIKind::SLIDER);
	MyUIThreeMyPhysicslayer.at(0).UIType.push_back(UIKind::BUTTON);
	MyUIThreeMyPhysicslayer.at(0).SliderNow.push_back(5);
	MyUIThreeMyPhysicslayer.at(0).SliderMax.push_back(10);


	MyUIThreeMyPhysicslayer.at(1).UIText.push_back("Interval");
	MyUIThreeMyPhysicslayer.at(1).UIText.push_back("Power");
	MyUIThreeMyPhysicslayer.at(1).UIText.push_back("MAX");

	for (int i = 0; i < MyUIThreeMyPhysicslayer.at(1).UIText.size(); i++){
		MyUIThreeMyPhysicslayer.at(1).UIType.push_back(UIKind::SLIDER);
	}

	MyUIThreeMyPhysicslayer.at(1).SliderNow.push_back(2);
	MyUIThreeMyPhysicslayer.at(1).SliderNow.push_back(1);
	MyUIThreeMyPhysicslayer.at(1).SliderNow.push_back(8);

	MyUIThreeMyPhysicslayer.at(1).SliderMax.push_back(10);
	MyUIThreeMyPhysicslayer.at(1).SliderMax.push_back(5);
	MyUIThreeMyPhysicslayer.at(1).SliderMax.push_back(16);


	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("CreateFriction");
	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("X");
	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("Y");
	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("Width");
	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("Height");
	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("Power");
	MyUIThreeMyPhysicslayer.at(2).UIText.push_back("FrictionNum");

	MyUIThreeMyPhysicslayer.at(2).UIType.push_back(UIKind::BUTTON);
	for (int i = 1; i < MyUIThreeMyPhysicslayer.at(2).UIText.size(); i++){
		MyUIThreeMyPhysicslayer.at(2).UIType.push_back(UIKind::SLIDER);
	}

	MyUIThreeMyPhysicslayer.at(2).SliderNow.push_back(480);
	MyUIThreeMyPhysicslayer.at(2).SliderNow.push_back(320);
	MyUIThreeMyPhysicslayer.at(2).SliderNow.push_back(60);
	MyUIThreeMyPhysicslayer.at(2).SliderNow.push_back(1);
	MyUIThreeMyPhysicslayer.at(2).SliderNow.push_back(5);
	MyUIThreeMyPhysicslayer.at(2).SliderNow.push_back(0);

	MyUIThreeMyPhysicslayer.at(2).SliderMax.push_back(960);
	MyUIThreeMyPhysicslayer.at(2).SliderMax.push_back(640);
	MyUIThreeMyPhysicslayer.at(2).SliderMax.push_back(96);
	MyUIThreeMyPhysicslayer.at(2).SliderMax.push_back(64);
	MyUIThreeMyPhysicslayer.at(2).SliderMax.push_back(10);
	MyUIThreeMyPhysicslayer.at(2).SliderMax.push_back(UIPhysics.MyPhysicsFriction.size());

	for (int i = 0, k = 0, SliderNum = 0; i < MyUIThreeMyPhysicslayer.size(); k++){
		if (k >= MyUIThreeMyPhysicslayer.at(i).UIText.size()){
			k = -1;
			SliderNum = 0;
			i++;
			continue;
		}
		switch (MyUIThreeMyPhysicslayer.at(i).UIType.at(k))
		{
		case UIKind::BUTTON:
			if (i == 2){
				MyUIThreeMyPhysicslayer.at(i).MyUserInterfaceEvent.push_back(std::bind(&MyUI::PushFriction, this, std::placeholders::_1));
				break;
			}

			MyUIThreeMyPhysicslayer.at(i).MyUserInterfaceEvent.push_back([=](Ref* ref){
				removeMyUIDrawMicrobe(3);
				CreateMyUserInterface(MyUIFourMyPhysicslayer,i);
			});
			break;
		case UIKind::SLIDER:
			MyUIThreeMyPhysicslayer.at(i).MySliderEvent.push_back([=](Ref* ref, Slider::EventType SliderType){
				if (SliderType == Slider::EventType::ON_PERCENTAGE_CHANGED){
					setObjectvalrue(2, i, k, SLIDER_PERCENT, &MyUIThreeMyPhysicslayer.at(i));
				}
				else if (SliderType == Slider::EventType::ON_SLIDEBALL_UP){
					setObjectvalrue(2, i, k, SLIDER_PERCENT, &MyUIThreeMyPhysicslayer.at(i));
				}
			});
			SliderNum++;
			break;
		case UIKind::CHECKBOX:
			MyUIThreeMyPhysicslayer.at(i).MyUserInterfaceEvent.push_back([=](Ref* ref){
				setObjectvalrue(2, i, k, CHECK_SWITCH, &MyUIThreeMyPhysicslayer.at(i));
			});
			break;
		default:
			break;
		}
	}
	/*
	*****************************************************/

}
void MyUI::FourLayerInformation(){

	/*
	**************************FourLayer***************************/
	/*
	**************************Paticle***************************/
	MyUIFourMyParticlelayer = std::vector<UIVariable>(3);
	MyUIFourMyParticlelayer.at(0).UIText.push_back("EnutterMode");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("Var");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("Angle");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("EmissionRate");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("PosVarX");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("PosVarY");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("StartRadius");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("EndRadius");
	MyUIFourMyParticlelayer.at(0).UIText.push_back("RotatePerSecond");

	for (int i = 0; i < 2; i++){
		MyUIFourMyParticlelayer.at(0).UIType.push_back(UIKind::CHECKBOX);
		MyUIFourMyParticlelayer.at(0).CheckState.push_back(false);
	}

	for (int i = 2; i < MyUIFourMyParticlelayer.at(0).UIText.size(); i++){
		MyUIFourMyParticlelayer.at(0).UIType.push_back(UIKind::SLIDER);
	}

	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(360);
	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(100);
	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(ScreenSize.width / 2);
	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(ScreenSize.height / 2);
	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(100);
	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(100);
	MyUIFourMyParticlelayer.at(0).SliderMax.push_back(100);

	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(90);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(90);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(50);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(0);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(0);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(0);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(0);
	MyUIFourMyParticlelayer.at(0).SliderNow.push_back(0);

	MyUIFourMyParticlelayer.at(1).UIText.push_back("Color4F");
	MyUIFourMyParticlelayer.at(1).UIText.push_back("BlendAdditive");
	MyUIFourMyParticlelayer.at(1).UIText.push_back("Var");
	MyUIFourMyParticlelayer.at(1).UIText.push_back("StartSize");
	MyUIFourMyParticlelayer.at(1).UIText.push_back("EndSize");
	MyUIFourMyParticlelayer.at(1).UIText.push_back("StartSpin");
	MyUIFourMyParticlelayer.at(1).UIText.push_back("EndSpin");

	MyUIFourMyParticlelayer.at(1).UIType.push_back(UIKind::BUTTON);

	for (int i = 0; i < 2; i++){
		MyUIFourMyParticlelayer.at(1).UIType.push_back(UIKind::CHECKBOX);
		MyUIFourMyParticlelayer.at(1).CheckState.push_back(false);
	}

	for (int i = 3; i < MyUIFourMyParticlelayer.at(1).UIText.size(); i++){
		MyUIFourMyParticlelayer.at(1).UIType.push_back(UIKind::SLIDER);
	}
	MyUIFourMyParticlelayer.at(1).SliderMax.push_back(50);
	MyUIFourMyParticlelayer.at(1).SliderMax.push_back(50);
	MyUIFourMyParticlelayer.at(1).SliderMax.push_back(100);
	MyUIFourMyParticlelayer.at(1).SliderMax.push_back(100);

	MyUIFourMyParticlelayer.at(1).SliderNow.push_back(20);
	MyUIFourMyParticlelayer.at(1).SliderNow.push_back(20);
	MyUIFourMyParticlelayer.at(1).SliderNow.push_back(0);
	MyUIFourMyParticlelayer.at(1).SliderNow.push_back(0);

	MyUIFourMyParticlelayer.at(2).UIText.push_back("Var");
	MyUIFourMyParticlelayer.at(2).UIText.push_back("Speed");
	MyUIFourMyParticlelayer.at(2).UIText.push_back("GravityX");
	MyUIFourMyParticlelayer.at(2).UIText.push_back("GravityY");
	MyUIFourMyParticlelayer.at(2).UIText.push_back("RadicalAccel");
	MyUIFourMyParticlelayer.at(2).UIText.push_back("TangentialAccel");

	MyUIFourMyParticlelayer.at(2).UIType.push_back(UIKind::CHECKBOX);
	MyUIFourMyParticlelayer.at(2).CheckState.push_back(false);

	for (int i = 1; i < MyUIFourMyParticlelayer.at(2).UIText.size(); i++){
		MyUIFourMyParticlelayer.at(2).UIType.push_back(UIKind::SLIDER);
	}
	MyUIFourMyParticlelayer.at(2).SliderMax.push_back(400);
	MyUIFourMyParticlelayer.at(2).SliderMax.push_back(400);
	MyUIFourMyParticlelayer.at(2).SliderMax.push_back(400);
	MyUIFourMyParticlelayer.at(2).SliderMax.push_back(400);
	MyUIFourMyParticlelayer.at(2).SliderMax.push_back(400);

	MyUIFourMyParticlelayer.at(2).SliderNow.push_back(200);
	MyUIFourMyParticlelayer.at(2).SliderNow.push_back(200);
	MyUIFourMyParticlelayer.at(2).SliderNow.push_back(200);
	MyUIFourMyParticlelayer.at(2).SliderNow.push_back(200);
	MyUIFourMyParticlelayer.at(2).SliderNow.push_back(200);

	for (int i = 0, k = 0,SliderNum = 0; i < MyUIFourMyParticlelayer.size(); k++){
		if (k >= MyUIFourMyParticlelayer.at(i).UIText.size()){
			k = -1;
			SliderNum = 0;
			i++;
			continue;
		}
		switch (MyUIFourMyParticlelayer.at(i).UIType.at(k))
		{
		case UIKind::BUTTON:
			MyUIFourMyParticlelayer.at(i).MyUserInterfaceEvent.push_back([=](Ref*){
				removeMyUIDrawMicrobe(4);
				CreateMyUserInterface(MyUIFiveParticlelayer,i);
			});
			break;
		case UIKind::SLIDER:
			MyUIFourMyParticlelayer.at(i).MySliderEvent.push_back([=](Ref* ref, Slider::EventType SliderType){
				if (SliderType == Slider::EventType::ON_PERCENTAGE_CHANGED){
					setObjectvalrue(3, i, k, SLIDER_PERCENT, &MyUIFourMyParticlelayer.at(i));
				}
				else if (SliderType == Slider::EventType::ON_SLIDEBALL_UP){
					setObjectvalrue(3, i, k, SLIDER_PERCENT, &MyUIFourMyParticlelayer.at(i));
				}
			});
			SliderNum++;
			break;
		case UIKind::CHECKBOX:
			MyUIFourMyParticlelayer.at(i).MyUserInterfaceEvent.push_back([=](Ref* ref){
				setObjectvalrue(3, i, k, CHECK_SWITCH, &MyUIFourMyParticlelayer.at(i));
			});
			break;
		default:
			break;
		}
	}

	/*
	**************************MYPhysics**************************/
	MyUIFourMyPhysicslayer.UIText.push_back("Createpiece");
	MyUIFourMyPhysicslayer.UIText.push_back("X");
	MyUIFourMyPhysicslayer.UIText.push_back("Y");
	MyUIFourMyPhysicslayer.UIText.push_back("Widht");
	MyUIFourMyPhysicslayer.UIText.push_back("Hight");
	MyUIFourMyPhysicslayer.UIText.push_back("Power");
	MyUIFourMyPhysicslayer.UIText.push_back("PieceNumber");

	MyUIFourMyPhysicslayer.UIType.push_back(UIKind::BUTTON);
	for (int i = 1; i < MyUIFourMyPhysicslayer.UIText.size(); i++){
		MyUIFourMyPhysicslayer.UIType.push_back(UIKind::SLIDER);
	}
	

	MyUIFourMyPhysicslayer.SliderNow.push_back(480);
	MyUIFourMyPhysicslayer.SliderNow.push_back(320);
	MyUIFourMyPhysicslayer.SliderNow.push_back(60);
	MyUIFourMyPhysicslayer.SliderNow.push_back(60);
	MyUIFourMyPhysicslayer.SliderNow.push_back(1);
	MyUIFourMyPhysicslayer.SliderNow.push_back(0);

	MyUIFourMyPhysicslayer.SliderMax.push_back(960);
	MyUIFourMyPhysicslayer.SliderMax.push_back(640);
	MyUIFourMyPhysicslayer.SliderMax.push_back(960);
	MyUIFourMyPhysicslayer.SliderMax.push_back(640);
	MyUIFourMyPhysicslayer.SliderMax.push_back(10);
	MyUIFourMyPhysicslayer.SliderMax.push_back(UIPhysics.MyGravityPiece.size());

	MyUIFourMyPhysicslayer.MyUserInterfaceEvent.push_back(std::bind(&MyUI::PushPiece, this, std::placeholders::_1));
	for (int i = 1; i < MyUIFourMyPhysicslayer.UIType.size(); i++){
		MyUIFourMyPhysicslayer.MySliderEvent.push_back([=](Ref* ref, Slider::EventType SliderType){
			setObjectvalrue(3, 0, i, SLIDER_PERCENT, &MyUIFourMyPhysicslayer);
		});
	}

	/*
	*****************************************************/
}
void MyUI::FiveLayderInformation(){
	/*
	BUtton을 누를 경우 (Start Nomar) 모드가 바뀐다 (end Var)
	*/
	MyUIFiveParticlelayer.UIText.push_back("Start");
	MyUIFiveParticlelayer.UIText.push_back("Var");
	MyUIFiveParticlelayer.UIText.push_back("R");
	MyUIFiveParticlelayer.UIText.push_back("G");
	MyUIFiveParticlelayer.UIText.push_back("B");
	MyUIFiveParticlelayer.UIText.push_back("T");

	MyUIFiveParticlelayer.UIType.push_back(UIKind::CHECKBOX);
	MyUIFiveParticlelayer.UIType.push_back(UIKind::CHECKBOX);
	MyUIFiveParticlelayer.CheckState.push_back(true);
	MyUIFiveParticlelayer.CheckState.push_back(false);
	for (int i = 2; i < MyUIFiveParticlelayer.UIText.size(); i++){
		MyUIFiveParticlelayer.UIType.push_back(UIKind::SLIDER);
	}

	MyUIFiveParticlelayer.SliderNow.push_back(100);
	MyUIFiveParticlelayer.SliderNow.push_back(100);
	MyUIFiveParticlelayer.SliderNow.push_back(100);
	MyUIFiveParticlelayer.SliderNow.push_back(250);

	MyUIFiveParticlelayer.SliderMax.push_back(250);
	MyUIFiveParticlelayer.SliderMax.push_back(250);
	MyUIFiveParticlelayer.SliderMax.push_back(250);
	MyUIFiveParticlelayer.SliderMax.push_back(250);

	for (int i = 0; i < MyUIFiveParticlelayer.UIText.size(); i++){
		switch (MyUIFiveParticlelayer.UIType.at(i))
		{
		case UIKind::BUTTON:
			break;
		case UIKind::SLIDER:
			MyUIFiveParticlelayer.MySliderEvent.push_back([=](Ref* ref, Slider::EventType SliderType){
				if (SliderType == Slider::EventType::ON_PERCENTAGE_CHANGED){
					setObjectvalrue(2, 0, i, SLIDER_PERCENT, &MyUIFiveParticlelayer);
				}
				else if (SliderType == Slider::EventType::ON_SLIDEBALL_UP){
					setObjectvalrue(2, 0, i, SLIDER_PERCENT, &MyUIFiveParticlelayer);
				}
			});
			break;
		case UIKind::CHECKBOX:
			MyUIFiveParticlelayer.MyUserInterfaceEvent.push_back([=](Ref* ref){
				setObjectvalrue(2, 0, i, CHECK_SWITCH, &MyUIFiveParticlelayer);
			});
			break;
		default:
			break;
		}
	}

}

Button* MyUI::CreateMyUIButton(int Buttonstairs, std::string Pname){
	Button* button = Button::create("WhiteBox20x20.png");
	button->ignoreContentAdaptWithSize(false);
	button->setContentSize(MyUIButtonSize);
	button->setPosition(Vec2(0.0f, MyUIButtonSize.height* Buttonstairs));
	button->setTitleText(Pname.c_str());
	button->setTitleColor(Color3B(1, 1, 0));
	button->setAnchorPoint(MyUIAnchorpoint);
	return button;
}
Slider* MyUI::CreateMyUISlider(int Sliderstairs, int sriderpercent, int SliderNow, std::function<void(Ref*, Slider::EventType)> sliderevent){
	Slider* slider = Slider::create("Sliderba200X10.png", "15X15.png");
	slider->ignoreContentAdaptWithSize(false);
	slider->setContentSize(MyUISliderSize);
	slider->setPosition(Vec2(MyUISliderSize.width, MyUIButtonSize.height * Sliderstairs));
	slider->setMaxPercent(sriderpercent);
	slider->setPercent(SliderNow);
	slider->setAnchorPoint(MyUIAnchorpoint);
	slider->addEventListener(sliderevent);
	return slider;
}
CheckBox* MyUI::CreateMyUICheckBox(int CheckBoxstairs, bool CheckState){
	CheckBox* checkbox = CheckBox::create("CheckBoxOff.png", "CheckBoxOn.png");
	checkbox->setSelectedState(CheckState);
	checkbox->setPosition(Vec2(0.0f, MyUIButtonSize.height* CheckBoxstairs));
	checkbox->setAnchorPoint(MyUIAnchorpoint);
	return checkbox;
}
EditBox* MyUI::CreateMyUIEditBox(int EditBoxStairs, int EditBoxText){
	EditBox* editbox = EditBox::create(Size(ScreenRatio.width*5.0f, ScreenRatio.height * 3.0f), "White20x20.png");

	char fontnumber[5];
	itoa(EditBoxText, fontnumber, 10);

	editbox->setInputMode(EditBox::InputMode::NUMERIC);
	editbox->setText(fontnumber);
	editbox->setFontSize(11.0f);
	editbox->setPositionY(EditBoxStairs + 1 * MyUIButtonSize.height);
	editbox->setAnchorPoint(Vec2(MyUIAnchorpoint.x,-1.0f));
	editbox->setFontColor(Color4B::WHITE);
	editbox->setColor(Color3B(INTERFACE_COLOR4B));

	return editbox;
}
/*UIVariavle의 값을 통해 내가 만들고 싶은 interface를 만드는 알고리즘
*/
void MyUI::InterfaceStart(Layer* parentslayer){
	Currentlayer = parentslayer;

	auto eventontouch = EventListenerTouchOneByOne::create();
	eventontouch->onTouchBegan = [=](Touch* touch, Event* et){
		Selectedmicrobe = false;
		for (int i = 0; i < microbe.size(); i++){
			if (microbe.at(i).Appearance->getPosition() - microbe.at(i).Appearance->getContentSize() / 2 < touch->getLocation()
				&& microbe.at(i).Appearance->getPosition() + microbe.at(i).Appearance->getContentSize() / 2> touch->getLocation()){
				microbenum = i;
				Selectedmicrobe = true;
			}
		}
		removeMyUIDrawMicrobe(1);
		return true;
	};
	eventontouch->onTouchMoved = std::bind(&MyUI::microchoiceMove, this, std::placeholders::_1, std::placeholders::_2);
	parentslayer->getEventDispatcher()->addEventListenerWithSceneGraphPriority(eventontouch, parentslayer);

	CreateMyUserInterface(MyUIOnelayer,0);
}

void MyUI::CreateMyUserInterface(UIVariable UIKinds,int KindsBundleNumber){
	AllUIDrawNodeValue AllDraNodevalue;
	AllDraNodevalue.UIBackground = DrawNode::create();

	int SliderNum = 0;
	int CheckNum = 0;
	int UIEventNum = 0;

	Vec2 Now_Benchmark = MyUIBenchmark;

	AllDraNodevalue.NodeSize = Size(MyUIButtonSize.width, 0.0f);

	for (int i = 0; i < MyUserInterface.size(); i++){
		Now_Benchmark.x += MyUserInterface.at(i).NodeSize.width* MyUIPlacement.x;
	} 

	for (int i = 0; i < UIKinds.UIType.size(); i++){
		switch (UIKinds.UIType.at(i))
		{
		case BUTTON:
			AllDraNodevalue.UI.push_back(CreateMyUIButton(i, UIKinds.UIText.at(i)));
			break;
		case SLIDER:{

			/*
			Create*/
			AllDraNodevalue.UI.push_back(CreateMyUISlider(i, UIKinds.SliderMax.at(SliderNum), UIKinds.SliderNow.at(SliderNum)
				, UIKinds.MySliderEvent.at(SliderNum)));
			AllDraNodevalue.SliderMax.push_back(UIKinds.SliderMax.at(SliderNum));

			AllDraNodevalue.NodeSize.width = MyUISliderSize.width;

			/*
			EditBox*/
			AllDraNodevalue.EditSliderShame.push_back(CreateMyUIEditBox(SliderNum,UIKinds.SliderNow.at(SliderNum)));

			if (UIKinds.SliderMax.at(SliderNum) >= 100){AllDraNodevalue.EditSliderShame.back()->setMaxLength(3);}
			else if (UIKinds.SliderMax.at(SliderNum) >= 10){AllDraNodevalue.EditSliderShame.back()->setMaxLength(2);}
			else{AllDraNodevalue.EditSliderShame.back()->setMaxLength(1);}
			
			AllDraNodevalue.EditSliderShame.back()->setPosition(Now_Benchmark +
				Vec2(AllDraNodevalue.UI.back()->getPosition().x * MyUIPlacement.x, AllDraNodevalue.UI.back()->getPosition().y * MyUIPlacement.y));

			AllDraNodevalue.EditSliderShame.back()->setDelegate(this);
			AllDraNodevalue.UIBackground->addChild(AllDraNodevalue.EditSliderShame.back());
			SliderNum++;
			break;
		}
		case CHECKBOX:
			AllDraNodevalue.UI.push_back(CreateMyUICheckBox(i, UIKinds.CheckState.at(CheckNum)));
			CheckNum++;
			break;
		default:
			break;
		}

		AllDraNodevalue.UI.back()->setPosition(Now_Benchmark +
			Vec2(AllDraNodevalue.UI.back()->getPosition().x * MyUIPlacement.x - AllDraNodevalue.NodeSize.width
			, AllDraNodevalue.UI.back()->getPosition().y * MyUIPlacement.y));

		if (UIKinds.UIType.at(i) != BUTTON){
			AllDraNodevalue.UILabel.push_back(Label::create(UIKinds.UIText.at(i), "", 11.0f));
			AllDraNodevalue.UILabel.back()->setPosition(Vec2(AllDraNodevalue.UI.back()->getPosition().x,
				AllDraNodevalue.UI.back()->getPosition().y - MyUIButtonSize.height * 0.5));
			AllDraNodevalue.UILabel.back()->setTextColor(Color4B(0.0f, 0.0f, 0.0f, 250.0f));
			AllDraNodevalue.UIBackground->addChild(AllDraNodevalue.UILabel.back(),1);
		}
		if (UIKinds.UIType.at(i) != SLIDER){
			AllDraNodevalue.UI.at(i)->addClickEventListener(UIKinds.MyUserInterfaceEvent.at(UIEventNum));
			UIEventNum++;
		}
		AllDraNodevalue.FromUI.push_back(UIKinds.UIType.at(i));
		AllDraNodevalue.NodeSize.height += MyUIButtonSize.height;
		AllDraNodevalue.UIBackground->addChild(AllDraNodevalue.UI.back(), 0);
	}
	AllDraNodevalue.NowUIBundleNumber = KindsBundleNumber;
	AllDraNodevalue.UIBackground->drawSolidRect(MyUIBenchmark, MyUIBenchmark - AllDraNodevalue.NodeSize, Color4F(INTERFACE_COLOR4B));
	MyUserInterface.push_back(AllDraNodevalue);
	Currentlayer->addChild(MyUserInterface.back().UIBackground);
}

void MyUI::SetUIStartPoint(){

	MyUIBenchmark = Vec2(0.0f, 0.0f);
	MyUIAnchorpoint = Vec2(0.0f, 0.0f);
	MyUIPlacement = Vec2(0.0f, 0.0f);

	if (MyUserInterfaceLocation & UIRIGHT){
		MyUIBenchmark.x = ScreenSize.width;
		MyUIAnchorpoint.x = 1.0f;
		MyUIPlacement.x = -1.0f;
	}
	else if (MyUserInterfaceLocation & UILEFT){
		MyUIBenchmark.x = 0.0f;
		MyUIAnchorpoint.x = 0.0f;
		MyUIPlacement.x = 1.0f;
	}
	else{
		MyUIBenchmark.x = ScreenSize.width / 2;
		MyUIAnchorpoint.x = 0.5f;
		MyUIPlacement.x = 1.0f;
	}

	if (MyUserInterfaceLocation & UIUP){
		MyUIBenchmark.y = ScreenSize.height;
		MyUIAnchorpoint.y = 1.0f;
		MyUIPlacement.y = -1.0f;
	}
	else if (MyUserInterfaceLocation & UIDOWN){
		MyUIBenchmark.y = 0.0f;
		MyUIAnchorpoint.y = 0.0f;
		MyUIPlacement.y = 1.0f;
	}
	else{
		MyUIBenchmark.y = ScreenSize.height / 2;
		MyUIAnchorpoint.y = 0.5f;
		MyUIPlacement.y = 1.0f;
	}

}
/*
버튼을 클릭했을때 그 위치를 참조하여 Box를 생성할 위치를 알려주는 함수
CreateDrawNode와 연관이 있다.
*/
void MyUI::removeMyUIDrawMicrobe(int Clickposition){

	while (MyUserInterface.size() > Clickposition){
		Currentlayer->removeChild(MyUserInterface.back().UIBackground);
		MyUserInterface.pop_back();
	}
}
void MyUI::microchoiceMove(Touch* tch, Event* et){
	if (Selectedmicrobe){
		NOW_MICROBE->setPosition(tch->getLocation());
	}
}
/*
lavel이 변하는 것을 다 바꾸자
Microbe의 사이즈가 변경될때마다 터치가되는 사이즈도 변경하다.
*/
void MyUI::editBoxReturn(EditBox* editBox){
	int BackgroundNum = MyUserInterface.size() - 1;
#pragma region UILAYERDATANUMBER
	std::vector<UIVariable*> TafetVariable;
	int UIBundle = 0;
	switch (MyUI_Taget)
	{
	case Target_of_UI::TARGET_MICROBE:
		switch (BackgroundNum)
		{
		case 2:
			for (int i = 0; i < MyUIThreeMicrobelayer.size(); i++){
				TafetVariable.push_back(&MyUIThreeMicrobelayer.at(i));
			}
			break;
		default:
			CCLOG("editbox BakcGroundnum two in a MyUI Taget is over ");
			break;
		}
		break;
	case Target_of_UI::TARGET_PARTICLE:
		switch (BackgroundNum)
		{
		case 2:
			for (int i = 0; i < MyUIThreeParticlelayer.size(); i++){
				TafetVariable.push_back(&MyUIThreeParticlelayer.at(i));
			}
			break;
		case 3:
			for (int i = 0; i < MyUIFourMyParticlelayer.size(); i++){
				TafetVariable.push_back(&MyUIFourMyParticlelayer.at(i));
			}
			break;
		case 4:
			TafetVariable.push_back(&MyUIFiveParticlelayer);
			break;
		default:
			CCLOG("editbox BakcGroundnum two in a MyUI Taget is over ");
			break;
		}
		break;
	case Target_of_UI::TARGET_GRAVITY:
		switch (BackgroundNum)
		{
		case 2:
			for (int i = 0; i < MyUIThreeMyPhysicslayer.size(); i++){
				TafetVariable.push_back(&MyUIThreeMyPhysicslayer.at(i));
			}
			break;
		case 3:
			TafetVariable.push_back(&MyUIFourMyPhysicslayer);
			break;
		default:
			CCLOG("editbox BakcGroundnum two in a MyUI Taget is over ");
			break;
		}
		break;
	default:
		CCLOG("editbox BakcGroundnum two in a MyUI Taget is over ");
		break;
	}
#pragma endregion UILAYERDATANUMBER
	for (int k = 0, LableNumber = 0,ShameNumber = 0; k < MyUserInterface.at(BackgroundNum).UI.size(); k++){
		/*
		이거 고치자*/
		if (MyUserInterface.at(BackgroundNum).FromUI.at(k) == UIKind::BUTTON){ continue; }
		else if (MyUserInterface.at(BackgroundNum).FromUI.at(k) == UIKind::CHECKBOX){
			LableNumber++;
			continue;
		}
		else if (MyUserInterface.at(BackgroundNum).EditSliderShame.at(ShameNumber) != editBox){
			LableNumber++;
			ShameNumber++;
			continue;
		}

		setObjectvalrue(BackgroundNum, MyUserInterface.at(BackgroundNum).NowUIBundleNumber, k
			,atoi(editBox->getText()), TafetVariable.at(MyUserInterface.at(BackgroundNum).NowUIBundleNumber));
		return;
	}
}

void MyUI::setObjectvalrue(int BackgroundNum, int UIBundle, int UINum, int UIState, UIVariable* VatiableChanged){

	if (MyUserInterface.size() <= BackgroundNum){
		CCLOG("Microbe Background has an Unexpected value    BackgroundNum = %d", BackgroundNum);
		return;
	}
	else if (MyUserInterface.at(BackgroundNum).UI.size() <= UINum){
		CCLOG("Microbe UINum Size has an Unexpected value    UINum = %d", UINum);
		return;
	}

	bool VarSwitch = ((CheckBox*)MyUserInterface.at(BackgroundNum).UI.at(UINum))->getSelectedState();
	switch (MyUI_Taget)
	{
	case MyUI::NONE:
		CCLOG("NONE = %d %d", BackgroundNum, UINum);
		return;
		break;
#pragma region TARGET_MICROBE
	case MyUI::TARGET_MICROBE:
		/*
		microbe는 bakcfround가 2밖에 없다.*/
		if (microbe.size() <= 0 || BackgroundNum != 2){return;}
		switch (UIBundle)
		{
			/*
			Microbe Size*/
		case 0:
			switch (UINum){
			case 0:
				NOW_MICROBE->setScale(UIState);
				break;
			case 1:
				NOW_MICROBE->setScaleX(UIState);
				break;
			case 2:
				NOW_MICROBE->setScaleY(UIState);
				break;
			default:
				CCLOG("Microbe BackgroundNum = 2 UIBundle = 0 in a UINum has an Unexpected value  UINum= %d", UINum);
				return;
			}
			break;
			/*
			Microbe Color*/
			switch (UINum){
			case 0:
				NOW_MICROBE->setColor(Color3B(UIState, NOW_MICROBE->getColor().g, NOW_MICROBE->getColor().b));
				break;
			case 1:
				NOW_MICROBE->setColor(Color3B(NOW_MICROBE->getColor().r, UIState, NOW_MICROBE->getColor().b));
				break;
			case 2:
				NOW_MICROBE->setColor(Color3B(NOW_MICROBE->getColor().r, NOW_MICROBE->getColor().g, UIState));
				break;
			case 3:
				NOW_MICROBE->setOpacity(UIState);
				break;
			default:
				CCLOG("Microbe BackgroundNum = 2 UIBundle = 1 in a UINum has an Unexpected value  UINum= %d", UINum);
				return;
			}
			break;
		case 2:
			/*
			Miceobe Position*/
			switch (UINum){
			case 0:
				NOW_MICROBE->setPositionX((float)UIState);
				break;
			case 1:
				NOW_MICROBE->setPositionY((float)UIState);
				break;
			default:
				CCLOG("Microbe BackgroundNum = 2 UIBundle = 2 in a UINum has an Unexpected value  UINum= %d", UINum);
				return;
			}
			break;
		default:
			CCLOG("UIBundle is Over UIBundle = %d", UINum);
			return;
		}
		break;
#pragma endregion TARGET_MICROBE

#pragma region TARGET_PATICLE
	case MyUI::TARGET_PARTICLE:{

		switch (BackgroundNum)
		{
		case 2:/***************************threepaticle Kind***************************/
#pragma region ParticleKind
			if (UIBundle){
				CCLOG("Paticle background Num = 2 UIBundle is not a value of zero.");
				return;
			}
			if (!UIState && ShinySwitchNumber == UINum){
				Currentlayer->removeChild(Shiny);
				ShinySwitchNumber = -1;
				break;
			}
			if (ShinySwitchNumber != UINum && ShinySwitchNumber != -1){
				Currentlayer->removeChild(Shiny);

				SET_CHECK_STATE(BackgroundNum, ShinySwitchNumber);

				MyUIThreeParticlelayer.at(0).CheckState.at(ShinySwitchNumber) = false;
			}
			switch (UINum){
			case 0:
				Shiny = ParticleFire::create();
				CCLOG("UINum = 0");
				break;
			case 1:
				Shiny = ParticleFireworks::create();
				break;
			case 2:
				Shiny = ParticleSun::create();
				break;
			case 3:
				Shiny = ParticleGalaxy::create();
				break;
			case 4:
				Shiny = ParticleFlower::create();
				break;
			case 5:
				Shiny = ParticleMeteor::create();
				break;
			case 6:
				Shiny = ParticleSpiral::create();
				break;
			case 7:
				Shiny = ParticleExplosion::create();
				break;
			case 8:
				Shiny = ParticleSmoke::create();
				break;
			case 9:
				Shiny = ParticleSnow::create();
				break;
			case 10:
				Shiny = ParticleRain::create();
				break;
			default:
				CCLOG("Paticle  Kind UINum has an Unexpected value UINum = %d ", UINum);
				break;
			}
			ShinySwitchNumber = UINum;
			Currentlayer->addChild(Shiny);
			break;
#pragma endregion  ParticleKind
		case 3:/***************************Fouraticle Emitter Shpe Mode ***************************/
			switch (UIBundle){
			case 0:
#pragma region PaticleEmitter
				switch (UINum)
				{
				case 0:
					Shiny->setEmitterMode((ParticleSystem::Mode)UIState);
					break;
				case 1:
					CCLOG("Shiny Emitter Var Switch ON!!!!!!!!!!!!!!!");
					break;
				case 2:
					if (VarSwitch){ Shiny->setAngleVar(UIState); }
					else{ Shiny->setAngle(UIState); }
					break;
				case 3:
					Shiny->setEmissionRate(UIState);
					break;
				case 4:
					Shiny->setPosVar(Vec2(UIState, Shiny->getPosVar().y));
					break;
				case 5:
					Shiny->setPosVar(Vec2(Shiny->getPosVar().x, UIState));
					break;
				case 6:
					if (VarSwitch){ Shiny->setStartRadiusVar(UIState); }
					else{ Shiny->setStartRadius(UIState); }
					break;
				case 7:
					if (VarSwitch){ Shiny->setEndRadiusVar(UIState); }
					else{ Shiny->setEndRadius(UIState); }
					break;
				case 8:
					if (VarSwitch){ Shiny->setRotatePerSecondVar(UIState); }
					else{ Shiny->setRotatePerSecond(UIState); }
					break;
				default:
					CCLOG("Paticle BackgroundNum = 3 UIBundle = 0 in a UINum has an Unexpected value  UINum= %d", UINum);
					break;
				}
				break;
#pragma endregion PaticleEmitter
			case 1:
#pragma region PaticleShpe
				switch (UINum)
				{
				case 0:
					Shiny->setBlendAdditive(UIState);
					break;
				case 1:
					CCLOG("Shiny Shpe Var Switch ON!!!!!!!!!!!!!!!");
					break;
				case 2:
					if (VarSwitch){ Shiny->setStartSizeVar(UIState); }
					else{ Shiny->setStartSize(UIState); }
					break;
				case 3:
					if (VarSwitch){ Shiny->setEndSizeVar(UIState); }
					else{ Shiny->setEndSize(UIState); }
					break;
				case 4:
					if (VarSwitch){ Shiny->setStartSpinVar(UIState); }
					else{ Shiny->setStartSpin(UIState); }
					break;
				case 5:
					if (VarSwitch){ Shiny->setEndSpinVar(UIState); }
					else{ Shiny->setEndSpin(UIState); }
					break;
				default:
					CCLOG("Paticle BackgroundNum = 3 UIBundle = 1 in a UINum has an Unexpected value  UINum= %d", UINum);
					break;
				}
				break;
#pragma endregion PaticleShpe
			case 2:
#pragma region PaticleMode
				switch (UINum)
				{
				case 0:
					CCLOG("Shiny Mode Var Switch ON!!!!!!!!!!!!!!!");
					break;
				case 1:
					if (VarSwitch){ Shiny->setSpeedVar(UIState); }
					else{ Shiny->setSpeed(UIState); }
					break;
				case 2:
					UIState -= 200;
					Shiny->setGravity(Vec2(UIState, Shiny->getGravity().y));
					break;
				case 3:
					UIState -= 200;
					Shiny->setGravity(Vec2(Shiny->getGravity().x, UIState));
					break;
				case 4:
					UIState -= 200;
					Shiny->setRadialAccel(UIState);
					break;
				case 5:
					UIState -= 200;
					if (VarSwitch){ Shiny->setTangentialAccelVar(UIState); }
					else{ Shiny->setTangentialAccel(UIState); }
				default:
					CCLOG("Paticle BackgroundNum = 3 UIBundle =2 in a UINum has an Unexpected value  UINum= %d", UINum);
					break;
				}
				break;
#pragma endregion PaticleMode
			}
			break;
			
		case 4:{ /***************************Five particle Color ***************************/
#pragma region PaticleColor
			if (UIBundle){
				CCLOG("Paticle background Num = 4 UIBundle is not a value of zero.");
				return;
			}

			int StartNum = 0;
			switch (UINum){
			case 0:
				CCLOG("StartColor Switch ON");
				break;
			case 1:
				CCLOG("Var Switch ON");
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			default:
				CCLOG("BackgroudNUm = 4 : UINUm has an Unexpected value UINum= %d", UINum);
				return;
			}
			if (StartNum){
				if (VarSwitch){
					switch (UINum){
					case 2:
						Shiny->setStartColorVar(Color4F(UIState, Shiny->getStartColorVar().g, Shiny->getStartColorVar().b, Shiny->getStartColorVar().a));
						break;
					case 3:
						Shiny->setStartColorVar(Color4F(Shiny->getStartColorVar().r, UIState, Shiny->getStartColorVar().b, Shiny->getStartColorVar().a));
						break;
					case 4:
						Shiny->setStartColorVar(Color4F(Shiny->getStartColorVar().r, Shiny->getStartColorVar().g, UIState, Shiny->getStartColorVar().a));
						break;
					case 5:
						Shiny->setStartColorVar(Color4F(Shiny->getStartColorVar().r, Shiny->getStartColorVar().g, Shiny->getStartColorVar().b, UIState));
						break;
					}
				}
				else{
					switch (UINum){
					case 2:
						Shiny->setStartColor(Color4F(UIState, Shiny->getStartColor().g, Shiny->getStartColor().b, Shiny->getStartColor().a));
						break;
					case 3:
						Shiny->setStartColor(Color4F(Shiny->getStartColor().r, UIState, Shiny->getStartColor().b, Shiny->getStartColor().a));
						break;
					case 4:
						Shiny->setStartColor(Color4F(Shiny->getStartColor().r, Shiny->getStartColor().g, UIState, Shiny->getStartColor().a));
						break;
					case 5:
						Shiny->setStartColor(Color4F(Shiny->getStartColor().r, Shiny->getStartColor().g, Shiny->getStartColor().b, UIState));
						break;
					}
				}
			}
			else{
				if (VarSwitch){
					switch (UINum){
					case 2:
						Shiny->setEndColorVar(Color4F(UIState, Shiny->getEndColorVar().g, Shiny->getEndColorVar().b, Shiny->getEndColorVar().a));
						break;
					case 3:
						Shiny->setEndColorVar(Color4F(Shiny->getEndColorVar().r, UIState, Shiny->getEndColorVar().b, Shiny->getEndColorVar().a));
						break;
					case 4:
						Shiny->setEndColorVar(Color4F(Shiny->getEndColorVar().r, Shiny->getEndColorVar().g, UIState, Shiny->getEndColorVar().a));
						break;
					case 5:
						Shiny->setEndColorVar(Color4F(Shiny->getEndColorVar().r, Shiny->getEndColorVar().g, Shiny->getEndColorVar().b, UIState));
						break;
					}
				}
				else{
					switch (UINum){
					case 2:
						Shiny->setEndColor(Color4F(UIState, Shiny->getEndColor().g, Shiny->getEndColor().b, Shiny->getEndColor().a));
						break;
					case 3:
						Shiny->setEndColor(Color4F(Shiny->getEndColor().r, UIState, Shiny->getEndColor().b, Shiny->getEndColor().a));
						break;
					case 4:
						Shiny->setEndColor(Color4F(Shiny->getEndColor().r, Shiny->getEndColor().g, UIState, Shiny->getEndColor().a));
						break;
					case 5:
						Shiny->setEndColor(Color4F(Shiny->getEndColor().r, Shiny->getEndColor().g, Shiny->getEndColor().b, UIState));
						break;
					}
				}
			}

			break;
#pragma endregion PaticleColor
		}
		default:
			CCLOG("UINum has an Unexpected value UINum = %d", UINum);
			break;
		}
		break;
	}
#pragma endregion TARGET_PATICLE

#pragma region TARGET_PHYSICS
	case MyUI::TARGET_GRAVITY:
		switch (BackgroundNum)
		{
		case 2:
			switch (UIBundle)
			{
			case 0:
#pragma region PhysicsGravity
				/*
				0은 gravityswitch이다.*/
				if (UINum == 0){
					UIPhysics.MyGravitySwitch = (bool)UIState;
					break;
				}
				switch (UINum)
				{
				case 5:
					switch (UIPhysics.MyGravityEcho)
					{
					case GravityEcho::TOP:
						UIPhysics.MyGravityPlace = Vec2((float)UIState, 0.0f);
						break;
					case GravityEcho::LEFT:
						UIPhysics.MyGravityPlace = Vec2(0.0f, (float)UIState);
						break;
					case GravityEcho::BOTTOM:
						UIPhysics.MyGravityPlace = Vec2(0.0f, (float)UIState * -1.0f);
						break;
					case GravityEcho::RIGHT:
						UIPhysics.MyGravityPlace = Vec2((float)UIState *-1.0f, 0.0f);
						break;
					default:
						CCLOG("UINum Grvity Power UINum is over= %d", UINum);
						break;
					}
					break;
				}
				if (UINum > GravityEcho::LEFT){
					CCLOG("End PhysicsGravity= %d",UINum);
					break;
				}
				if (UIPhysics.MyGravityEcho != UINum && UIState){
					SET_CHECK_STATE(BackgroundNum, UIPhysics.MyGravityEcho);
					MyUIThreeMyPhysicslayer.at(0).CheckState.at(UIPhysics.MyGravityEcho) = false;
				}
				switch (UINum)
				{
				case 1:
					UIPhysics.MyGravityEcho = GravityEcho::TOP;
					break;
				case 2:
					UIPhysics.MyGravityEcho = GravityEcho::RIGHT;
					break;
				case 3:
					UIPhysics.MyGravityEcho = GravityEcho::BOTTOM;
					break;
				case 4:
					UIPhysics.MyGravityEcho = GravityEcho::LEFT;
					break;
				default:
					CCLOG("UINum Kind UINum has an Unexpected value UINum = %d");
					break;
				}
				break;
#pragma endregion PhysicsGravity
			case 1:
#pragma region PhysicsAcceleration
				switch (UINum)
				{
				case 0:
					UIPhysics.MyBlockAcceleration.Interval = UIState;
					break;
				case 1:
					UIPhysics.MyBlockAcceleration.Power = UIState;
					break;
				case 2:
					UIPhysics.MyBlockAcceleration.PowerMax = UIState;
					break;
				default:
					break;
				}
				break;
#pragma endregion PhysicsAcceleration
			case 2:
#pragma region PhysicsFriction
				if (UIPhysics.MyPhysicsFriction.size() == 0){
					CCLOG("Not Friction Create");
					break;
				}
				switch (UINum)
				{
				case 1:
					NOWMYFRICTION.FrictionPosition.x = UIState;
					NOWMYFRICTION.FrictionBody->setPositionX(NOWMYFRICTION.FrictionPosition.x);
					NOWMYFRICTION.FrictionScale[0].x = NOWMYFRICTION.FrictionPosition.x - NOWMYFRICTION.FrictionSize.width / 2;
					NOWMYFRICTION.FrictionScale[1].x = NOWMYFRICTION.FrictionPosition.x + NOWMYFRICTION.FrictionSize.width / 2;
					break;
				case 2:
					NOWMYFRICTION.FrictionPosition.y = UIState;
					NOWMYFRICTION.FrictionBody->setPositionY(NOWMYFRICTION.FrictionPosition.y);
					NOWMYFRICTION.FrictionScale[0].y = NOWMYFRICTION.FrictionPosition.y - NOWMYFRICTION.FrictionSize.height / 2;
					NOWMYFRICTION.FrictionScale[1].y = NOWMYFRICTION.FrictionPosition.y + NOWMYFRICTION.FrictionSize.height / 2;
					break;
				case 3:
					NOWMYFRICTION.FrictionSize.width = UIState;
					NOWMYFRICTION.FrictionBody->setScaleX(NOWMYFRICTION.FrictionSize.width / 10);
					NOWMYFRICTION.FrictionScale[0].x -= UIState / 2;
					NOWMYFRICTION.FrictionScale[1].x += UIState / 2;
					break;
				case 4:
					NOWMYFRICTION.FrictionSize.height = UIState;
					NOWMYFRICTION.FrictionBody->setScaleY(NOWMYFRICTION.FrictionSize.height / 10);
					NOWMYFRICTION.FrictionScale[0].y -= UIState / 2;
					NOWMYFRICTION.FrictionScale[1].y += UIState / 2;
					break;
				case 5:
					NOWMYFRICTION.FrictionPower = UIState;
					break;
				case 6:
					UIPhysics.MyFrictionNumber = UIState;
					break;
				default:
					CCLOG(" Physics Friction UInum is over %d %d = %d", BackgroundNum, UIBundle,UINum);
					break;
				}
				break;
#pragma endregion PhysicsFriction
			default:
				CCLOG("Physics UIBundle is over Bacground = %d  UIBundle = %d", BackgroundNum, UIBundle);
				break;
			}
			break;
#pragma region PhysicsGravityPiece
		case 3:
			if (UIBundle != 0){
				CCLOG("Physics Piece UIBundle is over B = %d UIBundle %d", BackgroundNum, UIBundle);
			}
			switch (UINum)
			{
			case 1:
				NOWMYGRAVITYPIECE.PiecePosition.x = UIState;
				NOWMYGRAVITYPIECE.PieceBody->setPositionX(NOWMYGRAVITYPIECE.PiecePosition.x);
				NOWMYGRAVITYPIECE.PieceScale[0].x = NOWMYGRAVITYPIECE.PiecePosition.x - NOWMYGRAVITYPIECE.PieceSize.width / 2;
				NOWMYGRAVITYPIECE.PieceScale[1].x = NOWMYGRAVITYPIECE.PiecePosition.x + NOWMYGRAVITYPIECE.PieceSize.width / 2;
				break;
			case 2:
				NOWMYGRAVITYPIECE.PiecePosition.y = UIState;
				NOWMYGRAVITYPIECE.PieceBody->setPositionY(NOWMYGRAVITYPIECE.PiecePosition.y);
				NOWMYGRAVITYPIECE.PieceScale[0].y = NOWMYGRAVITYPIECE.PiecePosition.y - NOWMYGRAVITYPIECE.PieceSize.height / 2;
				NOWMYGRAVITYPIECE.PieceScale[1].y = NOWMYGRAVITYPIECE.PiecePosition.y + NOWMYGRAVITYPIECE.PieceSize.height / 2;
				break;
			case 3:
				NOWMYGRAVITYPIECE.PieceSize.width = UIState;
				NOWMYGRAVITYPIECE.PieceBody->setScaleX(NOWMYGRAVITYPIECE.PieceSize.width / 10);
				NOWMYGRAVITYPIECE.PieceScale[0].x -= UIState / 2;
				NOWMYGRAVITYPIECE.PieceScale[1].x += UIState / 2;
				break;
			case 4:
				NOWMYGRAVITYPIECE.PieceSize.height = UIState;
				NOWMYGRAVITYPIECE.PieceBody->setScaleY(NOWMYGRAVITYPIECE.PieceSize.height / 10);
				NOWMYGRAVITYPIECE.PieceScale[0].y -= UIState / 2;
				NOWMYGRAVITYPIECE.PieceScale[1].y += UIState / 2;
				break;
			case 5:
				NOWMYGRAVITYPIECE.PiecePower = UIState;
				break;
			case 6:
				UIPhysics.MyPieceNumber = UIState;
				break;
			default:
				CCLOG(" Physics Piece UInum is over %d %d = %d", BackgroundNum, UIBundle, UINum);
				break;
			}
			break;
#pragma endregion PhysicsGravityPiece
		default:
			break;
		}
#pragma	endregion TARGET_PHYSICS
		break;
	default:
		break;
	}
	
	char UIStateText[5];
	itoa(UIState, UIStateText, 10);

	switch (MyUserInterface.at(BackgroundNum).FromUI.at(UINum))
	{
	case MyUI::BUTTON:
		CCLOG("Button in the setObject function MyUserInterface.at(%d).FromUI.at(%d)",BackgroundNum,UINum);
		break;
	case MyUI::SLIDER:{
		((Slider*)MyUserInterface.at(BackgroundNum).UI.at(UINum))->setPercent(UIState);

		int Editboxnum = -1;
		/*
		UINum이 지금 클릭한 UI이 위치이다.*/
		/*
		이곳을 고치자*/
		for (int i = 0; i < UINum+1; i++){
			if (MyUserInterface.at(BackgroundNum).FromUI.at(i) == MyUI::SLIDER){
				Editboxnum++;
			}
		}
		MyUserInterface.at(BackgroundNum).EditSliderShame.at(Editboxnum)->setText(UIStateText);

		/*
		여기가 문제 SlideNow의 사이즈가 1이 된다.*/
		VatiableChanged->SliderNow.at(Editboxnum) = UIState;
		break;
	}
	case MyUI::CHECKBOX:
		VatiableChanged->CheckState.at(UINum) = UIState;
		break;
	default:
		break;
	}
}

/*
****************************************************UIEventListener*****************************************************/

/*
**************************OneLayer***************************/
/* 좌표그리는 함수*/
void MyUI::OneCoordinateplaneSwitch(Ref* ref){
	if (CHECK_SWITCH){
		Coordinatesystem = DrawNode::create();
		int Magnification = 5;
		for (int i = 0; i < ScreenSize.width / Magnification; i += Magnification){
			Coordinatesystem->drawLine(Vec2(i * Magnification, 0), Vec2(i * Magnification, ScreenSize.height), Color4F::GRAY);
		}
		for (int i = 0; i < ScreenSize.height / Magnification; i += Magnification){
			Coordinatesystem->drawLine(Vec2(0, i * Magnification), Vec2(ScreenSize.width, i * Magnification), Color4F::GRAY);
		}
		Currentlayer->addChild(Coordinatesystem);
	}
	else{
		Currentlayer->removeChild(Coordinatesystem);
	}
}
/*
**************************ThreeLayer***************************/
/*
Friction이 증가하지 않는다.*/

void MyUI::PushFriction(Ref* ref){
	PhysicsFriction FunctionPhysicsFriction;
	FunctionPhysicsFriction.FrictionPosition = Vec2(480.0f, 320.0f);
	FunctionPhysicsFriction.FrictionSize = Size(60.0f, 0.0f);
	FunctionPhysicsFriction.FrictionScale[0] = FunctionPhysicsFriction.FrictionPosition - FunctionPhysicsFriction.FrictionSize / 2;
	FunctionPhysicsFriction.FrictionScale[1] = FunctionPhysicsFriction.FrictionPosition + FunctionPhysicsFriction.FrictionSize / 2;
	FunctionPhysicsFriction.FrictionPower = 2;
	FunctionPhysicsFriction.FrictionBody = Sprite::create("WhiteBox20x20.png");
	FunctionPhysicsFriction.FrictionBody->setPosition(Vec2(FunctionPhysicsFriction.FrictionPosition));
	FunctionPhysicsFriction.FrictionBody->setColor(Color3B(100.0f,0.0f,0.0f));
	FunctionPhysicsFriction.FrictionBody->setScaleX(FunctionPhysicsFriction.FrictionSize.width / 10);
	FunctionPhysicsFriction.FrictionBody->setScaleY(0.1f);
	FunctionPhysicsFriction.FrictionBody->setAnchorPoint(Vec2(1.0f, 1.0f));
	Currentlayer->addChild(FunctionPhysicsFriction.FrictionBody);

	UIPhysics.MyPhysicsFriction.push_back(FunctionPhysicsFriction);
	UIPhysics.MyFrictionNumber++;
	/*
	Slider의 값을 변경하는 함수 friction이 추가 될때마다 slider의 값이 증가한다.*/
	MyUIThreeMyPhysicslayer.at(0).SliderMax.back() = UIPhysics.MyFrictionNumber;
	((Slider*)MyUserInterface.back().UI.back())->setMaxPercent(UIPhysics.MyFrictionNumber);
}
/*
**************************Microbe***************************/
void MyUI::ThreeMicrobeCreate(Ref* ref){
	Microbe method;
	method.Appearance = Sprite::create("WhiteBox20x20.png");
	method.Appearance->setPosition(ScreenSize / 2);
	method.Appearance->setColor(Color3B(250, 250, 250));
	method.Appearance->setAnchorPoint(Vec2(1.0f, 0.0f));
	/*
	microbe의 번호를 만드든중*/
	std::stringstream methodname;
	methodname << "Box" << microbe.size();
	method.Name = (char*)(methodname.str().c_str());
	microbe.push_back(method);
	microbenum = microbe.size() - 1;
	MyUI::Currentlayer->addChild(microbe.back().Appearance, microbe.size());
	/*
	Physics에 넣는 중*/
	UIPhysics.addMyBlock(microbe.back().Appearance);
}
/*
**************************FourLayer***************************/
void MyUI::PushPiece(Ref* ref){
	GravityPiece FunctionGravityPiece;
	FunctionGravityPiece.PiecePosition = Vec2(480, 320);
	FunctionGravityPiece.PieceSize = Vec2(60, 60);
	FunctionGravityPiece.PieceScale[0] = FunctionGravityPiece.PiecePosition - FunctionGravityPiece.PieceSize / 2;
	FunctionGravityPiece.PieceScale[1] = FunctionGravityPiece.PiecePosition + FunctionGravityPiece.PieceSize / 2;
	FunctionGravityPiece.PiecePower = 5;
	FunctionGravityPiece.PieceBody = Sprite::create("WhiteBox20x20.png");
	FunctionGravityPiece.PieceBody->setPosition(FunctionGravityPiece.PiecePosition);
	FunctionGravityPiece.PieceBody->setColor(Color3B(100.0f, 0.0f, 100.0f));
	FunctionGravityPiece.PieceBody->setScale(6.0f);
	FunctionGravityPiece.PieceBody->setAnchorPoint(Vec2(1.0f, 1.0f));
	Currentlayer->addChild(FunctionGravityPiece.PieceBody);
	UIPhysics.MyGravityPiece.push_back(FunctionGravityPiece);
	UIPhysics.MyPieceNumber++;
	/*
	Slider의 값을 변경하는 함수 Piece이 추가 될때마다 slider의 값이 증가한다.*/
	MyUIFourMyPhysicslayer.SliderMax.back() = UIPhysics.MyPieceNumber;
	((Slider*)MyUserInterface.back().UI.back())->setMaxPercent(UIPhysics.MyPieceNumber);
}