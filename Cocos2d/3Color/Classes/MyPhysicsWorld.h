#ifndef __MYPHYSICSWORLD_H__
#define __MYPHYSICSWORLD_H__
#include <cocos2d.h>
#include <ui\CocosGUI.h>

#define G 0.01f
#define BETWEENTWOPOINT(X1,Y1,X2,Y2) sqrt((Y1 - Y2)*(Y1 - Y2)+(X1 - X2)*(X1 - X2))
#define UNIVERSALGRAVITY(OneMas,TwoMas,Street) G *(OneMas*TwoMas) / (Street)

#define MYPHYSICSFRICTION MyPhysicsFriction.at(MyFrictionNumber)
#define MYPHYSICSPIECE MyGravityPiece.at(MyPieceNumber)

#define MYBLOCK MyBlock.at(i)
#define TARGETBLOCK MyBlock.at(k)

using namespace cocos2d;
using namespace cocos2d::ui;


struct GravityPiece{
	Sprite* PieceBody;
	Vec2 PiecePosition;
	Size PieceSize;
	Vec2 PieceScale[2];
	int PiecePower;

};

struct PhysicsFriction{
	Sprite* FrictionBody;
	Vec2 FrictionPosition;
	Size FrictionSize;
	Vec2 FrictionScale[2];
	int FrictionPower;
};

struct PhysicsAcceleration{
	int Interval;
	int Power;
	int PowerMax;
};

enum BlockLocation{ FLOATION, LANDING };
/*
���� ui�� 0������ gravity�� Ű�� ����ġ�� �ִ�.*/
enum GravityEcho{ TOP = 1, RIGHT, BOTTOM, LEFT };

class MyPhysics{
public:
	MyPhysics(){

		MyBlock = std::vector<Sprite*>(0);
		MyBlockMas = std::vector<int>(0);
		MyBlockEcho = std::vector<Vec2>(0);
		MyBlockSkyTime = std::vector<int>(0);
		MyBlockAcceleration = { 2, 1, 8 };
		MyBlockSkyTime = std::vector < int >(0) ;
		MyBlockState = std::vector<unsigned char>(0);
		MyBlockLocation = std::vector<BlockLocation>(0);
		MyBlockLocation.push_back(BlockLocation::FLOATION);
		MyGravityPiece = std::vector<GravityPiece>(0);
		MyPhysicsFriction  = std::vector<PhysicsFriction>(0);
		Director::getInstance()->getScheduler()->schedule(std::bind(&MyPhysics::MyGravityEngine, this, std::placeholders::_1), this, 0.01f,false, "GravityEngin");

	}

	/*
	My Block Value*/
	std::vector<Sprite*> MyBlock;
	std::vector<int> MyBlockMas;
	std::vector<Vec2> MyBlockEcho;
	std::vector<int> MyBlockSkyTime;
	PhysicsAcceleration MyBlockAcceleration;
	std::vector<unsigned char> MyBlockState;
	//block�� ���¸� �Լ� ���ο��� Ȯ���ϴ� ���� (�ϴÿ� �ִ��� ���� �ִ���)
	std::vector<BlockLocation> MyBlockLocation;

	/*
	�������� gravity���� */
	std::vector<GravityPiece> MyGravityPiece;
	int MyPieceNumber = -1;
	/*
	���� ����*/
	std::vector<PhysicsFriction> MyPhysicsFriction;
	int MyFrictionNumber = -1;
	/*
	�߷��� ������ ��Ÿ���� ����*/
	GravityEcho MyGravityEcho = GravityEcho::BOTTOM;

	bool MyGravitySwitch = false;

	Vec2 MyGravityPlace = Vec2(0.0f, -1.0f);
	Vec2 MyPhysicsMapEnd = Vec2(0.0f, 0.0f);

	//MyPhysics�� ������ �߰��ϴ� �Լ�
	void addMyBlock(Sprite* host);

private:

	void MyGravityEngine(float dt);

};


#endif