#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include <algorithm>

USING_NS_CC;


using namespace cocostudio::timeline;
Scene* HelloWorld::createScene()
{
	auto scene = Scene::create();
	auto layer = HelloWorld::create();
	scene->addChild(layer);
	return scene;
}

bool HelloWorld::init()
{
	if (!Layer::init()){ return false; }
	UIInterface.InterfaceStart(this);
	return true;
}

void HelloWorld::onEnter(){
	Layer::onEnter();
}