#include "MyPhysicsWorld.h"

#define BLOCKMOVING 0x1
#define BLOCKECHO 0x2
#define BLOCKSKY 0x4

void MyPhysics::addMyBlock(Sprite* host){
	MyBlock.push_back(host);
	MyBlockMas.push_back(10000);
	MyBlockEcho.push_back(MyBlock.back()->getPosition());
	MyBlockLocation.push_back(BlockLocation::FLOATION);
	MyBlockSkyTime.push_back(0);
	MyBlockState.push_back(BLOCKSKY);
}

/*
증력을 만드는 함수 UI.H에 넣을 필요가 있다.*/
void MyPhysics::MyGravityEngine(float dt){
	/*
	중력 스위치가 켜질경우 실행*/
	if (!MyGravitySwitch){ return; }
	/*
	가속도의 힘을 나타내는 변수다*/
	int accelerationPower = 1;
	int BlockFrictionPower = 1;
	Vec2 NextPosition = Vec2(0.0f, 0.0f);
	Vec2 NextMoveValue = Vec2(0.0f, 0.0f);

	for (int i = 0; i < MyBlock.size(); i++){

		NextMoveValue = Vec2(0.0f, 0.0f);
		NextPosition = MyBlockEcho.at(i);
		BlockFrictionPower = 1;

		switch (MyBlockLocation.at(i))
		{
		case FLOATION:{
			MyBlockState.at(i) |= BLOCKSKY;
			MyBlockSkyTime.at(i)++;
			accelerationPower = 1;
			break;
		}
		case LANDING:
			if (MyBlockState.at(i) &= BLOCKSKY){
				MyBlockState.at(i) ^= BLOCKSKY;
			}
			MyBlockSkyTime.at(i) = 0;
			NextMoveValue = Vec2(6.0f, 0.0f);
			break;
		default:
			break;
		}


		/*
		MyBlockState 에 Blockfriction의 값으로 변경하라 */

		for (int k = 0; k < MyPhysicsFriction.size() && MyBlockState.at(i) ^ BLOCKSKY; k++){
			if (MyBlockEcho.at(i).x < MyPhysicsFriction.at(k).FrictionScale[0].x || MyBlockEcho.at(i).x >MyPhysicsFriction.at(k).FrictionScale[1].x){
				BlockFrictionPower = 1;
				continue;
			}
			if (MyBlockEcho.at(i).y < MyPhysicsFriction.at(k).FrictionScale[0].y || MyBlockEcho.at(i).y > MyPhysicsFriction.at(k).FrictionScale[1].y){
				BlockFrictionPower = 1;
				continue;
			}
			BlockFrictionPower = MyPhysicsFriction.at(k).FrictionPower;
		}
#pragma region MYPIECE
		for (int k = 0; k < MyGravityPiece.size(); k++){
			/*
			myblockecho가 mypiece밖에 있을경우 continue를 해서 다른 piece를 찾는 알고리즘이다.*/

			if (MyBlockEcho.at(i).x < MyGravityPiece.at(k).PieceScale[0].x || MyBlockEcho.at(i).y < MyGravityPiece.at(k).PieceScale[0].y){ continue; }
			else if (MyBlockEcho.at(i).x > MyGravityPiece.at(k).PieceScale[1].x || MyBlockEcho.at(i).y > MyGravityPiece.at(k).PieceScale[1].y){ continue; }

			Vec2 BetweenPieceBlock = MyGravityPiece.at(k).PiecePosition - MyBlockEcho.at(i);

			if (BetweenPieceBlock.x > 0){
				if (BetweenPieceBlock.y > 0){
					NextMoveValue += Vec2(MyGravityPiece.at(k).PiecePower, MyGravityPiece.at(k).PiecePower);
				}
				else{
					NextMoveValue += Vec2(MyGravityPiece.at(k).PiecePower, -MyGravityPiece.at(k).PiecePower);
				}
			}
			else{
				if (BetweenPieceBlock.y > 0){
					NextMoveValue += Vec2(-MyGravityPiece.at(k).PiecePower, MyGravityPiece.at(k).PiecePower);
				}
				else{
					NextMoveValue += Vec2(-MyGravityPiece.at(k).PiecePower, -MyGravityPiece.at(k).PiecePower);
				}
			}

		}
#pragma endregion MYPIECE

		if (MyBlockState.at(i) & BLOCKSKY){
#pragma region MYGLAVITY
			/*
			가속도 재는 알고리즘*/
			int DivisionTimeInterval = MyBlockSkyTime.at(i) / MyBlockAcceleration.Interval;
			if (MyBlockAcceleration.PowerMax <= MyBlockAcceleration.Power * DivisionTimeInterval){
				accelerationPower = MyBlockAcceleration.PowerMax;
			}
			else if (DivisionTimeInterval > 0){
				accelerationPower = MyBlockAcceleration.Power * DivisionTimeInterval;
			}
			/*
			땅과 다았는지 아닌지를 채크하는 알고리즘*/
			NextMoveValue += MyGravityPlace * accelerationPower;
			NextPosition += NextMoveValue;
			switch (MyGravityEcho)
			{
			case TOP:
				if (NextPosition.y >= MyPhysicsMapEnd.y){
					MyBlockLocation.at(i) = BlockLocation::LANDING;
					NextPosition.y = MyPhysicsMapEnd.y;
				}
				break;
			case RIGHT:
				if (NextPosition.x >= MyPhysicsMapEnd.x){
					MyBlockLocation.at(i) = BlockLocation::LANDING;
					NextPosition.x = MyPhysicsMapEnd.x;
				}
				break;
			case BOTTOM:
				if (NextPosition.y <= MyPhysicsMapEnd.y){
					MyBlockLocation.at(i) = BlockLocation::LANDING;
					NextPosition.y = MyPhysicsMapEnd.y;
				}
				break;
			case LEFT:
				if (NextPosition.x <= MyPhysicsMapEnd.x){
					MyBlockLocation.at(i) = BlockLocation::LANDING;
					NextPosition.x = MyPhysicsMapEnd.x;
				}
				break;
			default:
				break;
			}
#pragma endregion MYGLAVITY
		}

		if (MyGravityEcho == BOTTOM || MyGravityEcho == TOP){
			NextMoveValue.x /= BlockFrictionPower;
		}
		else{
			NextMoveValue.y /= BlockFrictionPower;
		}

		NextPosition += NextMoveValue;

		MyBlockEcho.at(i) = NextPosition;

		if (MYBLOCK->getPosition() != MyBlockEcho.at(i)){
			if (MyBlockState.at(i) ^ BLOCKSKY){
				MyBlockEcho.at(i).y = 0.0f;
			}
			MYBLOCK->setPosition(MyBlockEcho.at(i));
			MyBlockEcho.at(i) = MYBLOCK->getPosition();
		}
	}
}
/*
*/