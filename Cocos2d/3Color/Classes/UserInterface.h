#ifndef __USERINTERFACE_H__
#define __USERINTERFACE_H__
#include "MyPhysicsWorld.h"

/*
할것들

gravity의 값을 전달해주지도 않는것 같다.

gravity 충돌처리

*/


#define NOW_MICROBE microbe.at(microbenum).Appearance
#define SLIDER_PERCENT ((Slider*)ref)->getPercent()
#define CHECK_SWITCH ((CheckBox*)ref)->getSelectedState()

#define SET_CHECK_STATE(LayerNumber,UINumber) ((CheckBox*)MyUserInterface.at(LayerNumber).UI.at(UINumber))->setSelectedState(false)
#define INTERFACE_COLOR4B Color4B(200.0f, 207.0f, 120.0f,240.0f)

#define NOWMYFRICTION UIPhysics.MyPhysicsFriction.at(UIPhysics.MyFrictionNumber)
#define NOWMYGRAVITYPIECE UIPhysics.MyGravityPiece.at(UIPhysics.MyPieceNumber)

/*
UI를 만드는 알고리즘
*/
/*
*/
class MyUI : public EditBoxDelegate{
public:
	enum UIKind{ BUTTON, SLIDER, CHECKBOX };

	enum Target_of_UI{ NONE, TARGET_MICROBE, TARGET_PARTICLE, TARGET_GRAVITY};

	struct Microbe{
		Sprite* Appearance;
		char* Name;
	};
	struct UIVariable{
		std::vector<UIKind> UIType;
		std::vector<char*> UIText;

		std::vector<int> SliderMax;
		std::vector<int> SliderNow;

		std::vector<bool> CheckState;

		std::vector<std::function<void(Ref*, Slider::EventType)>> MySliderEvent;
		std::vector<std::function<void(Ref*)>> MyUserInterfaceEvent;
	};
	/*현재 버튼에서 필요한 값들을 가지고 있는 struct*/
	struct AllUIDrawNodeValue{
		DrawNode* UIBackground;
		std::vector<Widget*> UI;
		std::vector<UIKind> FromUI;
		std::vector<Label*> UILabel;

		std::vector<ui::EditBox*> EditSliderShame;
		std::vector<int> SliderMax;
		Size NodeSize;
		int NowUIBundleNumber;
	};
public:
	MyUI(){
		ResetMyUI();
		ResetMyUIData();
	}

	/*
	UI Starte함수*/
	void InterfaceStart(Layer* parentslayer);

private:
	/*
	Interface Value
	*/
	std::vector<AllUIDrawNodeValue> MyUserInterface;
	
	unsigned int MyUserInterfaceLocation = 0;
	Vec2 MyUIBenchmark;
	Vec2 MyUIAnchorpoint;
	Vec2 MyUIPlacement;

	DrawNode* Coordinatesystem;
	Target_of_UI MyUI_Taget;

	MyPhysics UIPhysics;


	/*
	Interface Information*/
	UIVariable MyUIOnelayer;
	std::vector<UIVariable> MyUITwolayer;

	std::vector<UIVariable> MyUIThreeParticlelayer;
	std::vector<UIVariable> MyUIThreeMicrobelayer;
	std::vector<UIVariable> MyUIThreeMyPhysicslayer;

	std::vector<UIVariable> MyUIFourMyParticlelayer;
	UIVariable MyUIFourMyPhysicslayer;

	UIVariable MyUIFiveParticlelayer;

	/*
	MyUI가 있는 위치의 layer의 값*/
	Layer* Currentlayer;

	/*
	Specimen*/
	std::vector<Microbe> microbe;
	int microbenum = 0;
	bool Selectedmicrobe = false;


	ParticleSystemQuad* Shiny;
	bool ShinyStartColorSwitch = true;
	bool ShinyVarColorSwitch = false;
	int ShinySwitchNumber = -1;

	/*
	필요한 사이즈들*/
	Size ScreenSize;
	Size ScreenRatio;

	Size MyUIMargin;
	/*CheckBox도 이 사이즈를 사용한다.*/
	Size MyUIButtonSize;
	Size MyUISliderSize;

	float MyUILabelSize;

	/*
	**************************Fuction**************************
	*/

	/*
	UI와 background의 값을 초기화하는 함수들*/
	void OneLayerInformation();
	void TwoLayerInformation();
	void ThreeLayerInformation();
	void FourLayerInformation();
	void FiveLayderInformation();
	/*
	UI들의 생성위치 앵컬 포인트를 설정하는 함수*/
	void SetUIStartPoint();
	/*
	생성자에 들어가는 함수*/
	void ResetMyUI();
	void ResetMyUIData();

	/*
	UICreateFuction*/

	void CreateMyUserInterface(UIVariable UIKinds, int KindsBundleNumber);
	Button* CreateMyUIButton(int Buttonstairs, std::string ButtonText);
	Slider* CreateMyUISlider(int Sliderstairs, int SliderMax, int SliderNow, std::function<void(Ref*, Slider::EventType)> sliderevent);
	CheckBox* CreateMyUICheckBox(int CheckBoxstairs, bool CheckState);
	EditBox* CreateMyUIEditBox(int EditBoxStairs, int EditBoxText);

	/*
	버튼을 누를경우 후에 있던 UI를 없애주는 함수*/
	void removeMyUIDrawMicrobe(int Clickposition);

	/*
	UI값을 받아 다른 UI와 해당 object에 값을 변경하는 함수*/
	void setObjectvalrue(int BackgroundNum, int UIBundle, int UINum, int UIState, UIVariable* VatiableChanged);

	/*
	Editbox함수*/
	void editBoxReturn(EditBox* editBox);

	/*
	touchEventLisnter
	*/
	/*
	micro를 움직이게 하는 함수*/
	void microchoiceMove(Touch* tch, Event* et);

	/*
	UI Event Lisnter


	**************************OneLayer***************************/
	void OneCoordinateplaneSwitch(Ref*);
	/******************************************************/
	/*
	**************************ThreeLayer***************************/
	void PushFriction(Ref* ref);
	/*
	**************************Microbe***************************/
	void ThreeMicrobeCreate(Ref* ref);
	/*
	**************************FourLayer***************************/
	void PushPiece(Ref* ref);
};
#endif